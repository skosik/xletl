﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XLETL.DALFactory;
using XLETL.IDAL;
using XLETL.Model;

namespace XLETL.BLL
{
    public class ETL
    {
        private IETL dal = null;

        public ETL(string dbProvider)
        {
            if (string.IsNullOrEmpty(dbProvider))
                throw new ArgumentNullException("dbProvider", "Parameter is null or empty");

            // Get an instance of the ETL DAL using the DALFactory
            dal = XLETL.DALFactory.DataAccess.CreateETL(dbProvider);
        }

        public string BuildInsertStatement(TaskInfo taskInfo, bool isForTemp)
        {
            return dal.BuildInsertStatement(taskInfo, isForTemp);
        }

        public string BuildCreateTempTableStatement(TaskInfo taskInfo)
        {
            return dal.BuildCreateTempTableStatement(taskInfo);
        }

        public string BuildUpdateTempTableStatement(TaskInfo taskInfo)
        {
            return dal.BuildUpdateTempTableStatement(taskInfo);
        }

        public string BuildInsertNewStatement(TaskInfo taskInfo)
        {
            return dal.BuildInsertNewStatement(taskInfo);
        }

        public string BuildUpdateExistingStatement(TaskInfo taskInfo)
        {
            return dal.BuildUpdateExistingStatement(taskInfo);
        }

        public string BuildDeleteExistingStatement(TaskInfo taskInfo)
        {
            return dal.BuildDeleteExistingStatement(taskInfo);
        }

        public bool ExecuteSqlScript(string sqlQuery, string connString, bool testRun)
        {
            return dal.ExecuteSqlScript(sqlQuery, connString, testRun);
        }

        /// <summary>
        /// Begin Tran XLETL_TRAN_{0}
        /// </summary>
        public string GetStartTranString { get { return dal.GetStartTranString; } }
        /// <summary>
        /// Commit Tran XLETL_TRAN_{0}
        /// </summary>
        public string GetCommitTranString { get { return dal.GetCommitTranString; } }
        /// <summary>
        /// Rollback Tran XLETL_TRAN_{0}
        /// </summary>
        public string GetRollbackTranString { get { return dal.GetRollbackTranString; } }
        /// <summary>
        /// Drop Table #XLETL_TEMP_{0}
        /// </summary>
        public string GetDropTableString { get { return dal.GetDropTableString; } }
        
    }
}
