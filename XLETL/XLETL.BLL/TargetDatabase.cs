﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using XLETL.DALFactory;
using XLETL.IDAL;
using XLETL.Model;

namespace XLETL.BLL
{
    public class TargetDatabase
    {
        private ITargetDatabase dal = null;

        public TargetDatabase(string dbProvider)
        {
            if (string.IsNullOrEmpty(dbProvider))
                throw new ArgumentNullException("dbProvider", "Parameter is null or empty");

            // Get an instance of the TargetDatabase DAL using the DALFactory
            dal = XLETL.DALFactory.DataAccess.CreateTargetDatabase(dbProvider);
        }        

        /// <summary>
        /// Get table list for particular database specified in the connection string
        /// </summary>
        /// <param name="connectionString">Connection string to the target database</param>
        public DataTable GetDatabaseTableList(string connectionString)
        {
            return dal.GetDatabaseTableList(connectionString);
        }

        /// <summary>
        /// Get column list with column attributes for the specified table
        /// </summary>
        /// <param name="connectionString">Connection string to the target
        /// database</param>
        /// <param name="tableName">Name of target table</param>
        public DataTable GetTableColumnList(string connectionString, string tableName)
        {
            return dal.GetColumnListByTable(tableName, connectionString, null);
        }

        public List<TargetColumnInfo> GetTargetColumnListByTableName(string connectionString, string tableName)
        {
            return dal.GetTargetColumnListByTableName(tableName, connectionString, null);
        }

        /// <summary>
        /// Get Target Column List by taskId
        /// </summary>
        /// <param name="taskId">Unique indentifier of a Task</param>
        public DataTable GetTargetColumnList(int taskId)
        {
            return null;
        }

        /// <summary>
        /// Get data from database table
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="tableName"></param>
        /// <param name="columnList"></param>
        /// <returns></returns>
        public DataTable GetDataOfTable(string connString, string tableName, string[] columnList)
        {
            if (string.IsNullOrEmpty(connString) || string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("connString or tableName");

            return dal.GetDataOfTable(connString, tableName, columnList);
        }
    }
}
