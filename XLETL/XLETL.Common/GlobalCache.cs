﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace XLETL.Common
{
    /// <summary>
    /// Contains methods common to all subsystems
    /// </summary>
    public class GlobalCache
    {
        public static string COMMON_STORAGE_FOLDER = "C:\\XLETL_STORAGE\\Storage";

        public static string DB_STORAGE_PATH = COMMON_STORAGE_FOLDER + "//Databases.xml";
        public static string USER_STORAGE_PATH = COMMON_STORAGE_FOLDER + "//Users.xml";
        public static string TASK_STORAGE_PATH = COMMON_STORAGE_FOLDER + "//Tasks.xml";
        public static string SCHEDULE_STORAGE_PATH = COMMON_STORAGE_FOLDER + "//Schedules.xml";
        public static string ADMIN_STORAGE_PATH = COMMON_STORAGE_FOLDER + "//Admin.xml";

        public static string PLUGIN_STORAGE_FOLDER = "C:\\XLETL_STORAGE\\Plugins";
        public static string TEMPLATE_STORAGE_FOLDER = "C:\\XLETL_STORAGE\\TemplateStorage";
        public static string UPLOAD_STORAGE_FOLDER = "C:\\XLETL_STORAGE\\UploadedXLData";

        public static string ERROR_LOGS_PATH = "C:\\XLETL_STORAGE\\Logs";
    }
}
