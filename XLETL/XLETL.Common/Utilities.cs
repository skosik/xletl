﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Data;
using System.IO;

namespace XLETL.Common
{
    public class Utilities
    {
        /// <summary>
        /// Method to return Description Attribute string from specified Enum's value
        /// </summary>
        /// <param name="enumType">Enum Type</param>
        /// <param name="value">Enum Value</param>
        /// <returns>string</returns>
        public static string GetDescriptionAttrFromEnum(Type enumType, object value)
        {
            FieldInfo fi = enumType.GetField(Enum.GetName(enumType, value));
            DescriptionAttribute dna =
                (DescriptionAttribute)Attribute.GetCustomAttribute(
                fi, typeof(DescriptionAttribute));

            if (dna != null)
                return dna.Description;
            else return value.ToString();
        }

        public static Type GetSystemType(SqlDbType sqlType)
        {
            Hashtable map = new Hashtable();
            map.Add(SqlDbType.BigInt, typeof(Int64));
            map.Add(SqlDbType.Binary, typeof(byte[]));
            map.Add(SqlDbType.Bit, typeof(Boolean));
            map.Add(SqlDbType.Char, typeof(String));
            map.Add(SqlDbType.DateTime, typeof(DateTime));
            map.Add(SqlDbType.Decimal, typeof(Decimal));
            map.Add(SqlDbType.Float, typeof(Double));
            map.Add(SqlDbType.Image, typeof(byte[]));
            map.Add(SqlDbType.Int, typeof(Int32));
            map.Add(SqlDbType.Money, typeof(Decimal));
            map.Add(SqlDbType.NChar, typeof(String));
            map.Add(SqlDbType.NText, typeof(String));
            map.Add(SqlDbType.NVarChar, typeof(String));
            map.Add(SqlDbType.Real, typeof(Single)); //not sure      
            map.Add(SqlDbType.SmallDateTime, typeof(DateTime));
            map.Add(SqlDbType.SmallInt, typeof(Int16));
            map.Add(SqlDbType.SmallMoney, typeof(Decimal));
            map.Add(SqlDbType.Text, typeof(String));
            map.Add(SqlDbType.Timestamp, typeof(DateTime));
            map.Add(SqlDbType.TinyInt, typeof(Byte));
            map.Add(SqlDbType.UniqueIdentifier, typeof(Guid));
            map.Add(SqlDbType.VarBinary, typeof(byte[]));
            map.Add(SqlDbType.VarChar, typeof(String));
            map.Add(SqlDbType.Variant, typeof(Object));//not sure  

            return (Type)map[sqlType];
        }

        /*public static void LogData(string message, string logPath)
        {
          StreamWriter _sw = null;

          try
          {
            // Instantiate an appendable streamwriter that writes to a file called ETLLog.txt
            _sw = new StreamWriter(logPath + "//ETLLog.txt", true, System.Text.Encoding.ASCII);
            // Write a row of data containing the time, the error message, and the stack trace
            _sw.WriteLine("* " + message);
          }
          finally
          {
            //Ensure the streamwriter gets closed
            if (_sw != null)
            {
              _sw.Close();
              _sw = null;
            }
          }
        }*/

        public static void LogData(string message, string logPath, DateTime etlTime)
        {
          StreamWriter _sw = null;

          string logFilePath = "";

          try
          {
            // Instantiate an appendable streamwriter that writes to a file called ETLLog.txt

            if (etlTime != null)
            {
              logFilePath = string.Format("{0}//ETLLog_{1:yyMMddHHmm}.txt", logPath, etlTime);
            }
            else
            {
              logFilePath = logPath + "//ETLLog.txt";
            }

            _sw = new StreamWriter(logFilePath, true, System.Text.Encoding.ASCII);
            // Write a row of data containing the time, the error message, and the stack trace
            _sw.WriteLine("* " + message);
          }
          finally
          {
            //Ensure the streamwriter gets closed
            if (_sw != null)
            {
              _sw.Close();
              _sw = null;
            }
          }
        }

        public static void LogError(Exception ex)
        {
          StreamWriter _sw = null;

          try
          {
            // Make sure the directory exist
            if (!Directory.Exists(GlobalCache.ERROR_LOGS_PATH))
            {
              Directory.CreateDirectory(GlobalCache.ERROR_LOGS_PATH);
            }

            // Instantiate an appendable streamwriter that writes to a file called ErrorLog.txt
            _sw = new StreamWriter(GlobalCache.ERROR_LOGS_PATH + "//ErrorLog.txt", true, System.Text.Encoding.ASCII);
            // Write a row of data containing the time, the error message, and the stack trace
            _sw.WriteLine("* " + DateTime.Now.ToString() + " | " + ex.Message + " | " + ex.StackTrace);
          }
          finally
          {
            //Ensure the streamwriter gets closed
            if (_sw != null)
            {
              _sw.Close();
              _sw = null;
            }
          }

        }
    }
}