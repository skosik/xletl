﻿using System.Reflection;
using System.Configuration;

namespace XLETL.DALFactory
{
    /// <summary>
    /// This class is implemented following the Abstract Factory pattern to create the DAL implementation
    /// for the specified database provider
    /// </summary>
    public sealed class DataAccess
    {        
        private DataAccess() { }

        public static XLETL.IDAL.ITargetDatabase CreateTargetDatabase(string assemblyPath)
        {
            // TODO: should be used the dbtypepath parameter
            string className = assemblyPath + ".TargetDatabase";
            return (XLETL.IDAL.ITargetDatabase)Assembly.Load(assemblyPath).CreateInstance(className);
        }

        public static XLETL.IDAL.IETL CreateETL(string assemblyPath)
        {
            string className = assemblyPath + ".ETL";
            return (XLETL.IDAL.IETL)Assembly.Load(assemblyPath).CreateInstance(className);
        }
        
    }
}
