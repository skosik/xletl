using System;
using System.Web;

/// <summary>
/// Implements ASP unhandled exception manager as a HttpModule
/// </summary>
/// <remarks>
/// to use:
///    1) place ASPUnhandledException.dll in the \bin folder
///    2) add this section to your Web.config under the &lt;system.web&gt; element:
///         &lt;httpModules&gt;
///	 	        &lt;add name="ASPUnhandledException" 
///                 type="ASPUnhandledException.UehHttpModule, ASPUnhandledException" /&gt;
///		    &lt;/httpModules&gt;
///
///  Jeff Atwood
///  http://www.codinghorror.com/
/// </remarks>
public class UehHttpModule : IHttpModule
{
	void IHttpModule.Init( HttpApplication app )
	{
		app.Error += new System.EventHandler( OnError );
	}

	void IHttpModule.Dispose()
	{
	}

	protected virtual void OnError( object sender, EventArgs args )
	{
		HttpApplication app = (HttpApplication)sender;
		WebExceptionHandler.HandleException( app.Server.GetLastError(), true );
	}
}