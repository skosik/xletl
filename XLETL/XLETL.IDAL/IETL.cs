﻿using System;
using System.Collections.Generic;
using System.Text;
using XLETL.Model;

namespace XLETL.IDAL
{
    public interface IETL
    {
        string BuildCreateTempTableStatement(TaskInfo taskInfo);
        string BuildInsertStatement(TaskInfo taskInfo, bool isForTemp);
        string BuildUpdateTempTableStatement(TaskInfo taskInfo);
        string BuildInsertNewStatement(TaskInfo taskInfo);
        string BuildUpdateExistingStatement(TaskInfo taskInfo);
        string BuildDeleteExistingStatement(TaskInfo taskInfo);
        string GetStartTranString { get; }
        string GetCommitTranString { get; }
        string GetRollbackTranString { get; }
        string GetDropTableString { get; }        
        bool ExecuteSqlScript(string sqlQuery, string connString, bool testRun);
    }
}
