﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using XLETL.Model;

namespace XLETL.IDAL
{
    public interface ITargetDatabase
    {
        /// <summary>
        /// Method to get table list
        /// </summary>
        /// <param name="connectionString">Connection string to the target database</param>
        DataTable GetDatabaseTableList(string connectionString);

        DataTable GetColumnListByTable(string tableName, string connString, string ColumnCommaDelimitedString);

        List<TargetColumnInfo> GetTargetColumnListByTableName(string tableName, string connString, string ColumnCommaDelimitedString);

        DataTable GetDataOfTable(string connString, string tableName, string[] columns);
    }
}
