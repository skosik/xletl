﻿using System;

namespace XLETL.Model
{
  /// <summary>
  /// Business entity used to model an admin
  /// </summary>
  [Serializable]
  public class AdminInfo
  {
    #region Constants
    public const string ADMIN_INFO = "Admin";
    public const string ADMIN_COL = "Admins";
    public const string ADMIN_ID = "ID";
    public const string USERNAME = "Username";
    public const string PASSWORD = "Password";    
    #endregion

    #region Constructor(s)
    public AdminInfo() { }
    #endregion

    #region Fields / Properties
    public Guid Id { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
    #endregion
  }
}
