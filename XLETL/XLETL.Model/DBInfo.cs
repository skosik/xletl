﻿using System;
using System.Collections.Generic;

namespace XLETL.Model
{
    /// <summary>
    /// Business entity used to model database
    /// </summary>
    [Serializable]
    public class DBInfo
    {
        #region Constants
        public const string DB_INFO = "Database";
        public const string DB_COL = "Databases";
        public const string DB_ID = "ID";
        public const string DB_FRIENDLY_NAME = "FriendlyName";
        public const string DB_DESC = "Description";
        public const string CONN_STRING = "ConnString";
        public const string DATABASE_NAME = "DBName";
        public const string DAL_CLASSNAME = "DALClassName";
        #endregion

        #region Fields
        private Guid _dbId;
        private string _dbFriendlyName;
        private string _dbDesc;
        private string _databaseName;
        private string _connString;
        private string _dalClassName;
        #endregion

        #region Constructor(s)
        public DBInfo() { }
        #endregion

        #region Properties
        public Guid DBId
        {
            get
            {
                return _dbId;
            }
            set
            {
                _dbId = value;
            }
        }
        
        public string DBFriendlyName
        {
            get
            {
                return _dbFriendlyName;
            }
            set
            {
                _dbFriendlyName = value;
            }
        }

        public string DBDesc
        {
            get
            {
                return _dbDesc;
            }
            set
            {
                _dbDesc = value;
            }
        }

        public string DatabaseName
        {
            get
            {
                return _databaseName;
            }
            set
            {
                _databaseName = value;
            }
        }

        public string ConnString
        {
            get
            {
                return _connString;
            }
            set
            {
                _connString = value;
            }
        }

        public string DALClassName
        {
            get
            {
                return _dalClassName;
            }
            set
            {
                _dalClassName = value;
            }
        }

        public IList<UserInfo> UserInfoList { get; set; }
        public IList<TaskInfo> TaskInfoList { get; set; }
        #endregion
    }
}
