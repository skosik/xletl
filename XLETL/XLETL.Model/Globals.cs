﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace XLETL.Model
{
    public class Globals
    {
        //public enum Providers
        //{
        //    SqlServer = 1,
        //    Oracle
        //}

        public enum LoadMode
        {
            [Description("Insert All")]
            InsertAll,
            [Description("Insert New")]
            InsertNew,
            [Description("Insert or Update")]
            InsertOrUpdate,
            [Description("Insert or Delete")]
            InsertOrDelete,
            [Description("Update")]
            Update,
            [Description("Delete")]
            Delete
        }

        public enum CellStyle
        {
            [Description("Cell")]
            Cell,
            [Description("Combobox")]
            Combobox,
            [Description("Checkbox")]
            Checkbox
        }

        public enum StatusType
        {
            [Description("Ready")]
            Ready,
            [Description("Aborted")]
            Aborted,
            [Description("Canceled")]
            Canceled
        }

        public enum ColumnType
        {
            [Description("System.Int32")]
            Numeric,
            [Description("System.String")]
            String,
            [Description("System.Boolean")]
            Boolean,
            [Description("System.DateTime")]
            DateTime                        
        }

        public enum PluginTarget
        {
          Target,
          Source,
          Both
        }
    }
}
