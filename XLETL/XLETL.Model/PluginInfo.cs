﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace XLETL.Model
{
    [Serializable]
    public class Parameter
    {
        public string ParameterName { get; set; }
        public string ParameterType { get; set; }
        public string ParameterValue { get; set; }
    }

    [Serializable]
    public class PluginInfo
    {
        #region Constants
        public const string PLUGIN_INFO = "Plugin";
        public const string PLUGIN_TARGET = "Target";
        public const string PLUGIN_DESC = "Description";
        public const string PLUGIN_TYPE = "Type";
        public const string PLUGIN_NAME = "Name";
        public const string PLUGIN_ASSEMBLY = "AssemblyFile";
        public const string PLUGIN_INSTALL_PATH = "InstallPath";
        public const string PLUGIN_PARAMS = "PluginParams";
        public const string PLUGIN_PARAM = "PluginParam";
        public const string PLUGIN_TRANS_PARAMS = "TransformationParams";
        public const string PLUGIN_TRANS_PARAM = "TransformationParam";
        public const string PARAM_NAME = "ParamName";
        public const string PARAM_TYPE = "ParamType";
        public const string PARAM_VALUE = "ParamValue";

        public const string PARAM_GLOBAL_DB_CONN_STRING = "GlobalDBConnString";
        public const string PARAM_GLOBAL_FOREIGN_TABLE_NAME = "GlobalForeignTableName";
        #endregion

        private string _name;
        [XmlAttribute("Name")]
        public string Name
        {
            get
            {
                if (string.IsNullOrEmpty(_name))
                    return "No Transformation Control";
                else return _name;
            }
            set
            {
                _name = value;
            }
        }

        [XmlAttribute("Description")]
        public string Description { get; set; }

        [XmlAttribute("Type")]
        public string Type { get; set; }

        [XmlAttribute("AssemblyFile")]
        public string AssemblyFile { get; set; }

        [XmlAttribute("InstallPath")]
        public string InstallPath { get; set; }

        [XmlAttribute("Target")]
        public string Target { get; set; }

        [XmlArray(IsNullable = true, ElementName = "PluginParameters")]
        [XmlArrayItem(typeof(Parameter), ElementName = "PluginParameter")]
        public Parameter[] PluginParamList { get; set; }

        [XmlArray(IsNullable = true, ElementName = "TransformationParams")]
        [XmlArrayItem(typeof(Parameter), ElementName = "TransformationParam")]
        public Parameter[] TransformationParamList { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
