﻿using System;
using System.Text;

namespace XLETL.Model
{
    /// <summary>
    /// Business entity used to model a schedule
    /// </summary>
    [Serializable]
    public class ScheduleInfo
    {
        public const string SCHEDULE = "Schedule";
        public const string SCHEDULES = "Schedules";
        public const string SCHEDULE_ID = "ID";
        public const string TASK_ID = "TaskID";
        public const string DAYILY = "Dayly";
        public const string WEEKLY = "Weekly";
        public const string WEEKDAYS = "WeekDays";
        public const string WEEKDAY = "WeekDay";
        //public const string MON = "Mon";
        //public const string TUE = "Tue";
        //public const string WEN = "Wen";
        //public const string THU = "Thu";
        //public const string FRI = "Fri";
        //public const string SAT = "Sat";
        //public const string SUN = "Sun";
        public const string HOUR = "Hour";
        public const string MIN = "Min";

        //public bool Mon { get; set; }
        //public bool Tue { get; set; }
        //public bool Wen { get; set; }
        //public bool Thu { get; set; }
        //public bool Fri { get; set; }
        //public bool Sat { get; set; }
        //public bool Sun { get; set; }

        public int Hour { get; set; }
        public int Min { get; set; }
        
        /*private bool _weekly = false;
        private bool _dayly = false;
        private bool _mon = false;
        private bool _tue = false;
        private bool _wen = false;
        private bool _thu = false;
        private bool _fri = false;
        private bool _sat = false;
        private bool _sun = false;
        private int _hour = 0;
        private int _min = 0;*/

        public Guid TaskID { get; set; }
        public Guid ID { get; set; }
        public string[] WeekDays { get; set; }

        public ScheduleInfo() { }

        public bool Weekly { get; set; }        

        public bool Dayly { get; set; }

        public override string ToString()
        {
          StringBuilder sb = new StringBuilder();
          sb.Append((Dayly) ? "Daily - " : "");
          sb.Append((Weekly) ? "Weekly - " : "");
          sb.AppendFormat("{0:00}:{1:00} ", Hour, Min);
          if (Weekly)
          {
            foreach(string dow in WeekDays)
            {
               sb.AppendFormat("{0},", dow.Substring(0, 3));
            }
          //sb.Append((Mon) ? "Mon," : "");
          //sb.Append((Tue) ? "Tue," : "");
          //sb.Append((Wen) ? "Wen," : "");
          //sb.Append((Thu) ? "Thu," : "");
          //sb.Append((Fri) ? "Fri," : "");
          //sb.Append((Sat) ? "Sat," : "");
          //sb.Append((Sun) ? "Sun," : "");
          sb.Remove(sb.Length - 1, 1); // remove last ,
          }         
          
          return sb.ToString(); 
        }

    }
}
