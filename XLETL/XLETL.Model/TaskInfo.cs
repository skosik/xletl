﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using XLETL.Model.Convertors;

namespace XLETL.Model
{
    /// <summary>
    /// Business entity used to model a task
    /// </summary>
    [Serializable]
    public class TaskInfo
    {
        #region Constants
        public const string TASK = "Task";
        public const string TASKS = "Tasks";
        public const string TASK_ID = "ID";              
        public const string DATABASE_ID = "DatabaseID";
        public const string PRELOAD_SQLSCRIPT = "PreloadSqlScript";
        public const string POSTLOAD_SQLSCRIPT = "PostloadSqlScript";
        public const string TARGET_TABLE_NAME = "TargetTableName";        
        public const string TASK_STATUS = "TaskStatus";
        public const string TEMPLATE_PATH = "TemplatePath";
        public const string TASK_SCHEDULE = "TaskSchedule";
        public const string FILE_UPLOAD_PATH = "FileUploadPath";

        public const string TASK_DETAILS = "TaskDetails";  
        public const string SOURCE_COLUMNS = "SourceColoumns";
        public const string TARGET_COLUMNS = "TargetColumns";
        public const string MAPPED_COLUMNS = "MappedColumns";                
        
        #endregion

        #region Fields / Properties
        //private bool isChanged;
        //private int id;
        //private string name;
        //private string description;
        //private string targetTableName;
        //private string sourceTableName;
        //private int taskModeTypeID;
        //private int taskStatusTypeID;
        //private int preLoadSQLScriptID;
        //private int postLoadSQLScriptID;
        private List<MappingInfo> _columnMapping = new List<MappingInfo>();
        private List<SourceColumnInfo> _sourceColumns = new List<SourceColumnInfo>();
        private List<TargetColumnInfo> _targetColumns = new List<TargetColumnInfo>();
        private TaskDetailsInfo _taskDetails = new TaskDetailsInfo();        

        [Browsable(false)]
        public Guid ID { get; set; }
        public string TargetTableName { get; set; }        
        public Globals.StatusType TaskStatusType { get; set; }
        public string PreLoadSQLScriptID { get; set; }
        public string PostLoadSQLScriptID { get; set; }
        public Guid DatabaseID { get; set; }
        [Browsable(false)]
        public string TemplatePath { get; set; }
        public ScheduleInfo TaskSchedule { get; set; }
        public string FileUploadPath { get; set; }
                
        public List<MappingInfo> ColumnMapping 
        {
            get
            {
                return _columnMapping;
            }
            set
            {
                _columnMapping = value;
            }
        }

        public List<SourceColumnInfo> SourceColumns
        {
            get
            {
                return _sourceColumns;
            }
            set
            {
                _sourceColumns = value;
            }
        }

        public List<TargetColumnInfo> TargetColumns
        {
            get
            {
                return _targetColumns;
            }
            set
            {
                _targetColumns = value;
            }
        }

        public TaskDetailsInfo TaskDetails
        {
            get
            {
                return _taskDetails;
            }
            set
            {
                _taskDetails = value;
            }
        }
        #endregion

        #region Constructor(s)
        public TaskInfo() { }
        #endregion
    }

    [Serializable]
    [Category()]
    public class TaskDetailsInfo
    {
        #region Constants
        public const string TASK_NAME = "Name";
        public const string TASK_DESC = "Description";
        public const string TASK_PROCESS_MODE = "ProcessMode";
        public const string SOURCE_SHEET_NAME = "SourceSheetName";
        #endregion

        #region Fields / Properties
        [Description("Name of ETL task")]
        public string Name { get; set; }        
        [Description("Description of ETL task intention")]
        public string Description { get; set; }
        [DisplayName("Process Mode")]
        [Description("Define the task processing mode as Insert All, Insert New, Insert or Update, Insert or Delete, Update, Delete.")]        
        [TypeConverter(typeof(EnumTypeConvertor))]
        public Globals.LoadMode ProcessMode { get; set; }
        [DisplayName("Sheet Name")]
        [Description("Template Sheet Name")]
        public string SourceSheetName { get; set; }
        #endregion
    }

    [Serializable]
    public class SourceColumnInfo
    {
        #region Constants        
        public const string SOURCE_COLUMN = "SourceColoumn";
        public const string SOURCE_COL_ID = "ID";
        public const string SOURCE_TARGET_COLUMN_ID = "TargetColumnID";
        public const string SOURCE_COL_NAME = "Name";
        public const string SOURCE_COL_TYPE = "Type";
        public const string SOURCE_COL_FORMAT = "NumberFormat";
        public const string SOURCE_COL_STYLE = "Style";
        public const string SOURCE_COL_TRANSFORMATIONS = "Transformations";
        public const string SOURCE_COL_KEY_FIELD = "KeyField";

        #endregion
        #region Fields / Properties
        [Browsable(false)]
        public Guid ID { get; set; }
        [Browsable(false)]
        public Guid TargetColumnID { get; set; }

        [DisplayName("Column Heading")]
        public string Name { get; set; }
        [DisplayName("Column Type")]
        public Globals.ColumnType Type { get; set; }
        
        [DisplayName("Cell Style")]
        public Globals.CellStyle Style { get; set; }

        [DisplayName("Key Field")]
        [Description("Select the field of the table to be used as the key field during load process")]
        public bool IsKeyField { get; set; }

        private string _numFormat = "";
        [DisplayName("Number Format")]
        public string NumberFormat { get { return _numFormat; } set { _numFormat = value; } }

        private List<PluginInfo> _transformations = new List<PluginInfo>();
        [Browsable(false)]
        public List<PluginInfo> Transformations
        {
            get
            {
                return _transformations;
            }
            set
            {
                _transformations = value;
            }
        }
        #endregion
    }

    [Serializable]
    public class TargetColumnInfo
    {
        #region Constants
        public const string TARGET_COLUMN = "TargetColumn";
        public const string TARGET_COL_ID = "ID";
        public const string TARGET_COL_NAME = "Name";
        public const string TARGET_COL_TYPE = "Type";
        public const string TARGET_COL_SIZE = "Size";
        public const string TARGET_COL_IDENTITY = "Identity";
        public const string TARGET_COL_DEFAULT_NULL = "DefaultNull";        
        public const string TARGET_COL_PRIMARYKEY = "PrimaryKey";
        public const string TARGET_COL_FOREIGNKEY = "ForeignKey";
        public const string TARGET_COL_CONSTRAINT_NAME = "ConstraintName";
        public const string TARGET_COL_PR_TO_FR_TABLE_NAME = "PRToFRTableName";
        public const string TARGET_COL_ORDER_INDEX = "OrderIndex";
        public const string TARGET_COL_DEFAULT_VALUE = "DefaulValue";
        public const string TARGET_COL_TRANSFORMATIONS = "Transformations";
        public const string TARGET_COL_TRANS_PLUGIN = "TransformationPlugin";
        #endregion

        #region Fields / Properties
        [Browsable(false)]
        public Guid ID { get; set; }

        [ReadOnly(true)]
        [Browsable(true)]
        [DisplayName("Name")]
        public string Name { get; set; }
        [ReadOnly(true)]
        public string Type { get; set; }
        [ReadOnly(true)]
        public int Size { get; set; }
        [ReadOnly(true)]
        [DisplayName("Identity")]
        public bool IsIdentity { get; set; }
        [ReadOnly(true)]
        [DisplayName("Nullable")]
        public bool IsDefaultNull { get; set; }        
        [ReadOnly(true)]
        [DisplayName("Primary Key")]
        public bool IsPrimaryKey { get; set; }
        [ReadOnly(true)]
        [DisplayName("Foreign Key")]
        public bool IsForeignKey { get; set; }
        [ReadOnly(true)]
        [DisplayName("Constraint")]
        public string ConstraintName { get; set; }
        [ReadOnly(true)]
        [DisplayName("Primary Table")]
        public string PrimaryToForeignTableName { get; set; }
        [Browsable(false)]
        public int OrderIndex { get; set; }
        [Browsable(false)]
        public object DefaultValue { get; set; }
        private List<PluginInfo> _transformations = new List<PluginInfo>();
        [Browsable(false)]
        public List<PluginInfo> Transformations
        {
            get
            {
                return _transformations;
            }
            set
            {
                _transformations = value;
            }
        }
        #endregion
    }

    /// <summary>
    /// TODO: check if it is referenced somewhere and if no then remove it.
    /// </summary>
    [Serializable]
    public class MappingInfo
    {
        #region Constants
        public const string COLUMN_MAPPING = "ColumnMapping";
        public const string SOURCE_COLUMN_ID = "SourceColumnID";
        public const string TARGET_COLUMN_ID = "TargetColumnID";
        #endregion

        #region Fields / Properties
        public Guid SourceColumnID { get; set; }
        public Guid TargetColumnID { get; set; }
        //public string Type { get; set; }
        #endregion
    }
}