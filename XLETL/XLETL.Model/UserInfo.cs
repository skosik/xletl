﻿using System;

namespace XLETL.Model
{
    /// <summary>
    /// Business entity used to model a user
    /// </summary>
    [Serializable]
    public class UserInfo
    {
        #region Constants
        public const string USER_INFO = "User";
        public const string USER_COL = "Users";
        public const string USER_ID = "ID";
        public const string USERNAME = "Username";
        public const string PASSWORD = "Password";
        public const string FIRST_NAME = "FirstName";
        public const string LAST_NAME = "LastName";
        public const string EMAIL = "Email";
        public const string PHONE = "Phone";
        public const string DATABASE_ID = "DatabaseID";
        #endregion

        #region Constructor(s)
        public UserInfo() { }
        #endregion

        #region Fields / Properties
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Guid DatabaseID { get; set; }
        #endregion
    }
}
