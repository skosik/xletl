﻿namespace XLETL.Scheduler
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.timer1 = new System.Windows.Forms.Timer(this.components);
      this.label1 = new System.Windows.Forms.Label();
      this.lblTotalTasks = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.tbLog = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.lblTaskNumInQueue = new System.Windows.Forms.Label();
      this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
      this.label2 = new System.Windows.Forms.Label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.btnHide = new System.Windows.Forms.Button();
      this.btnExit = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.lblTaskName = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.lblDb = new System.Windows.Forms.Label();
      this.lblTaskDbName = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.lblLatSyncTime = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // timer1
      // 
      this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(95, 21);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(179, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Total ETL Tasks in Process Queue: ";
      // 
      // lblTotalTasks
      // 
      this.lblTotalTasks.AutoSize = true;
      this.lblTotalTasks.Location = new System.Drawing.Point(301, 21);
      this.lblTotalTasks.Name = "lblTotalTasks";
      this.lblTotalTasks.Size = new System.Drawing.Size(13, 13);
      this.lblTotalTasks.TabIndex = 1;
      this.lblTotalTasks.Text = "0";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 188);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(119, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "Last Task Process Log:";
      // 
      // tbLog
      // 
      this.tbLog.Location = new System.Drawing.Point(13, 206);
      this.tbLog.Multiline = true;
      this.tbLog.Name = "tbLog";
      this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.tbLog.Size = new System.Drawing.Size(429, 170);
      this.tbLog.TabIndex = 3;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(21, 21);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(161, 13);
      this.label4.TabIndex = 4;
      this.label4.Text = "Task Number in Process Queue:";
      // 
      // lblTaskNumInQueue
      // 
      this.lblTaskNumInQueue.AutoSize = true;
      this.lblTaskNumInQueue.Location = new System.Drawing.Point(217, 21);
      this.lblTaskNumInQueue.Name = "lblTaskNumInQueue";
      this.lblTaskNumInQueue.Size = new System.Drawing.Size(13, 13);
      this.lblTaskNumInQueue.TabIndex = 5;
      this.lblTaskNumInQueue.Text = "0";
      // 
      // notifyIcon1
      // 
      this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
      this.notifyIcon1.Text = "XLETL Scheduler";
      this.notifyIcon1.Visible = true;
      this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.Color.Red;
      this.label2.Location = new System.Drawing.Point(140, 167);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(204, 15);
      this.label2.TabIndex = 6;
      this.label2.Text = "Task is being processed now...";
      this.label2.Visible = false;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
      this.pictureBox1.Location = new System.Drawing.Point(13, 21);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(55, 50);
      this.pictureBox1.TabIndex = 7;
      this.pictureBox1.TabStop = false;
      // 
      // btnHide
      // 
      this.btnHide.Location = new System.Drawing.Point(149, 383);
      this.btnHide.Name = "btnHide";
      this.btnHide.Size = new System.Drawing.Size(75, 19);
      this.btnHide.TabIndex = 8;
      this.btnHide.Text = "Hide";
      this.btnHide.UseVisualStyleBackColor = true;
      this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
      // 
      // btnExit
      // 
      this.btnExit.Location = new System.Drawing.Point(230, 383);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(75, 19);
      this.btnExit.TabIndex = 9;
      this.btnExit.Text = "Exit";
      this.btnExit.UseVisualStyleBackColor = true;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.lblTaskName);
      this.groupBox1.Controls.Add(this.label8);
      this.groupBox1.Controls.Add(this.lblDb);
      this.groupBox1.Controls.Add(this.lblTaskDbName);
      this.groupBox1.Controls.Add(this.lblTaskNumInQueue);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Location = new System.Drawing.Point(85, 69);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(357, 90);
      this.groupBox1.TabIndex = 10;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Next Processing ETL Task Details ";
      // 
      // lblTaskName
      // 
      this.lblTaskName.Location = new System.Drawing.Point(116, 66);
      this.lblTaskName.Name = "lblTaskName";
      this.lblTaskName.Size = new System.Drawing.Size(235, 14);
      this.lblTaskName.TabIndex = 9;
      this.lblTaskName.Text = "0";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(22, 65);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(65, 13);
      this.label8.TabIndex = 8;
      this.label8.Text = "Task Name:";
      // 
      // lblDb
      // 
      this.lblDb.AutoSize = true;
      this.lblDb.Location = new System.Drawing.Point(21, 43);
      this.lblDb.Name = "lblDb";
      this.lblDb.Size = new System.Drawing.Size(90, 13);
      this.lblDb.TabIndex = 7;
      this.lblDb.Text = "Target Database:";
      // 
      // lblTaskDbName
      // 
      this.lblTaskDbName.Location = new System.Drawing.Point(116, 44);
      this.lblTaskDbName.Name = "lblTaskDbName";
      this.lblTaskDbName.Size = new System.Drawing.Size(235, 14);
      this.lblTaskDbName.TabIndex = 6;
      this.lblTaskDbName.Text = "0";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(96, 39);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(199, 13);
      this.label5.TabIndex = 11;
      this.label5.Text = "Last Time of ETL Tasks synchronisation:";
      // 
      // lblLatSyncTime
      // 
      this.lblLatSyncTime.AutoSize = true;
      this.lblLatSyncTime.Location = new System.Drawing.Point(301, 39);
      this.lblLatSyncTime.Name = "lblLatSyncTime";
      this.lblLatSyncTime.Size = new System.Drawing.Size(49, 13);
      this.lblLatSyncTime.TabIndex = 12;
      this.lblLatSyncTime.Text = "00:00:00";
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(454, 411);
      this.Controls.Add(this.lblLatSyncTime);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.lblTotalTasks);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.btnExit);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.btnHide);
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.tbLog);
      this.Controls.Add(this.label3);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "MainForm";
      this.Text = "XLETL Scheduler";
      this.Resize += new System.EventHandler(this.Form1_Resize);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label lblTotalTasks;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox tbLog;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label lblTaskNumInQueue;
    private System.Windows.Forms.NotifyIcon notifyIcon1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Button btnHide;
    private System.Windows.Forms.Button btnExit;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label lblLatSyncTime;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label lblDb;
    private System.Windows.Forms.Label lblTaskDbName;
    private System.Windows.Forms.Label lblTaskName;
  }
}

