﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using XLETL.ExceptionHandling;

namespace XLETL.Scheduler
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
      ExceptionHandler.AddHandler(true, true, false);

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new MainForm());
    }
  }
}
