using System;
using System.Collections.Generic;

namespace XLETL.Scheduler
{
	/// <summary>
	/// ScheduleTask class that contains methods and properies required for Task scheduling functionality
	/// </summary>
	public class ScheduleTask
	{
		private static TaskSchedule[] scheduleQueue; // hold the list of task schedule objects

		// Current Task Index
		protected static int currentTaskIndex	= -1;
    public static bool isEndOfTaskList = false;
    
		/// <summary>
		/// Check if the Task should be run
		/// </summary>
		/// <returns>true if task should be run, false otherwise</returns>
    public static bool RunTask()
    {
      bool bResult = false;

      if (currentTaskIndex == -1)
        return bResult;

      // Get the Current Time
      TimeSpan spanCurrent = DateTime.Now.TimeOfDay;

      // Check if the schedule's time equals to the current time (ignore seconds)       
      if (spanCurrent.Hours == scheduleQueue[currentTaskIndex].RunTime.Hour && spanCurrent.Minutes == scheduleQueue[currentTaskIndex].RunTime.Minute)
      {
        bResult = true;
      }

      return bResult;
    }

		/// <summary>
		/// Gets or Sets the Task Queue
		/// </summary>
		public static TaskSchedule[] TaskQueue
		{
			get
			{
				return ScheduleTask.scheduleQueue;
			}
			set
			{
        ScheduleTask.scheduleQueue = value;
			}
		}    

    /// <summary>
		/// Gets or Sets the Current Task ID
		/// </summary>
		public static int CurrentTaskID
		{
			get
			{
				return ScheduleTask.currentTaskIndex;
			}
			set
			{
				ScheduleTask.currentTaskIndex = value;
			}
		}

		/// <summary>
		/// Set the Initial Task to Run
		/// </summary>
		/// <param name="iTaskScheduleID"></param>
		/// <returns></returns>
		public static void SetInitialTask()
		{
      // Get the Current Time
      TimeSpan spanCurrent = DateTime.Now.TimeOfDay;

      int hour = 0;
      int min = 0;

      // Check if the schedule's time equals to the current time (ignore seconds)
      for (int i = 0; i < scheduleQueue.Length; i++)
      {
        hour = scheduleQueue[i].RunTime.Hour;
        min = scheduleQueue[i].RunTime.Minute;

        // If we found it then store the task index and exit the loop
        if (spanCurrent.Hours <= hour)
        {
          // check for minutes if hours are equal and if current time's minutes are greater then continue loop
          if (spanCurrent.Hours == hour && spanCurrent.Minutes > min)
            continue;

          currentTaskIndex = i;
          break;
        }        
      }		
		}

    /// <summary>
    /// Method to set the next task index
    /// </summary>
    public static void SetNextTask()
    {
      // Make sure we stay within Task schedule array bound
      if (currentTaskIndex + 1 < scheduleQueue.Length)
      {
        currentTaskIndex++;
      }
      else
      {
        // We have reached the end of Task Schedule list
        currentTaskIndex = -1;
        isEndOfTaskList = true;
      }
    }
		
	}

  /// <summary>
  /// Struct object to hold TaskID and Run Time
  /// </summary>
  public struct TaskSchedule
  {
    public Guid TaskID;
    public DateTime RunTime;

    public TaskSchedule(Guid taskId, DateTime runTime)
    {
      TaskID = taskId;
      RunTime = runTime;
    }
  }
}
