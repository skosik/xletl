﻿namespace XLETL.TaskManager.Controls
{
    partial class DBConnector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlAccess = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkAccessWork = new System.Windows.Forms.CheckBox();
            this.grbAccessWorkGroup = new System.Windows.Forms.GroupBox();
            this.txtAccessFileWork = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnPathAccessWork = new System.Windows.Forms.Button();
            this.txtAccessWorkPassword = new System.Windows.Forms.TextBox();
            this.txtAccessPassword = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAccessUserName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnBrowseFileAccess = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.txtAccessFile = new System.Windows.Forms.TextBox();
            this.cbDatabaseSources = new System.Windows.Forms.ComboBox();
            this.btnTestConn = new System.Windows.Forms.Button();
            this.pnlSqlExpress = new System.Windows.Forms.Panel();
            this.cbSqlExpressName = new System.Windows.Forms.ComboBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.btnBrowseFile = new System.Windows.Forms.Button();
            this.grbconnexion = new System.Windows.Forms.GroupBox();
            this.chkSqlExpressSavePass = new System.Windows.Forms.CheckBox();
            this.txtSqlExpressPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtSqlExpressUserName = new System.Windows.Forms.TextBox();
            this.lblUsername = new System.Windows.Forms.Label();
            this.rbAuthSQLExpress = new System.Windows.Forms.RadioButton();
            this.rbAuthWinSQLExpress = new System.Windows.Forms.RadioButton();
            this.Label2 = new System.Windows.Forms.Label();
            this.TxtSqlExpressFile = new System.Windows.Forms.TextBox();
            this.pnlSqlServer = new System.Windows.Forms.Panel();
            this.cbSqlServerName = new System.Windows.Forms.ComboBox();
            this.cbDatabases = new System.Windows.Forms.ComboBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.chkSqlServerSavePass = new System.Windows.Forms.CheckBox();
            this.txtSqlServerPassword = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtSqlServerUserName = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.rbAuthSQLServer = new System.Windows.Forms.RadioButton();
            this.rbAuthWinSQLServer = new System.Windows.Forms.RadioButton();
            this.Label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnOptions = new System.Windows.Forms.Button();
            this.pnlSQLCE = new System.Windows.Forms.Panel();
            this.cbSqlCeFile = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkSqlCESavePass = new System.Windows.Forms.CheckBox();
            this.txtSqlCePassword = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSqlCeUsername = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.pnlOptions = new System.Windows.Forms.Panel();
            this.grbAdvancedOptions = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.nudMaxDbSize = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.nudLockEscalation = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.grbNetworkProperties = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cmbProtocol = new System.Windows.Forms.ComboBox();
            this.nudPacketSize = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.chbEncryptConn = new System.Windows.Forms.CheckBox();
            this.nudExecTimeout = new System.Windows.Forms.NumericUpDown();
            this.nudConnTimeout = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.pnlAccess.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grbAccessWorkGroup.SuspendLayout();
            this.pnlSqlExpress.SuspendLayout();
            this.grbconnexion.SuspendLayout();
            this.pnlSqlServer.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.pnlSQLCE.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.pnlOptions.SuspendLayout();
            this.grbAdvancedOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxDbSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLockEscalation)).BeginInit();
            this.grbNetworkProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPacketSize)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExecTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudConnTimeout)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlAccess
            // 
            this.pnlAccess.BackColor = System.Drawing.Color.Transparent;
            this.pnlAccess.Controls.Add(this.groupBox2);
            this.pnlAccess.Controls.Add(this.btnBrowseFileAccess);
            this.pnlAccess.Controls.Add(this.Label1);
            this.pnlAccess.Controls.Add(this.txtAccessFile);
            this.pnlAccess.Location = new System.Drawing.Point(350, 21);
            this.pnlAccess.Name = "pnlAccess";
            this.pnlAccess.Size = new System.Drawing.Size(290, 182);
            this.pnlAccess.TabIndex = 85;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.chkAccessWork);
            this.groupBox2.Controls.Add(this.grbAccessWorkGroup);
            this.groupBox2.Controls.Add(this.txtAccessPassword);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtAccessUserName);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(10, 31);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(270, 148);
            this.groupBox2.TabIndex = 60;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Connect to Database";
            // 
            // chkAccessWork
            // 
            this.chkAccessWork.AutoSize = true;
            this.chkAccessWork.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAccessWork.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.chkAccessWork.Location = new System.Drawing.Point(12, 64);
            this.chkAccessWork.Name = "chkAccessWork";
            this.chkAccessWork.Size = new System.Drawing.Size(98, 17);
            this.chkAccessWork.TabIndex = 7;
            this.chkAccessWork.Text = "Use workgroup";
            this.chkAccessWork.UseVisualStyleBackColor = true;
            // 
            // grbAccessWorkGroup
            // 
            this.grbAccessWorkGroup.BackColor = System.Drawing.SystemColors.Control;
            this.grbAccessWorkGroup.Controls.Add(this.txtAccessFileWork);
            this.grbAccessWorkGroup.Controls.Add(this.label11);
            this.grbAccessWorkGroup.Controls.Add(this.btnPathAccessWork);
            this.grbAccessWorkGroup.Controls.Add(this.txtAccessWorkPassword);
            this.grbAccessWorkGroup.Location = new System.Drawing.Point(10, 79);
            this.grbAccessWorkGroup.Name = "grbAccessWorkGroup";
            this.grbAccessWorkGroup.Size = new System.Drawing.Size(248, 66);
            this.grbAccessWorkGroup.TabIndex = 6;
            this.grbAccessWorkGroup.TabStop = false;
            // 
            // txtAccessFileWork
            // 
            this.txtAccessFileWork.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccessFileWork.Location = new System.Drawing.Point(52, 39);
            this.txtAccessFileWork.Name = "txtAccessFileWork";
            this.txtAccessFileWork.Size = new System.Drawing.Size(152, 20);
            this.txtAccessFileWork.TabIndex = 69;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(5, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 26);
            this.label11.TabIndex = 8;
            this.label11.Text = "File (*.mdw)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnPathAccessWork
            // 
            this.btnPathAccessWork.Location = new System.Drawing.Point(210, 37);
            this.btnPathAccessWork.Name = "btnPathAccessWork";
            this.btnPathAccessWork.Size = new System.Drawing.Size(32, 23);
            this.btnPathAccessWork.TabIndex = 68;
            this.btnPathAccessWork.Text = "...";
            this.btnPathAccessWork.UseVisualStyleBackColor = true;
            // 
            // txtAccessWorkPassword
            // 
            this.txtAccessWorkPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccessWorkPassword.Location = new System.Drawing.Point(52, 13);
            this.txtAccessWorkPassword.Name = "txtAccessWorkPassword";
            this.txtAccessWorkPassword.Size = new System.Drawing.Size(152, 20);
            this.txtAccessWorkPassword.TabIndex = 66;
            this.txtAccessWorkPassword.UseSystemPasswordChar = true;
            // 
            // txtAccessPassword
            // 
            this.txtAccessPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccessPassword.Location = new System.Drawing.Point(123, 44);
            this.txtAccessPassword.Name = "txtAccessPassword";
            this.txtAccessPassword.Size = new System.Drawing.Size(131, 20);
            this.txtAccessPassword.TabIndex = 5;
            this.txtAccessPassword.UseSystemPasswordChar = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(34, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Password :";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAccessUserName
            // 
            this.txtAccessUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccessUserName.Location = new System.Drawing.Point(123, 18);
            this.txtAccessUserName.Name = "txtAccessUserName";
            this.txtAccessUserName.Size = new System.Drawing.Size(131, 20);
            this.txtAccessUserName.TabIndex = 4;
            this.txtAccessUserName.Text = "Admin";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "User name :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnBrowseFileAccess
            // 
            this.btnBrowseFileAccess.Location = new System.Drawing.Point(247, 7);
            this.btnBrowseFileAccess.Name = "btnBrowseFileAccess";
            this.btnBrowseFileAccess.Size = new System.Drawing.Size(32, 23);
            this.btnBrowseFileAccess.TabIndex = 2;
            this.btnBrowseFileAccess.Text = "...";
            this.btnBrowseFileAccess.UseVisualStyleBackColor = true;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(7, 6);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(52, 46);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "File (*.mdb)";
            // 
            // txtAccessFile
            // 
            this.txtAccessFile.BackColor = System.Drawing.Color.White;
            this.txtAccessFile.Location = new System.Drawing.Point(65, 8);
            this.txtAccessFile.Name = "txtAccessFile";
            this.txtAccessFile.ReadOnly = true;
            this.txtAccessFile.Size = new System.Drawing.Size(169, 20);
            this.txtAccessFile.TabIndex = 0;
            // 
            // cbDatabaseSources
            // 
            this.cbDatabaseSources.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDatabaseSources.FormattingEnabled = true;
            this.cbDatabaseSources.Location = new System.Drawing.Point(126, 25);
            this.cbDatabaseSources.Name = "cbDatabaseSources";
            this.cbDatabaseSources.Size = new System.Drawing.Size(177, 21);
            this.cbDatabaseSources.TabIndex = 91;
            this.cbDatabaseSources.SelectedIndexChanged += new System.EventHandler(this.cbDatabaseSources_SelectedIndexChanged);
            // 
            // btnTestConn
            // 
            this.btnTestConn.Location = new System.Drawing.Point(13, 272);
            this.btnTestConn.Name = "btnTestConn";
            this.btnTestConn.Size = new System.Drawing.Size(100, 23);
            this.btnTestConn.TabIndex = 95;
            this.btnTestConn.Text = "Test connection";
            this.btnTestConn.UseVisualStyleBackColor = true;
            this.btnTestConn.Click += new System.EventHandler(this.btnTestConn_Click);
            // 
            // pnlSqlExpress
            // 
            this.pnlSqlExpress.BackColor = System.Drawing.Color.Transparent;
            this.pnlSqlExpress.Controls.Add(this.cbSqlExpressName);
            this.pnlSqlExpress.Controls.Add(this.Label7);
            this.pnlSqlExpress.Controls.Add(this.btnBrowseFile);
            this.pnlSqlExpress.Controls.Add(this.grbconnexion);
            this.pnlSqlExpress.Controls.Add(this.Label2);
            this.pnlSqlExpress.Controls.Add(this.TxtSqlExpressFile);
            this.pnlSqlExpress.Location = new System.Drawing.Point(17, 337);
            this.pnlSqlExpress.Name = "pnlSqlExpress";
            this.pnlSqlExpress.Size = new System.Drawing.Size(290, 205);
            this.pnlSqlExpress.TabIndex = 96;
            // 
            // cbSqlExpressName
            // 
            this.cbSqlExpressName.FormattingEnabled = true;
            this.cbSqlExpressName.Location = new System.Drawing.Point(65, 6);
            this.cbSqlExpressName.Name = "cbSqlExpressName";
            this.cbSqlExpressName.Size = new System.Drawing.Size(179, 21);
            this.cbSqlExpressName.TabIndex = 82;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(7, 8);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(54, 20);
            this.Label7.TabIndex = 81;
            this.Label7.Text = "Server";
            // 
            // btnBrowseFile
            // 
            this.btnBrowseFile.Location = new System.Drawing.Point(250, 33);
            this.btnBrowseFile.Name = "btnBrowseFile";
            this.btnBrowseFile.Size = new System.Drawing.Size(33, 23);
            this.btnBrowseFile.TabIndex = 2;
            this.btnBrowseFile.Text = "...";
            this.btnBrowseFile.UseVisualStyleBackColor = true;
            this.btnBrowseFile.Click += new System.EventHandler(this.btnBrowseFile_Click);
            // 
            // grbconnexion
            // 
            this.grbconnexion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbconnexion.Controls.Add(this.chkSqlExpressSavePass);
            this.grbconnexion.Controls.Add(this.txtSqlExpressPassword);
            this.grbconnexion.Controls.Add(this.lblPassword);
            this.grbconnexion.Controls.Add(this.txtSqlExpressUserName);
            this.grbconnexion.Controls.Add(this.lblUsername);
            this.grbconnexion.Controls.Add(this.rbAuthSQLExpress);
            this.grbconnexion.Controls.Add(this.rbAuthWinSQLExpress);
            this.grbconnexion.Location = new System.Drawing.Point(9, 61);
            this.grbconnexion.Name = "grbconnexion";
            this.grbconnexion.Size = new System.Drawing.Size(270, 141);
            this.grbconnexion.TabIndex = 79;
            this.grbconnexion.TabStop = false;
            this.grbconnexion.Text = "Connect to Server";
            // 
            // chkSqlExpressSavePass
            // 
            this.chkSqlExpressSavePass.AutoSize = true;
            this.chkSqlExpressSavePass.Enabled = false;
            this.chkSqlExpressSavePass.Location = new System.Drawing.Point(20, 116);
            this.chkSqlExpressSavePass.Name = "chkSqlExpressSavePass";
            this.chkSqlExpressSavePass.Size = new System.Drawing.Size(126, 17);
            this.chkSqlExpressSavePass.TabIndex = 101;
            this.chkSqlExpressSavePass.Text = "Remember Password";
            this.chkSqlExpressSavePass.UseVisualStyleBackColor = true;
            // 
            // txtSqlExpressPassword
            // 
            this.txtSqlExpressPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlExpressPassword.Enabled = false;
            this.txtSqlExpressPassword.Location = new System.Drawing.Point(124, 90);
            this.txtSqlExpressPassword.Name = "txtSqlExpressPassword";
            this.txtSqlExpressPassword.Size = new System.Drawing.Size(131, 20);
            this.txtSqlExpressPassword.TabIndex = 5;
            this.txtSqlExpressPassword.UseSystemPasswordChar = true;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(32, 94);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(59, 13);
            this.lblPassword.TabIndex = 4;
            this.lblPassword.Text = "Password :";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSqlExpressUserName
            // 
            this.txtSqlExpressUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlExpressUserName.Enabled = false;
            this.txtSqlExpressUserName.Location = new System.Drawing.Point(124, 64);
            this.txtSqlExpressUserName.Name = "txtSqlExpressUserName";
            this.txtSqlExpressUserName.Size = new System.Drawing.Size(131, 20);
            this.txtSqlExpressUserName.TabIndex = 4;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(28, 68);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(66, 13);
            this.lblUsername.TabIndex = 2;
            this.lblUsername.Text = "User Name :";
            this.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbAuthSQLExpress
            // 
            this.rbAuthSQLExpress.AutoSize = true;
            this.rbAuthSQLExpress.Location = new System.Drawing.Point(16, 42);
            this.rbAuthSQLExpress.Name = "rbAuthSQLExpress";
            this.rbAuthSQLExpress.Size = new System.Drawing.Size(177, 17);
            this.rbAuthSQLExpress.TabIndex = 3;
            this.rbAuthSQLExpress.Text = "Use SQL Server authentification";
            this.rbAuthSQLExpress.CheckedChanged += new System.EventHandler(this.rbAuthSQLExpress_CheckedChanged);
            // 
            // rbAuthWinSQLExpress
            // 
            this.rbAuthWinSQLExpress.AutoSize = true;
            this.rbAuthWinSQLExpress.Checked = true;
            this.rbAuthWinSQLExpress.Location = new System.Drawing.Point(16, 19);
            this.rbAuthWinSQLExpress.Name = "rbAuthWinSQLExpress";
            this.rbAuthWinSQLExpress.Size = new System.Drawing.Size(166, 17);
            this.rbAuthWinSQLExpress.TabIndex = 2;
            this.rbAuthWinSQLExpress.TabStop = true;
            this.rbAuthWinSQLExpress.Text = "Use Windows authentification";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(10, 31);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(51, 27);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "File (*.mdf)";
            // 
            // TxtSqlExpressFile
            // 
            this.TxtSqlExpressFile.BackColor = System.Drawing.Color.White;
            this.TxtSqlExpressFile.Location = new System.Drawing.Point(65, 34);
            this.TxtSqlExpressFile.Name = "TxtSqlExpressFile";
            this.TxtSqlExpressFile.ReadOnly = true;
            this.TxtSqlExpressFile.Size = new System.Drawing.Size(179, 20);
            this.TxtSqlExpressFile.TabIndex = 0;
            // 
            // pnlSqlServer
            // 
            this.pnlSqlServer.BackColor = System.Drawing.Color.Transparent;
            this.pnlSqlServer.Controls.Add(this.cbSqlServerName);
            this.pnlSqlServer.Controls.Add(this.cbDatabases);
            this.pnlSqlServer.Controls.Add(this.Label6);
            this.pnlSqlServer.Controls.Add(this.GroupBox1);
            this.pnlSqlServer.Controls.Add(this.Label5);
            this.pnlSqlServer.Location = new System.Drawing.Point(14, 59);
            this.pnlSqlServer.Name = "pnlSqlServer";
            this.pnlSqlServer.Size = new System.Drawing.Size(290, 205);
            this.pnlSqlServer.TabIndex = 97;
            // 
            // cbSqlServerName
            // 
            this.cbSqlServerName.FormattingEnabled = true;
            this.cbSqlServerName.Location = new System.Drawing.Point(65, 5);
            this.cbSqlServerName.Name = "cbSqlServerName";
            this.cbSqlServerName.Size = new System.Drawing.Size(215, 21);
            this.cbSqlServerName.TabIndex = 83;
            this.cbSqlServerName.SelectedIndexChanged += new System.EventHandler(this.cbSqlServerName_SelectedIndexChanged);
            // 
            // cbDatabases
            // 
            this.cbDatabases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDatabases.FormattingEnabled = true;
            this.cbDatabases.Location = new System.Drawing.Point(65, 31);
            this.cbDatabases.Name = "cbDatabases";
            this.cbDatabases.Size = new System.Drawing.Size(215, 21);
            this.cbDatabases.TabIndex = 82;
            this.cbDatabases.DropDown += new System.EventHandler(this.cbDatabases_DropDown);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(7, 34);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(66, 22);
            this.Label6.TabIndex = 81;
            this.Label6.Text = "DataBase";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBox1.Controls.Add(this.chkSqlServerSavePass);
            this.GroupBox1.Controls.Add(this.txtSqlServerPassword);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.txtSqlServerUserName);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.rbAuthSQLServer);
            this.GroupBox1.Controls.Add(this.rbAuthWinSQLServer);
            this.GroupBox1.Location = new System.Drawing.Point(10, 57);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(270, 145);
            this.GroupBox1.TabIndex = 79;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Connect to Server";
            // 
            // chkSqlServerSavePass
            // 
            this.chkSqlServerSavePass.AutoSize = true;
            this.chkSqlServerSavePass.Enabled = false;
            this.chkSqlServerSavePass.Location = new System.Drawing.Point(19, 120);
            this.chkSqlServerSavePass.Name = "chkSqlServerSavePass";
            this.chkSqlServerSavePass.Size = new System.Drawing.Size(126, 17);
            this.chkSqlServerSavePass.TabIndex = 100;
            this.chkSqlServerSavePass.Text = "Remember Password";
            this.chkSqlServerSavePass.UseVisualStyleBackColor = true;
            // 
            // txtSqlServerPassword
            // 
            this.txtSqlServerPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlServerPassword.Enabled = false;
            this.txtSqlServerPassword.Location = new System.Drawing.Point(123, 92);
            this.txtSqlServerPassword.Name = "txtSqlServerPassword";
            this.txtSqlServerPassword.Size = new System.Drawing.Size(132, 20);
            this.txtSqlServerPassword.TabIndex = 5;
            this.txtSqlServerPassword.UseSystemPasswordChar = true;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(32, 96);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(59, 13);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "Password :";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSqlServerUserName
            // 
            this.txtSqlServerUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlServerUserName.Enabled = false;
            this.txtSqlServerUserName.Location = new System.Drawing.Point(123, 65);
            this.txtSqlServerUserName.Name = "txtSqlServerUserName";
            this.txtSqlServerUserName.Size = new System.Drawing.Size(132, 20);
            this.txtSqlServerUserName.TabIndex = 4;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(29, 69);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(64, 13);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "User name :";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // rbAuthSQLServer
            // 
            this.rbAuthSQLServer.AutoSize = true;
            this.rbAuthSQLServer.Location = new System.Drawing.Point(16, 42);
            this.rbAuthSQLServer.Name = "rbAuthSQLServer";
            this.rbAuthSQLServer.Size = new System.Drawing.Size(177, 17);
            this.rbAuthSQLServer.TabIndex = 3;
            this.rbAuthSQLServer.Text = "Use SQL Server authentification";
            this.rbAuthSQLServer.CheckedChanged += new System.EventHandler(this.rbAuthSQLServer_CheckedChanged);
            // 
            // rbAuthWinSQLServer
            // 
            this.rbAuthWinSQLServer.AutoSize = true;
            this.rbAuthWinSQLServer.Checked = true;
            this.rbAuthWinSQLServer.Location = new System.Drawing.Point(16, 19);
            this.rbAuthWinSQLServer.Name = "rbAuthWinSQLServer";
            this.rbAuthWinSQLServer.Size = new System.Drawing.Size(166, 17);
            this.rbAuthWinSQLServer.TabIndex = 2;
            this.rbAuthWinSQLServer.TabStop = true;
            this.rbAuthWinSQLServer.Text = "Use Windows authentification";
            this.rbAuthWinSQLServer.CheckedChanged += new System.EventHandler(this.rbAuthWinSQLServer_CheckedChanged);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(7, 8);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(54, 16);
            this.Label5.TabIndex = 1;
            this.Label5.Text = "Server";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.btnOptions);
            this.groupBox3.Controls.Add(this.pnlSqlServer);
            this.groupBox3.Controls.Add(this.cbDatabaseSources);
            this.groupBox3.Controls.Add(this.btnTestConn);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(318, 307);
            this.groupBox3.TabIndex = 98;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Target Database";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(15, 28);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 16);
            this.label8.TabIndex = 99;
            this.label8.Text = "Database Provider:";
            // 
            // btnOptions
            // 
            this.btnOptions.Location = new System.Drawing.Point(237, 272);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(68, 23);
            this.btnOptions.TabIndex = 98;
            this.btnOptions.Text = "Options";
            this.btnOptions.UseVisualStyleBackColor = true;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            // 
            // pnlSQLCE
            // 
            this.pnlSQLCE.BackColor = System.Drawing.Color.Transparent;
            this.pnlSQLCE.Controls.Add(this.cbSqlCeFile);
            this.pnlSQLCE.Controls.Add(this.groupBox4);
            this.pnlSQLCE.Controls.Add(this.button2);
            this.pnlSQLCE.Controls.Add(this.label15);
            this.pnlSQLCE.Location = new System.Drawing.Point(350, 224);
            this.pnlSQLCE.Name = "pnlSQLCE";
            this.pnlSQLCE.Size = new System.Drawing.Size(290, 205);
            this.pnlSQLCE.TabIndex = 99;
            // 
            // cbSqlCeFile
            // 
            this.cbSqlCeFile.FormattingEnabled = true;
            this.cbSqlCeFile.Location = new System.Drawing.Point(10, 25);
            this.cbSqlCeFile.Name = "cbSqlCeFile";
            this.cbSqlCeFile.Size = new System.Drawing.Size(233, 21);
            this.cbSqlCeFile.TabIndex = 61;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.chkSqlCESavePass);
            this.groupBox4.Controls.Add(this.txtSqlCePassword);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.txtSqlCeUsername);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Location = new System.Drawing.Point(10, 51);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(270, 151);
            this.groupBox4.TabIndex = 60;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Connect to Server";
            // 
            // chkSqlCESavePass
            // 
            this.chkSqlCESavePass.AutoSize = true;
            this.chkSqlCESavePass.Location = new System.Drawing.Point(26, 99);
            this.chkSqlCESavePass.Name = "chkSqlCESavePass";
            this.chkSqlCESavePass.Size = new System.Drawing.Size(126, 17);
            this.chkSqlCESavePass.TabIndex = 102;
            this.chkSqlCESavePass.Text = "Remember Password";
            this.chkSqlCESavePass.UseVisualStyleBackColor = true;
            // 
            // txtSqlCePassword
            // 
            this.txtSqlCePassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlCePassword.Location = new System.Drawing.Point(123, 54);
            this.txtSqlCePassword.Name = "txtSqlCePassword";
            this.txtSqlCePassword.Size = new System.Drawing.Size(131, 20);
            this.txtSqlCePassword.TabIndex = 5;
            this.txtSqlCePassword.UseSystemPasswordChar = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(34, 58);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Password :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSqlCeUsername
            // 
            this.txtSqlCeUsername.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSqlCeUsername.Enabled = false;
            this.txtSqlCeUsername.Location = new System.Drawing.Point(123, 28);
            this.txtSqlCeUsername.Name = "txtSqlCeUsername";
            this.txtSqlCeUsername.Size = new System.Drawing.Size(131, 20);
            this.txtSqlCeUsername.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Enabled = false;
            this.label14.Location = new System.Drawing.Point(29, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "User name :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(247, 24);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(32, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(7, 6);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(236, 16);
            this.label15.TabIndex = 1;
            this.label15.Text = "Database File";
            // 
            // pnlOptions
            // 
            this.pnlOptions.BackColor = System.Drawing.Color.Transparent;
            this.pnlOptions.Controls.Add(this.grbAdvancedOptions);
            this.pnlOptions.Controls.Add(this.grbNetworkProperties);
            this.pnlOptions.Controls.Add(this.groupBox5);
            this.pnlOptions.Location = new System.Drawing.Point(350, 448);
            this.pnlOptions.Name = "pnlOptions";
            this.pnlOptions.Size = new System.Drawing.Size(290, 205);
            this.pnlOptions.TabIndex = 100;
            // 
            // grbAdvancedOptions
            // 
            this.grbAdvancedOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbAdvancedOptions.Controls.Add(this.label25);
            this.grbAdvancedOptions.Controls.Add(this.nudMaxDbSize);
            this.grbAdvancedOptions.Controls.Add(this.label22);
            this.grbAdvancedOptions.Controls.Add(this.nudLockEscalation);
            this.grbAdvancedOptions.Controls.Add(this.label23);
            this.grbAdvancedOptions.Controls.Add(this.label24);
            this.grbAdvancedOptions.Location = new System.Drawing.Point(305, 11);
            this.grbAdvancedOptions.Name = "grbAdvancedOptions";
            this.grbAdvancedOptions.Size = new System.Drawing.Size(269, 94);
            this.grbAdvancedOptions.TabIndex = 62;
            this.grbAdvancedOptions.TabStop = false;
            this.grbAdvancedOptions.Text = "Advanced Options";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(226, 21);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(23, 13);
            this.label25.TabIndex = 86;
            this.label25.Text = "MB";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nudMaxDbSize
            // 
            this.nudMaxDbSize.Location = new System.Drawing.Point(158, 19);
            this.nudMaxDbSize.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudMaxDbSize.Name = "nudMaxDbSize";
            this.nudMaxDbSize.Size = new System.Drawing.Size(62, 20);
            this.nudMaxDbSize.TabIndex = 85;
            this.nudMaxDbSize.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(225, 49);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 13);
            this.label22.TabIndex = 84;
            this.label22.Text = "locks";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nudLockEscalation
            // 
            this.nudLockEscalation.Location = new System.Drawing.Point(158, 45);
            this.nudLockEscalation.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudLockEscalation.Name = "nudLockEscalation";
            this.nudLockEscalation.Size = new System.Drawing.Size(62, 20);
            this.nudLockEscalation.TabIndex = 6;
            this.nudLockEscalation.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(20, 49);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(123, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Default Lock Escalation:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(20, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(123, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "Maximux Database Size:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grbNetworkProperties
            // 
            this.grbNetworkProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbNetworkProperties.Controls.Add(this.label21);
            this.grbNetworkProperties.Controls.Add(this.cmbProtocol);
            this.grbNetworkProperties.Controls.Add(this.nudPacketSize);
            this.grbNetworkProperties.Controls.Add(this.label17);
            this.grbNetworkProperties.Controls.Add(this.label18);
            this.grbNetworkProperties.Location = new System.Drawing.Point(10, 8);
            this.grbNetworkProperties.Name = "grbNetworkProperties";
            this.grbNetworkProperties.Size = new System.Drawing.Size(269, 77);
            this.grbNetworkProperties.TabIndex = 61;
            this.grbNetworkProperties.TabStop = false;
            this.grbNetworkProperties.Text = "Network Properties";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(225, 49);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 13);
            this.label21.TabIndex = 84;
            this.label21.Text = "bytes";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbProtocol
            // 
            this.cmbProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProtocol.FormattingEnabled = true;
            this.cmbProtocol.Items.AddRange(new object[] {
            "<Default>",
            "Shared Memory",
            "TCP/IP",
            "IPX/SPX",
            "Named Pipes",
            "Multiprotocol"});
            this.cmbProtocol.Location = new System.Drawing.Point(158, 18);
            this.cmbProtocol.Name = "cmbProtocol";
            this.cmbProtocol.Size = new System.Drawing.Size(100, 21);
            this.cmbProtocol.TabIndex = 83;
            // 
            // nudPacketSize
            // 
            this.nudPacketSize.Location = new System.Drawing.Point(158, 45);
            this.nudPacketSize.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudPacketSize.Name = "nudPacketSize";
            this.nudPacketSize.Size = new System.Drawing.Size(62, 20);
            this.nudPacketSize.TabIndex = 6;
            this.nudPacketSize.Value = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(33, 49);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Network Packet Size:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(51, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 13);
            this.label18.TabIndex = 2;
            this.label18.Text = "Network Protocol:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.chbEncryptConn);
            this.groupBox5.Controls.Add(this.nudExecTimeout);
            this.groupBox5.Controls.Add(this.nudConnTimeout);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Location = new System.Drawing.Point(10, 89);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(269, 100);
            this.groupBox5.TabIndex = 60;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Connection Properties";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(225, 49);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(24, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "sec";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(225, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(24, 13);
            this.label19.TabIndex = 8;
            this.label19.Text = "sec";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chbEncryptConn
            // 
            this.chbEncryptConn.AutoSize = true;
            this.chbEncryptConn.Location = new System.Drawing.Point(21, 70);
            this.chbEncryptConn.Name = "chbEncryptConn";
            this.chbEncryptConn.Size = new System.Drawing.Size(119, 17);
            this.chbEncryptConn.TabIndex = 7;
            this.chbEncryptConn.Text = "Encrypt Connection";
            this.chbEncryptConn.UseVisualStyleBackColor = true;
            // 
            // nudExecTimeout
            // 
            this.nudExecTimeout.Location = new System.Drawing.Point(158, 45);
            this.nudExecTimeout.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudExecTimeout.Name = "nudExecTimeout";
            this.nudExecTimeout.Size = new System.Drawing.Size(62, 20);
            this.nudExecTimeout.TabIndex = 6;
            // 
            // nudConnTimeout
            // 
            this.nudConnTimeout.Location = new System.Drawing.Point(158, 19);
            this.nudConnTimeout.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudConnTimeout.Name = "nudConnTimeout";
            this.nudConnTimeout.Size = new System.Drawing.Size(62, 20);
            this.nudConnTimeout.TabIndex = 5;
            this.nudConnTimeout.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(42, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Execution Time-out:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(35, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(108, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Connection Time-out:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DBConnector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlOptions);
            this.Controls.Add(this.pnlSQLCE);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.pnlSqlExpress);
            this.Controls.Add(this.pnlAccess);
            this.Name = "DBConnector";
            this.Size = new System.Drawing.Size(318, 307);
            this.pnlAccess.ResumeLayout(false);
            this.pnlAccess.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grbAccessWorkGroup.ResumeLayout(false);
            this.grbAccessWorkGroup.PerformLayout();
            this.pnlSqlExpress.ResumeLayout(false);
            this.pnlSqlExpress.PerformLayout();
            this.grbconnexion.ResumeLayout(false);
            this.grbconnexion.PerformLayout();
            this.pnlSqlServer.ResumeLayout(false);
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.pnlSQLCE.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.pnlOptions.ResumeLayout(false);
            this.grbAdvancedOptions.ResumeLayout(false);
            this.grbAdvancedOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudMaxDbSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLockEscalation)).EndInit();
            this.grbNetworkProperties.ResumeLayout(false);
            this.grbNetworkProperties.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPacketSize)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExecTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudConnTimeout)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel pnlAccess;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkAccessWork;
        private System.Windows.Forms.GroupBox grbAccessWorkGroup;
        private System.Windows.Forms.TextBox txtAccessFileWork;
        private System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Button btnPathAccessWork;
        private System.Windows.Forms.TextBox txtAccessWorkPassword;
        private System.Windows.Forms.TextBox txtAccessPassword;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAccessUserName;
        private System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Button btnBrowseFileAccess;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TextBox txtAccessFile;
        internal System.Windows.Forms.ComboBox cbDatabaseSources;
        internal System.Windows.Forms.Button btnTestConn;
        internal System.Windows.Forms.Panel pnlSqlExpress;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button btnBrowseFile;
        private System.Windows.Forms.GroupBox grbconnexion;
        private System.Windows.Forms.TextBox txtSqlExpressPassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtSqlExpressUserName;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.RadioButton rbAuthSQLExpress;
        private System.Windows.Forms.RadioButton rbAuthWinSQLExpress;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TxtSqlExpressFile;
        internal System.Windows.Forms.Panel pnlSqlServer;
        private System.Windows.Forms.ComboBox cbDatabases;
        internal System.Windows.Forms.Label Label6;
        private System.Windows.Forms.GroupBox GroupBox1;
        private System.Windows.Forms.TextBox txtSqlServerPassword;
        private System.Windows.Forms.Label Label3;
        private System.Windows.Forms.TextBox txtSqlServerUserName;
        private System.Windows.Forms.Label Label4;
        private System.Windows.Forms.RadioButton rbAuthSQLServer;
        private System.Windows.Forms.RadioButton rbAuthWinSQLServer;
        internal System.Windows.Forms.Label Label5;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Button btnOptions;
        internal System.Windows.Forms.Panel pnlSQLCE;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtSqlCePassword;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSqlCeUsername;
        private System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Button button2;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Panel pnlOptions;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown nudConnTimeout;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nudExecTimeout;
        private System.Windows.Forms.CheckBox chbEncryptConn;
        private System.Windows.Forms.GroupBox grbNetworkProperties;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cmbProtocol;
        private System.Windows.Forms.NumericUpDown nudPacketSize;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox grbAdvancedOptions;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown nudLockEscalation;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown nudMaxDbSize;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkSqlServerSavePass;
        private System.Windows.Forms.CheckBox chkSqlExpressSavePass;
        private System.Windows.Forms.CheckBox chkSqlCESavePass;
        private System.Windows.Forms.ComboBox cbSqlExpressName;
        private System.Windows.Forms.ComboBox cbSqlServerName;
        private System.Windows.Forms.ComboBox cbSqlCeFile;
    }
}
