﻿namespace XLETL.TaskManager.Controls
{
    partial class TransformationPluginsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TransformationPluginsDialog));
          this.pnlPluginViewer = new System.Windows.Forms.Panel();
          this.label1 = new System.Windows.Forms.Label();
          this.cbTransformationPluginList = new System.Windows.Forms.ComboBox();
          this.btnOk = new System.Windows.Forms.Button();
          this.btnCancel = new System.Windows.Forms.Button();
          this.pictureBox1 = new System.Windows.Forms.PictureBox();
          this.pnlPluginViewer.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
          this.SuspendLayout();
          // 
          // pnlPluginViewer
          // 
          this.pnlPluginViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.pnlPluginViewer.AutoScroll = true;
          this.pnlPluginViewer.BackColor = System.Drawing.SystemColors.Window;
          this.pnlPluginViewer.Controls.Add(this.label1);
          this.pnlPluginViewer.Location = new System.Drawing.Point(12, 69);
          this.pnlPluginViewer.Name = "pnlPluginViewer";
          this.pnlPluginViewer.Padding = new System.Windows.Forms.Padding(5, 3, 5, 5);
          this.pnlPluginViewer.Size = new System.Drawing.Size(260, 122);
          this.pnlPluginViewer.TabIndex = 18;
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(22, 53);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(215, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "Select a Transformation Control from the List";
          // 
          // cbTransformationPluginList
          // 
          this.cbTransformationPluginList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.cbTransformationPluginList.FormattingEnabled = true;
          this.cbTransformationPluginList.Location = new System.Drawing.Point(88, 26);
          this.cbTransformationPluginList.Name = "cbTransformationPluginList";
          this.cbTransformationPluginList.Size = new System.Drawing.Size(174, 21);
          this.cbTransformationPluginList.TabIndex = 17;
          this.cbTransformationPluginList.SelectedIndexChanged += new System.EventHandler(this.cbTransformationPluginList_SelectedIndexChanged);
          // 
          // btnOk
          // 
          this.btnOk.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
          this.btnOk.Location = new System.Drawing.Point(63, 197);
          this.btnOk.Name = "btnOk";
          this.btnOk.Size = new System.Drawing.Size(73, 22);
          this.btnOk.TabIndex = 19;
          this.btnOk.Text = "Ok";
          this.btnOk.UseVisualStyleBackColor = true;
          this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
          // 
          // btnCancel
          // 
          this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
          this.btnCancel.Location = new System.Drawing.Point(147, 197);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(73, 22);
          this.btnCancel.TabIndex = 20;
          this.btnCancel.Text = "Cancel";
          this.btnCancel.UseVisualStyleBackColor = true;
          this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
          // 
          // pictureBox1
          // 
          this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
          this.pictureBox1.Location = new System.Drawing.Point(12, 12);
          this.pictureBox1.Name = "pictureBox1";
          this.pictureBox1.Size = new System.Drawing.Size(52, 50);
          this.pictureBox1.TabIndex = 21;
          this.pictureBox1.TabStop = false;
          // 
          // TransformationPluginsDialog
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(282, 225);
          this.Controls.Add(this.pictureBox1);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnOk);
          this.Controls.Add(this.pnlPluginViewer);
          this.Controls.Add(this.cbTransformationPluginList);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
          this.Name = "TransformationPluginsDialog";
          this.Text = "Transformations";
          this.pnlPluginViewer.ResumeLayout(false);
          this.pnlPluginViewer.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPluginViewer;
        private System.Windows.Forms.ComboBox cbTransformationPluginList;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
    }
}