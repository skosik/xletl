﻿namespace XLETL.TaskManager.Controls
{
    partial class TransformationProperty
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.btnAddTransformation = new System.Windows.Forms.Button();
          this.btnEditTransformation = new System.Windows.Forms.Button();
          this.btnRemove = new System.Windows.Forms.Button();
          this.lbTransformations = new System.Windows.Forms.ListBox();
          this.btnUp = new System.Windows.Forms.Button();
          this.btnDown = new System.Windows.Forms.Button();
          this.lblTitle = new System.Windows.Forms.Label();
          this.SuspendLayout();
          // 
          // btnAddTransformation
          // 
          this.btnAddTransformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.btnAddTransformation.Location = new System.Drawing.Point(6, 83);
          this.btnAddTransformation.Name = "btnAddTransformation";
          this.btnAddTransformation.Size = new System.Drawing.Size(49, 20);
          this.btnAddTransformation.TabIndex = 4;
          this.btnAddTransformation.Text = "Add";
          this.btnAddTransformation.UseVisualStyleBackColor = true;
          this.btnAddTransformation.Click += new System.EventHandler(this.btnAddTransformation_Click);
          // 
          // btnEditTransformation
          // 
          this.btnEditTransformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
          this.btnEditTransformation.Location = new System.Drawing.Point(56, 83);
          this.btnEditTransformation.Name = "btnEditTransformation";
          this.btnEditTransformation.Size = new System.Drawing.Size(49, 20);
          this.btnEditTransformation.TabIndex = 7;
          this.btnEditTransformation.Text = "Edit";
          this.btnEditTransformation.UseVisualStyleBackColor = true;
          this.btnEditTransformation.Click += new System.EventHandler(this.btnEditTransformation_Click);
          // 
          // btnRemove
          // 
          this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
          this.btnRemove.Location = new System.Drawing.Point(133, 83);
          this.btnRemove.Name = "btnRemove";
          this.btnRemove.Size = new System.Drawing.Size(64, 20);
          this.btnRemove.TabIndex = 8;
          this.btnRemove.Text = "Remove";
          this.btnRemove.UseVisualStyleBackColor = true;
          this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
          // 
          // lbTransformations
          // 
          this.lbTransformations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.lbTransformations.FormattingEnabled = true;
          this.lbTransformations.Location = new System.Drawing.Point(5, 24);
          this.lbTransformations.Name = "lbTransformations";
          this.lbTransformations.Size = new System.Drawing.Size(192, 56);
          this.lbTransformations.TabIndex = 9;
          // 
          // btnUp
          // 
          this.btnUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.btnUp.Location = new System.Drawing.Point(201, 24);
          this.btnUp.Name = "btnUp";
          this.btnUp.Size = new System.Drawing.Size(25, 20);
          this.btnUp.TabIndex = 10;
          this.btnUp.Text = "/\\";
          this.btnUp.UseVisualStyleBackColor = true;
          this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
          // 
          // btnDown
          // 
          this.btnDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnDown.Location = new System.Drawing.Point(201, 47);
          this.btnDown.Name = "btnDown";
          this.btnDown.Size = new System.Drawing.Size(25, 20);
          this.btnDown.TabIndex = 11;
          this.btnDown.Text = "\\/";
          this.btnDown.UseVisualStyleBackColor = true;
          this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
          // 
          // lblTitle
          // 
          this.lblTitle.BackColor = System.Drawing.Color.Silver;
          this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
          this.lblTitle.Location = new System.Drawing.Point(0, 0);
          this.lblTitle.Name = "lblTitle";
          this.lblTitle.Size = new System.Drawing.Size(229, 18);
          this.lblTitle.TabIndex = 12;
          this.lblTitle.Text = "Template Column Transformation";
          this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // TransformationProperty
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.Controls.Add(this.lblTitle);
          this.Controls.Add(this.btnDown);
          this.Controls.Add(this.btnUp);
          this.Controls.Add(this.lbTransformations);
          this.Controls.Add(this.btnRemove);
          this.Controls.Add(this.btnEditTransformation);
          this.Controls.Add(this.btnAddTransformation);
          this.Name = "TransformationProperty";
          this.Size = new System.Drawing.Size(229, 107);
          this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddTransformation;
        private System.Windows.Forms.Button btnEditTransformation;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.ListBox lbTransformations;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Label lblTitle;
    }
}
