﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CodeProject.Dialog;
using XLETL.Model;

namespace XLETL.TaskManager.Controls
{
  [Serializable]
    public partial class TransformationProperty : UserControl, INotifyPropertyChanged
    {
        private List<PluginInfo> transList = null;
        public List<PluginInfo> TransList
        {
            get
            {
                if (transList == null)
                {
                    transList = new List<PluginInfo>();
                }
                return transList;
            }
            set
            {
                transList = value;
                if (transList == null || transList.Count == 0)
                {
                    lbTransformations.Items.Clear();
                }
                else
                {
                    PopulateListBox(-1);
                }

            }
        }

        [Browsable(true)]
        public override string Text
        {
            get { return this.lblTitle.Text; }
            set
            {
                this.lblTitle.Text = value;
            }
        }

        // this can be used by different plugins
        private string dbConnString;
        public string DatabaseConnString
        {
            get
            {                
                return dbConnString;
            }
            set
            {
                dbConnString = value;
            }
        }

        public string DbTableName { get; set; }

        private bool _allowShowDlg = false;
        public bool AllowShowDialog
        {
            get
            {
                return _allowShowDlg;
            }
            set
            {
                _allowShowDlg = value;
                //if (!_allowShowDlg)
                //{
                //    ClearTransList();
                //}
            }
        }

        public Globals.PluginTarget PgPluginTarget { get; set; }
        
        public TransformationProperty()
        {
            InitializeComponent();            
        }

        private void btnAddTransformation_Click(object sender, EventArgs e)
        {
            if (!AllowShowDialog) return; // return from method if it is not allowed to display dialog;

            if (string.IsNullOrEmpty(dbConnString))
            {
                throw new ArgumentNullException("dbConnString");
            }

            //if (PgPluginTarget == null)
            //{
            //  throw new ArgumentException("PluginTarget argument is not set");
            //}

            TransformationPluginsDialog selectTransPlugin = new TransformationPluginsDialog(DatabaseConnString, DbTableName, null, PgPluginTarget);

            if (DlgBox.ShowDialog(selectTransPlugin) != DialogResult.Cancel)
            {
                // do we have the transformation already in the list?
                PluginInfo pluginInfo = transList.Find(delegate(PluginInfo search)
                        {
                            if (search.Name.Equals(selectTransPlugin.SelectedTransPlugin.Name))
                                return true;
                            else return false;                            
                        });

                if (pluginInfo == null)
                {
                    transList.Add(selectTransPlugin.SelectedTransPlugin);
                    PopulateListBox(-1);
                    this.OnPropertyChanged("TransList");
                }
                
            }

            selectTransPlugin.Dispose();
            selectTransPlugin = null;

        }

        private void btnEditTransformation_Click(object sender, EventArgs e)
        {
            int index = lbTransformations.SelectedIndex;
            if (index > -1)
            {
                if (string.IsNullOrEmpty(dbConnString))
                {
                    throw new ArgumentNullException("dbConnString");
                }

                //if (PgPluginTarget == null)
                //{
                //  throw new ArgumentException("PluginTarget argument is not set");
                //}

                TransformationPluginsDialog selectTransPlugin = new TransformationPluginsDialog(DatabaseConnString, DbTableName, (PluginInfo)lbTransformations.Items[index], PgPluginTarget);

                if (DlgBox.ShowDialog(selectTransPlugin) != DialogResult.Cancel)
                {
                    this.OnPropertyChanged("TransList");
                }

                selectTransPlugin.Dispose();
                selectTransPlugin = null;
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int index = lbTransformations.SelectedIndex;
            if (index > -1)
            {
                transList.Remove((PluginInfo)lbTransformations.Items[index]);
                PopulateListBox(-1);
                this.OnPropertyChanged("TransList");
            }
        }

        private void btnUp_Click(object sender, EventArgs e)
        {            
            int selectedIndex = lbTransformations.SelectedIndex;
            if (selectedIndex > 0)
            {
                for (int i = 0; i < transList.Count; i++)
                {
                    if (selectedIndex == i)//identify the selected item
                    {
                        //swap with the top item(move up)                        
                        PluginInfo bottomItem = transList[i];
                        transList.Remove(bottomItem);
                        transList.Insert(i - 1, bottomItem);
                        PopulateListBox(i - 1);
                    }                    
                }                
            }            
        }

        private void btnDown_Click(object sender, EventArgs e)
        {
            int selectedIndex = lbTransformations.SelectedIndex;
            if (selectedIndex > -1 && selectedIndex < transList.Count - 1)
            {
                for (int i = transList.Count - 1; i > -1; i--)
                {
                    if (selectedIndex == i)//identify the selected item
                    {
                        //swap with the lower item(move down)
                        PluginInfo bottomItem = transList[i];
                        transList.Remove(bottomItem);
                        transList.Insert(i + 1, bottomItem);
                        PopulateListBox(i + 1);
                    }
                }
            }            
        }

        private void PopulateListBox(int itemIndexToSelect)
        {
                lbTransformations.BeginUpdate();
                lbTransformations.Items.Clear();

                // Add transformation to the ListBox
                foreach (PluginInfo plugin in transList)
                {
                    lbTransformations.Items.Add(plugin);
                }

                if (itemIndexToSelect > -1)
                {
                    lbTransformations.SelectedItem = lbTransformations.Items[itemIndexToSelect];
                }

                lbTransformations.EndUpdate();            
        }

        private void ClearTransList()
        {
            lbTransformations.Items.Clear();
            transList.Clear();
        }


        #region INotifyPropertyChanged Members
        
        public event PropertyChangedEventHandler PropertyChanged;

        // Helper
        void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(
                  this,
                  new PropertyChangedEventArgs(propertyName));
            }
        }


        #endregion
    }
}
