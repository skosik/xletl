﻿namespace XLETL.TaskManager.Forms
{
    partial class BuildTaskDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuildTaskDialog));
          this.pnlTargetColumns = new System.Windows.Forms.Panel();
          this.dgvTargetColumnList = new System.Windows.Forms.DataGridView();
          this.pnlTargetColumnProperties = new System.Windows.Forms.Panel();
          this.label2 = new System.Windows.Forms.Label();
          this.pgTargetColumns = new System.Windows.Forms.PropertyGrid();
          this.tpTarget = new XLETL.TaskManager.Controls.TransformationProperty();
          this.label1 = new System.Windows.Forms.Label();
          this.pnlTemplateColumns = new System.Windows.Forms.Panel();
          this.pgTemplateColumns = new System.Windows.Forms.PropertyGrid();
          this.label6 = new System.Windows.Forms.Label();
          this.transPropTemplate = new XLETL.TaskManager.Controls.TransformationProperty();
          this.pnlTaskDetails = new System.Windows.Forms.Panel();
          this.pgTaskDetails = new System.Windows.Forms.PropertyGrid();
          this.label5 = new System.Windows.Forms.Label();
          this.dgvTemplateDesign = new System.Windows.Forms.DataGridView();
          this.pnlSqlScript = new System.Windows.Forms.Panel();
          this.btnScriptLoad = new System.Windows.Forms.Button();
          this.cbSqlScriptType = new System.Windows.Forms.ComboBox();
          this.btnScriptTestRun = new System.Windows.Forms.Button();
          this.btnScriptClear = new System.Windows.Forms.Button();
          this.rtbPreLoadScript = new System.Windows.Forms.RichTextBox();
          this.label7 = new System.Windows.Forms.Label();
          this.pnlErrorHelp = new System.Windows.Forms.Panel();
          this.splitContainer2 = new System.Windows.Forms.SplitContainer();
          this.rtbErrors = new System.Windows.Forms.RichTextBox();
          this.label3 = new System.Windows.Forms.Label();
          this.rtbHelp = new System.Windows.Forms.RichTextBox();
          this.label4 = new System.Windows.Forms.Label();
          this.statusStrip1 = new System.Windows.Forms.StatusStrip();
          this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
          this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
          this.toolStrip1 = new System.Windows.Forms.ToolStrip();
          this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
          this.tsDatabaseName = new System.Windows.Forms.ToolStripLabel();
          this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
          this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
          this.tsCbTableList = new System.Windows.Forms.ToolStripComboBox();
          this.tsBtnSave = new System.Windows.Forms.ToolStripButton();
          this.tsValidate = new System.Windows.Forms.ToolStripButton();
          this.collapsibleSplitter1 = new NJFLib.Controls.CollapsibleSplitter();
          this.collapsibleSplitter2 = new NJFLib.Controls.CollapsibleSplitter();
          this.collapsibleSplitter3 = new NJFLib.Controls.CollapsibleSplitter();
          this.tabControl1 = new System.Windows.Forms.TabControl();
          this.tabPage1 = new System.Windows.Forms.TabPage();
          this.tabPage2 = new System.Windows.Forms.TabPage();
          this.label8 = new System.Windows.Forms.Label();
          this.cmbDbTableList = new System.Windows.Forms.ComboBox();
          this.dgvTargetData = new System.Windows.Forms.DataGridView();
          this.collapsibleSplitter4 = new NJFLib.Controls.CollapsibleSplitter();
          this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
          this.pnlTargetColumns.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dgvTargetColumnList)).BeginInit();
          this.pnlTargetColumnProperties.SuspendLayout();
          this.pnlTemplateColumns.SuspendLayout();
          this.pnlTaskDetails.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dgvTemplateDesign)).BeginInit();
          this.pnlSqlScript.SuspendLayout();
          this.pnlErrorHelp.SuspendLayout();
          this.splitContainer2.Panel1.SuspendLayout();
          this.splitContainer2.Panel2.SuspendLayout();
          this.splitContainer2.SuspendLayout();
          this.statusStrip1.SuspendLayout();
          this.toolStrip1.SuspendLayout();
          this.tabControl1.SuspendLayout();
          this.tabPage1.SuspendLayout();
          this.tabPage2.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dgvTargetData)).BeginInit();
          this.SuspendLayout();
          // 
          // pnlTargetColumns
          // 
          this.pnlTargetColumns.BackColor = System.Drawing.SystemColors.Control;
          this.pnlTargetColumns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pnlTargetColumns.Controls.Add(this.dgvTargetColumnList);
          this.pnlTargetColumns.Controls.Add(this.pnlTargetColumnProperties);
          this.pnlTargetColumns.Controls.Add(this.tpTarget);
          this.pnlTargetColumns.Controls.Add(this.label1);
          this.pnlTargetColumns.Dock = System.Windows.Forms.DockStyle.Left;
          this.pnlTargetColumns.Location = new System.Drawing.Point(0, 25);
          this.pnlTargetColumns.Name = "pnlTargetColumns";
          this.pnlTargetColumns.Size = new System.Drawing.Size(231, 493);
          this.pnlTargetColumns.TabIndex = 5;
          // 
          // dgvTargetColumnList
          // 
          this.dgvTargetColumnList.AllowDrop = true;
          this.dgvTargetColumnList.AllowUserToAddRows = false;
          this.dgvTargetColumnList.AllowUserToDeleteRows = false;
          this.dgvTargetColumnList.BackgroundColor = System.Drawing.SystemColors.Window;
          this.dgvTargetColumnList.BorderStyle = System.Windows.Forms.BorderStyle.None;
          this.dgvTargetColumnList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dgvTargetColumnList.Dock = System.Windows.Forms.DockStyle.Fill;
          this.dgvTargetColumnList.Location = new System.Drawing.Point(0, 18);
          this.dgvTargetColumnList.MultiSelect = false;
          this.dgvTargetColumnList.Name = "dgvTargetColumnList";
          this.dgvTargetColumnList.ReadOnly = true;
          this.dgvTargetColumnList.RowHeadersVisible = false;
          this.dgvTargetColumnList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dgvTargetColumnList.Size = new System.Drawing.Size(229, 149);
          this.dgvTargetColumnList.TabIndex = 5;
          this.toolTip1.SetToolTip(this.dgvTargetColumnList, "Please Drag & Drop required column to the Template Designer");
          this.dgvTargetColumnList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvTargetColumnList_MouseDown);
          this.dgvTargetColumnList.Enter += new System.EventHandler(this.dgvTargetColumnList_Enter);
          this.dgvTargetColumnList.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvTargetColumnList_DragEnter);
          this.dgvTargetColumnList.SelectionChanged += new System.EventHandler(this.dgvTargetColumnList_SelectionChanged);
          this.dgvTargetColumnList.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvTargetColumnList_DragDrop);
          // 
          // pnlTargetColumnProperties
          // 
          this.pnlTargetColumnProperties.Controls.Add(this.label2);
          this.pnlTargetColumnProperties.Controls.Add(this.pgTargetColumns);
          this.pnlTargetColumnProperties.Dock = System.Windows.Forms.DockStyle.Bottom;
          this.pnlTargetColumnProperties.Location = new System.Drawing.Point(0, 167);
          this.pnlTargetColumnProperties.Name = "pnlTargetColumnProperties";
          this.pnlTargetColumnProperties.Size = new System.Drawing.Size(229, 231);
          this.pnlTargetColumnProperties.TabIndex = 11;
          // 
          // label2
          // 
          this.label2.BackColor = System.Drawing.Color.Silver;
          this.label2.Dock = System.Windows.Forms.DockStyle.Top;
          this.label2.Location = new System.Drawing.Point(0, 0);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(229, 18);
          this.label2.TabIndex = 2;
          this.label2.Text = "Target Column Properties";
          this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // pgTargetColumns
          // 
          this.pgTargetColumns.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pgTargetColumns.Location = new System.Drawing.Point(0, 0);
          this.pgTargetColumns.Name = "pgTargetColumns";
          this.pgTargetColumns.PropertySort = System.Windows.Forms.PropertySort.NoSort;
          this.pgTargetColumns.Size = new System.Drawing.Size(229, 231);
          this.pgTargetColumns.TabIndex = 1;
          this.pgTargetColumns.ToolbarVisible = false;
          // 
          // tpTarget
          // 
          this.tpTarget.AllowShowDialog = false;
          this.tpTarget.DatabaseConnString = null;
          this.tpTarget.DbTableName = null;
          this.tpTarget.Dock = System.Windows.Forms.DockStyle.Bottom;
          this.tpTarget.Location = new System.Drawing.Point(0, 398);
          this.tpTarget.Name = "tpTarget";
          this.tpTarget.PgPluginTarget = XLETL.Model.Globals.PluginTarget.Target;
          this.tpTarget.Size = new System.Drawing.Size(229, 93);
          this.tpTarget.TabIndex = 10;
          this.tpTarget.TransList = ((System.Collections.Generic.List<XLETL.Model.PluginInfo>)(resources.GetObject("tpTarget.TransList")));
          this.tpTarget.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(this.tpTarget_PropertyChanged);
          // 
          // label1
          // 
          this.label1.BackColor = System.Drawing.Color.Silver;
          this.label1.Dock = System.Windows.Forms.DockStyle.Top;
          this.label1.Location = new System.Drawing.Point(0, 0);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(229, 18);
          this.label1.TabIndex = 0;
          this.label1.Text = "Target Columns";
          this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // pnlTemplateColumns
          // 
          this.pnlTemplateColumns.BackColor = System.Drawing.SystemColors.Control;
          this.pnlTemplateColumns.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pnlTemplateColumns.Controls.Add(this.pgTemplateColumns);
          this.pnlTemplateColumns.Controls.Add(this.label6);
          this.pnlTemplateColumns.Controls.Add(this.transPropTemplate);
          this.pnlTemplateColumns.Controls.Add(this.pnlTaskDetails);
          this.pnlTemplateColumns.Dock = System.Windows.Forms.DockStyle.Right;
          this.pnlTemplateColumns.Location = new System.Drawing.Point(646, 25);
          this.pnlTemplateColumns.Name = "pnlTemplateColumns";
          this.pnlTemplateColumns.Size = new System.Drawing.Size(234, 493);
          this.pnlTemplateColumns.TabIndex = 7;
          // 
          // pgTemplateColumns
          // 
          this.pgTemplateColumns.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pgTemplateColumns.Location = new System.Drawing.Point(0, 18);
          this.pgTemplateColumns.Name = "pgTemplateColumns";
          this.pgTemplateColumns.PropertySort = System.Windows.Forms.PropertySort.NoSort;
          this.pgTemplateColumns.Size = new System.Drawing.Size(232, 172);
          this.pgTemplateColumns.TabIndex = 1;
          this.pgTemplateColumns.ToolbarVisible = false;
          this.pgTemplateColumns.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.pgTemplateColumns_PropertyValueChanged);
          // 
          // label6
          // 
          this.label6.BackColor = System.Drawing.Color.Silver;
          this.label6.Dock = System.Windows.Forms.DockStyle.Top;
          this.label6.Location = new System.Drawing.Point(0, 0);
          this.label6.Name = "label6";
          this.label6.Size = new System.Drawing.Size(232, 18);
          this.label6.TabIndex = 0;
          this.label6.Text = "Template Column Properties";
          this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // transPropTemplate
          // 
          this.transPropTemplate.AllowShowDialog = false;
          this.transPropTemplate.DatabaseConnString = null;
          this.transPropTemplate.DbTableName = null;
          this.transPropTemplate.Dock = System.Windows.Forms.DockStyle.Bottom;
          this.transPropTemplate.Location = new System.Drawing.Point(0, 190);
          this.transPropTemplate.Name = "transPropTemplate";
          this.transPropTemplate.PgPluginTarget = XLETL.Model.Globals.PluginTarget.Target;
          this.transPropTemplate.Size = new System.Drawing.Size(232, 134);
          this.transPropTemplate.TabIndex = 2;
          this.transPropTemplate.TransList = ((System.Collections.Generic.List<XLETL.Model.PluginInfo>)(resources.GetObject("transPropTemplate.TransList")));
          this.transPropTemplate.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(this.transPropTemplate_PropertyChanged);
          // 
          // pnlTaskDetails
          // 
          this.pnlTaskDetails.BackColor = System.Drawing.SystemColors.Control;
          this.pnlTaskDetails.Controls.Add(this.pgTaskDetails);
          this.pnlTaskDetails.Controls.Add(this.label5);
          this.pnlTaskDetails.Dock = System.Windows.Forms.DockStyle.Bottom;
          this.pnlTaskDetails.Location = new System.Drawing.Point(0, 324);
          this.pnlTaskDetails.Name = "pnlTaskDetails";
          this.pnlTaskDetails.Size = new System.Drawing.Size(232, 167);
          this.pnlTaskDetails.TabIndex = 6;
          // 
          // pgTaskDetails
          // 
          this.pgTaskDetails.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pgTaskDetails.Location = new System.Drawing.Point(0, 18);
          this.pgTaskDetails.Name = "pgTaskDetails";
          this.pgTaskDetails.PropertySort = System.Windows.Forms.PropertySort.NoSort;
          this.pgTaskDetails.Size = new System.Drawing.Size(232, 149);
          this.pgTaskDetails.TabIndex = 1;
          this.pgTaskDetails.ToolbarVisible = false;
          // 
          // label5
          // 
          this.label5.BackColor = System.Drawing.Color.Silver;
          this.label5.Dock = System.Windows.Forms.DockStyle.Top;
          this.label5.Location = new System.Drawing.Point(0, 0);
          this.label5.Name = "label5";
          this.label5.Size = new System.Drawing.Size(232, 18);
          this.label5.TabIndex = 0;
          this.label5.Text = "Task Details";
          this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // dgvTemplateDesign
          // 
          this.dgvTemplateDesign.AllowDrop = true;
          this.dgvTemplateDesign.BackgroundColor = System.Drawing.SystemColors.Window;
          this.dgvTemplateDesign.BorderStyle = System.Windows.Forms.BorderStyle.None;
          this.dgvTemplateDesign.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dgvTemplateDesign.Dock = System.Windows.Forms.DockStyle.Fill;
          this.dgvTemplateDesign.Location = new System.Drawing.Point(3, 3);
          this.dgvTemplateDesign.MultiSelect = false;
          this.dgvTemplateDesign.Name = "dgvTemplateDesign";
          this.dgvTemplateDesign.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullColumnSelect;
          this.dgvTemplateDesign.Size = new System.Drawing.Size(385, 252);
          this.dgvTemplateDesign.TabIndex = 2;
          this.toolTip1.SetToolTip(this.dgvTemplateDesign, "Drag & Drop column back into Target Columns data grid");
          this.dgvTemplateDesign.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvTemplateDesign_MouseDown);
          this.dgvTemplateDesign.Enter += new System.EventHandler(this.dgvTemplateDesign_Enter);
          this.dgvTemplateDesign.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvTemplateDesign_RowPostPaint);
          this.dgvTemplateDesign.DragEnter += new System.Windows.Forms.DragEventHandler(this.dgvTemplateDesign_DragEnter);
          this.dgvTemplateDesign.SelectionChanged += new System.EventHandler(this.dgvTemplateDesign_SelectionChanged);
          this.dgvTemplateDesign.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgvTemplateDesign_DragDrop);
          // 
          // pnlSqlScript
          // 
          this.pnlSqlScript.BackColor = System.Drawing.SystemColors.Control;
          this.pnlSqlScript.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pnlSqlScript.Controls.Add(this.btnScriptLoad);
          this.pnlSqlScript.Controls.Add(this.cbSqlScriptType);
          this.pnlSqlScript.Controls.Add(this.btnScriptTestRun);
          this.pnlSqlScript.Controls.Add(this.btnScriptClear);
          this.pnlSqlScript.Controls.Add(this.rtbPreLoadScript);
          this.pnlSqlScript.Controls.Add(this.label7);
          this.pnlSqlScript.Dock = System.Windows.Forms.DockStyle.Bottom;
          this.pnlSqlScript.Location = new System.Drawing.Point(239, 317);
          this.pnlSqlScript.Name = "pnlSqlScript";
          this.pnlSqlScript.Size = new System.Drawing.Size(399, 201);
          this.pnlSqlScript.TabIndex = 4;
          // 
          // btnScriptLoad
          // 
          this.btnScriptLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnScriptLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.btnScriptLoad.Location = new System.Drawing.Point(156, 26);
          this.btnScriptLoad.Name = "btnScriptLoad";
          this.btnScriptLoad.Size = new System.Drawing.Size(75, 18);
          this.btnScriptLoad.TabIndex = 15;
          this.btnScriptLoad.Text = "Load";
          this.btnScriptLoad.TextAlign = System.Drawing.ContentAlignment.TopCenter;
          this.btnScriptLoad.UseVisualStyleBackColor = true;
          // 
          // cbSqlScriptType
          // 
          this.cbSqlScriptType.FormattingEnabled = true;
          this.cbSqlScriptType.Items.AddRange(new object[] {
            "Pre-Load",
            "Post-Load"});
          this.cbSqlScriptType.Location = new System.Drawing.Point(5, 23);
          this.cbSqlScriptType.Name = "cbSqlScriptType";
          this.cbSqlScriptType.Size = new System.Drawing.Size(107, 21);
          this.cbSqlScriptType.TabIndex = 14;
          // 
          // btnScriptTestRun
          // 
          this.btnScriptTestRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnScriptTestRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.btnScriptTestRun.Location = new System.Drawing.Point(236, 26);
          this.btnScriptTestRun.Name = "btnScriptTestRun";
          this.btnScriptTestRun.Size = new System.Drawing.Size(75, 18);
          this.btnScriptTestRun.TabIndex = 13;
          this.btnScriptTestRun.Text = "Test Run";
          this.btnScriptTestRun.TextAlign = System.Drawing.ContentAlignment.TopCenter;
          this.btnScriptTestRun.UseVisualStyleBackColor = true;
          // 
          // btnScriptClear
          // 
          this.btnScriptClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
          this.btnScriptClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.btnScriptClear.Location = new System.Drawing.Point(316, 26);
          this.btnScriptClear.Name = "btnScriptClear";
          this.btnScriptClear.Size = new System.Drawing.Size(75, 18);
          this.btnScriptClear.TabIndex = 10;
          this.btnScriptClear.Text = "Clear";
          this.btnScriptClear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
          this.btnScriptClear.UseVisualStyleBackColor = true;
          // 
          // rtbPreLoadScript
          // 
          this.rtbPreLoadScript.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.rtbPreLoadScript.Location = new System.Drawing.Point(3, 48);
          this.rtbPreLoadScript.Name = "rtbPreLoadScript";
          this.rtbPreLoadScript.Size = new System.Drawing.Size(389, 147);
          this.rtbPreLoadScript.TabIndex = 9;
          this.rtbPreLoadScript.Text = "";
          // 
          // label7
          // 
          this.label7.BackColor = System.Drawing.Color.Silver;
          this.label7.Dock = System.Windows.Forms.DockStyle.Top;
          this.label7.Location = new System.Drawing.Point(0, 0);
          this.label7.Name = "label7";
          this.label7.Size = new System.Drawing.Size(397, 18);
          this.label7.TabIndex = 1;
          this.label7.Text = "Add Sql Script";
          this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // pnlErrorHelp
          // 
          this.pnlErrorHelp.BackColor = System.Drawing.SystemColors.Control;
          this.pnlErrorHelp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pnlErrorHelp.Controls.Add(this.splitContainer2);
          this.pnlErrorHelp.Dock = System.Windows.Forms.DockStyle.Bottom;
          this.pnlErrorHelp.Location = new System.Drawing.Point(0, 521);
          this.pnlErrorHelp.Name = "pnlErrorHelp";
          this.pnlErrorHelp.Size = new System.Drawing.Size(880, 144);
          this.pnlErrorHelp.TabIndex = 2;
          // 
          // splitContainer2
          // 
          this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
          this.splitContainer2.Location = new System.Drawing.Point(0, 0);
          this.splitContainer2.Name = "splitContainer2";
          // 
          // splitContainer2.Panel1
          // 
          this.splitContainer2.Panel1.Controls.Add(this.rtbErrors);
          this.splitContainer2.Panel1.Controls.Add(this.label3);
          // 
          // splitContainer2.Panel2
          // 
          this.splitContainer2.Panel2.Controls.Add(this.rtbHelp);
          this.splitContainer2.Panel2.Controls.Add(this.label4);
          this.splitContainer2.Size = new System.Drawing.Size(878, 142);
          this.splitContainer2.SplitterDistance = 455;
          this.splitContainer2.TabIndex = 1;
          // 
          // rtbErrors
          // 
          this.rtbErrors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.rtbErrors.Dock = System.Windows.Forms.DockStyle.Fill;
          this.rtbErrors.ForeColor = System.Drawing.Color.Red;
          this.rtbErrors.Location = new System.Drawing.Point(0, 18);
          this.rtbErrors.Name = "rtbErrors";
          this.rtbErrors.Size = new System.Drawing.Size(455, 124);
          this.rtbErrors.TabIndex = 1;
          this.rtbErrors.Text = "";
          // 
          // label3
          // 
          this.label3.BackColor = System.Drawing.Color.Silver;
          this.label3.Dock = System.Windows.Forms.DockStyle.Top;
          this.label3.Location = new System.Drawing.Point(0, 0);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(455, 18);
          this.label3.TabIndex = 0;
          this.label3.Text = "Errors";
          this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // rtbHelp
          // 
          this.rtbHelp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.rtbHelp.Dock = System.Windows.Forms.DockStyle.Fill;
          this.rtbHelp.Location = new System.Drawing.Point(0, 18);
          this.rtbHelp.Name = "rtbHelp";
          this.rtbHelp.Size = new System.Drawing.Size(419, 124);
          this.rtbHelp.TabIndex = 2;
          this.rtbHelp.Text = "";
          // 
          // label4
          // 
          this.label4.BackColor = System.Drawing.Color.Silver;
          this.label4.Dock = System.Windows.Forms.DockStyle.Top;
          this.label4.Location = new System.Drawing.Point(0, 0);
          this.label4.Name = "label4";
          this.label4.Size = new System.Drawing.Size(419, 18);
          this.label4.TabIndex = 1;
          this.label4.Text = "Help";
          this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // statusStrip1
          // 
          this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1});
          this.statusStrip1.Location = new System.Drawing.Point(0, 665);
          this.statusStrip1.Name = "statusStrip1";
          this.statusStrip1.Size = new System.Drawing.Size(880, 22);
          this.statusStrip1.TabIndex = 0;
          this.statusStrip1.Text = "statusStrip1";
          // 
          // toolStripStatusLabel1
          // 
          this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
          this.toolStripStatusLabel1.Size = new System.Drawing.Size(38, 17);
          this.toolStripStatusLabel1.Text = "Ready";
          // 
          // toolStripProgressBar1
          // 
          this.toolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.toolStripProgressBar1.AutoSize = false;
          this.toolStripProgressBar1.Name = "toolStripProgressBar1";
          this.toolStripProgressBar1.Size = new System.Drawing.Size(300, 16);
          this.toolStripProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
          this.toolStripProgressBar1.Visible = false;
          // 
          // toolStrip1
          // 
          this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
          this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.tsDatabaseName,
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.tsCbTableList,
            this.tsBtnSave,
            this.tsValidate});
          this.toolStrip1.Location = new System.Drawing.Point(0, 0);
          this.toolStrip1.Name = "toolStrip1";
          this.toolStrip1.Size = new System.Drawing.Size(880, 25);
          this.toolStrip1.TabIndex = 1;
          this.toolStrip1.Text = "toolStrip1";
          // 
          // toolStripLabel1
          // 
          this.toolStripLabel1.Name = "toolStripLabel1";
          this.toolStripLabel1.Size = new System.Drawing.Size(101, 22);
          this.toolStripLabel1.Text = "  Target Database: ";
          // 
          // tsDatabaseName
          // 
          this.tsDatabaseName.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
          this.tsDatabaseName.Name = "tsDatabaseName";
          this.tsDatabaseName.Size = new System.Drawing.Size(54, 22);
          this.tsDatabaseName.Text = "Unilever";
          // 
          // toolStripSeparator1
          // 
          this.toolStripSeparator1.Name = "toolStripSeparator1";
          this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
          // 
          // toolStripLabel2
          // 
          this.toolStripLabel2.Name = "toolStripLabel2";
          this.toolStripLabel2.Size = new System.Drawing.Size(81, 22);
          this.toolStripLabel2.Text = "   Target Table:";
          // 
          // tsCbTableList
          // 
          this.tsCbTableList.DropDownWidth = 200;
          this.tsCbTableList.Name = "tsCbTableList";
          this.tsCbTableList.Size = new System.Drawing.Size(300, 25);
          this.tsCbTableList.SelectedIndexChanged += new System.EventHandler(this.tsCbTableList_SelectedIndexChanged);
          // 
          // tsBtnSave
          // 
          this.tsBtnSave.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.tsBtnSave.Image = ((System.Drawing.Image)(resources.GetObject("tsBtnSave.Image")));
          this.tsBtnSave.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.tsBtnSave.Name = "tsBtnSave";
          this.tsBtnSave.Size = new System.Drawing.Size(76, 22);
          this.tsBtnSave.Text = "Save Task";
          this.tsBtnSave.Click += new System.EventHandler(this.tsBtnSave_Click);
          // 
          // tsValidate
          // 
          this.tsValidate.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.tsValidate.Image = ((System.Drawing.Image)(resources.GetObject("tsValidate.Image")));
          this.tsValidate.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.tsValidate.Name = "tsValidate";
          this.tsValidate.Size = new System.Drawing.Size(65, 22);
          this.tsValidate.Text = "Validate";
          this.tsValidate.ToolTipText = "Validate Task Configuration";
          this.tsValidate.Click += new System.EventHandler(this.tsValidate_Click);
          // 
          // collapsibleSplitter1
          // 
          this.collapsibleSplitter1.AnimationDelay = 20;
          this.collapsibleSplitter1.AnimationStep = 20;
          this.collapsibleSplitter1.BorderStyle3D = System.Windows.Forms.Border3DStyle.Flat;
          this.collapsibleSplitter1.ControlToHide = this.pnlErrorHelp;
          this.collapsibleSplitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
          this.collapsibleSplitter1.ExpandParentForm = false;
          this.collapsibleSplitter1.Location = new System.Drawing.Point(0, 518);
          this.collapsibleSplitter1.Name = "collapsibleSplitter1";
          this.collapsibleSplitter1.TabIndex = 8;
          this.collapsibleSplitter1.TabStop = false;
          this.collapsibleSplitter1.UseAnimations = false;
          this.collapsibleSplitter1.VisualStyle = NJFLib.Controls.VisualStyles.XP;
          // 
          // collapsibleSplitter2
          // 
          this.collapsibleSplitter2.AnimationDelay = 20;
          this.collapsibleSplitter2.AnimationStep = 20;
          this.collapsibleSplitter2.BorderStyle3D = System.Windows.Forms.Border3DStyle.Flat;
          this.collapsibleSplitter2.ControlToHide = this.pnlTargetColumns;
          this.collapsibleSplitter2.ExpandParentForm = false;
          this.collapsibleSplitter2.Location = new System.Drawing.Point(231, 25);
          this.collapsibleSplitter2.Name = "collapsibleSplitter2";
          this.collapsibleSplitter2.TabIndex = 9;
          this.collapsibleSplitter2.TabStop = false;
          this.collapsibleSplitter2.UseAnimations = false;
          this.collapsibleSplitter2.VisualStyle = NJFLib.Controls.VisualStyles.XP;
          // 
          // collapsibleSplitter3
          // 
          this.collapsibleSplitter3.AnimationDelay = 20;
          this.collapsibleSplitter3.AnimationStep = 20;
          this.collapsibleSplitter3.BorderStyle3D = System.Windows.Forms.Border3DStyle.Flat;
          this.collapsibleSplitter3.ControlToHide = this.pnlTemplateColumns;
          this.collapsibleSplitter3.Dock = System.Windows.Forms.DockStyle.Right;
          this.collapsibleSplitter3.ExpandParentForm = false;
          this.collapsibleSplitter3.Location = new System.Drawing.Point(638, 25);
          this.collapsibleSplitter3.Name = "collapsibleSplitter3";
          this.collapsibleSplitter3.TabIndex = 10;
          this.collapsibleSplitter3.TabStop = false;
          this.collapsibleSplitter3.UseAnimations = false;
          this.collapsibleSplitter3.VisualStyle = NJFLib.Controls.VisualStyles.XP;
          // 
          // tabControl1
          // 
          this.tabControl1.Controls.Add(this.tabPage1);
          this.tabControl1.Controls.Add(this.tabPage2);
          this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.tabControl1.Location = new System.Drawing.Point(239, 25);
          this.tabControl1.Name = "tabControl1";
          this.tabControl1.SelectedIndex = 0;
          this.tabControl1.Size = new System.Drawing.Size(399, 284);
          this.tabControl1.TabIndex = 11;
          this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
          // 
          // tabPage1
          // 
          this.tabPage1.Controls.Add(this.dgvTemplateDesign);
          this.tabPage1.Location = new System.Drawing.Point(4, 22);
          this.tabPage1.Name = "tabPage1";
          this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
          this.tabPage1.Size = new System.Drawing.Size(391, 258);
          this.tabPage1.TabIndex = 0;
          this.tabPage1.Text = "Template Designer";
          this.tabPage1.UseVisualStyleBackColor = true;
          // 
          // tabPage2
          // 
          this.tabPage2.Controls.Add(this.label8);
          this.tabPage2.Controls.Add(this.cmbDbTableList);
          this.tabPage2.Controls.Add(this.dgvTargetData);
          this.tabPage2.Location = new System.Drawing.Point(4, 22);
          this.tabPage2.Name = "tabPage2";
          this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
          this.tabPage2.Size = new System.Drawing.Size(391, 253);
          this.tabPage2.TabIndex = 1;
          this.tabPage2.Text = "Target Data Viewer";
          this.tabPage2.UseVisualStyleBackColor = true;
          // 
          // label8
          // 
          this.label8.AutoSize = true;
          this.label8.Location = new System.Drawing.Point(11, 9);
          this.label8.Name = "label8";
          this.label8.Size = new System.Drawing.Size(37, 13);
          this.label8.TabIndex = 2;
          this.label8.Text = "Table:";
          // 
          // cmbDbTableList
          // 
          this.cmbDbTableList.FormattingEnabled = true;
          this.cmbDbTableList.Location = new System.Drawing.Point(54, 5);
          this.cmbDbTableList.Name = "cmbDbTableList";
          this.cmbDbTableList.Size = new System.Drawing.Size(334, 21);
          this.cmbDbTableList.TabIndex = 1;
          this.cmbDbTableList.SelectedIndexChanged += new System.EventHandler(this.cmbDbTableList_SelectedIndexChanged);
          // 
          // dgvTargetData
          // 
          this.dgvTargetData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                      | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.dgvTargetData.BackgroundColor = System.Drawing.Color.White;
          this.dgvTargetData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dgvTargetData.Location = new System.Drawing.Point(3, 29);
          this.dgvTargetData.Name = "dgvTargetData";
          this.dgvTargetData.ReadOnly = true;
          this.dgvTargetData.RowHeadersVisible = false;
          this.dgvTargetData.Size = new System.Drawing.Size(385, 221);
          this.dgvTargetData.TabIndex = 0;
          // 
          // collapsibleSplitter4
          // 
          this.collapsibleSplitter4.AnimationDelay = 20;
          this.collapsibleSplitter4.AnimationStep = 20;
          this.collapsibleSplitter4.BorderStyle3D = System.Windows.Forms.Border3DStyle.Flat;
          this.collapsibleSplitter4.ControlToHide = this.pnlSqlScript;
          this.collapsibleSplitter4.Dock = System.Windows.Forms.DockStyle.Bottom;
          this.collapsibleSplitter4.ExpandParentForm = false;
          this.collapsibleSplitter4.Location = new System.Drawing.Point(239, 309);
          this.collapsibleSplitter4.Name = "collapsibleSplitter3";
          this.collapsibleSplitter4.TabIndex = 12;
          this.collapsibleSplitter4.TabStop = false;
          this.collapsibleSplitter4.UseAnimations = false;
          this.collapsibleSplitter4.VisualStyle = NJFLib.Controls.VisualStyles.XP;
          // 
          // BuildTaskDialog
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.BackColor = System.Drawing.SystemColors.Control;
          this.ClientSize = new System.Drawing.Size(880, 687);
          this.Controls.Add(this.tabControl1);
          this.Controls.Add(this.collapsibleSplitter4);
          this.Controls.Add(this.pnlSqlScript);
          this.Controls.Add(this.collapsibleSplitter2);
          this.Controls.Add(this.collapsibleSplitter3);
          this.Controls.Add(this.pnlTargetColumns);
          this.Controls.Add(this.pnlTemplateColumns);
          this.Controls.Add(this.toolStrip1);
          this.Controls.Add(this.collapsibleSplitter1);
          this.Controls.Add(this.pnlErrorHelp);
          this.Controls.Add(this.statusStrip1);
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.MinimizeBox = false;
          this.Name = "BuildTaskDialog";
          this.ShowIcon = false;
          this.ShowInTaskbar = false;
          this.Text = "TaskDialog";
          this.Load += new System.EventHandler(this.TaskDialog_Load);
          this.pnlTargetColumns.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.dgvTargetColumnList)).EndInit();
          this.pnlTargetColumnProperties.ResumeLayout(false);
          this.pnlTemplateColumns.ResumeLayout(false);
          this.pnlTaskDetails.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.dgvTemplateDesign)).EndInit();
          this.pnlSqlScript.ResumeLayout(false);
          this.pnlErrorHelp.ResumeLayout(false);
          this.splitContainer2.Panel1.ResumeLayout(false);
          this.splitContainer2.Panel2.ResumeLayout(false);
          this.splitContainer2.ResumeLayout(false);
          this.statusStrip1.ResumeLayout(false);
          this.statusStrip1.PerformLayout();
          this.toolStrip1.ResumeLayout(false);
          this.toolStrip1.PerformLayout();
          this.tabControl1.ResumeLayout(false);
          this.tabPage1.ResumeLayout(false);
          this.tabPage2.ResumeLayout(false);
          this.tabPage2.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dgvTargetData)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTargetColumns;
        private System.Windows.Forms.DataGridView dgvTargetColumnList;
        private System.Windows.Forms.PropertyGrid pgTargetColumns;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlTemplateColumns;
        private System.Windows.Forms.PropertyGrid pgTemplateColumns;
        private System.Windows.Forms.Label label6;
        private XLETL.TaskManager.Controls.TransformationProperty transPropTemplate;
        private System.Windows.Forms.DataGridView dgvTemplateDesign;
        private System.Windows.Forms.Panel pnlTaskDetails;
        private System.Windows.Forms.PropertyGrid pgTaskDetails;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnlSqlScript;
        private System.Windows.Forms.Button btnScriptTestRun;
        private System.Windows.Forms.Button btnScriptClear;
        private System.Windows.Forms.RichTextBox rtbPreLoadScript;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlErrorHelp;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.RichTextBox rtbErrors;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rtbHelp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox tsCbTableList;
        private NJFLib.Controls.CollapsibleSplitter collapsibleSplitter1;
        private NJFLib.Controls.CollapsibleSplitter collapsibleSplitter2;
        private XLETL.TaskManager.Controls.TransformationProperty tpTarget;
        private System.Windows.Forms.ToolStripLabel tsDatabaseName;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private NJFLib.Controls.CollapsibleSplitter collapsibleSplitter3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStripButton tsBtnSave;
        private System.Windows.Forms.ComboBox cbSqlScriptType;
        private System.Windows.Forms.Button btnScriptLoad;
        private NJFLib.Controls.CollapsibleSplitter collapsibleSplitter4;
        private System.Windows.Forms.Panel pnlTargetColumnProperties;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.DataGridView dgvTargetData;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbDbTableList;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripButton tsValidate;        
    }
}