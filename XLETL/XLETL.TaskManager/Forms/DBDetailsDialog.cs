using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Configuration;
using XLETL.Model;
using XLETL.BLL;
using XLETL.ExceptionHandling;

namespace XLETL.TaskManager.Forms
{
    public partial class DBDetailsDialog : Form
    {
        #region Properties

        /// <summary>
        /// Hold the status of save task's operation to report it to the mainform
        /// </summary>
        public bool IsSaveSuccess { get; set; }

        /// <summary>
        /// Hold identity of database which needs to edit
        /// </summary>
        public Guid DBID { get; private set; }

        #endregion

        public DBDetailsDialog() 
        {
            InitializeComponent();

            this.Text = "Create Database";
        }
               
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="isCreate"></param>
        public DBDetailsDialog(DBInfo dbInfo)
        {
            InitializeComponent();

            // Set appropriate dialog title
            this.Text = "Edit Database";
            DBID = dbInfo.DBId;
            this.tbDBFriendlyName.Text = dbInfo.DBFriendlyName;
            this.tbDescription.Text = dbInfo.DBDesc;
            //this.tbConnString.Text = companyInfo.ConnString;            
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                // Save Database Details to persistent storage
                DBInfo dbInfo = new DBInfo();
                dbInfo.DBFriendlyName = tbDBFriendlyName.Text;
                dbInfo.DBDesc = tbDescription.Text;
                dbInfo.DatabaseName = dbConnector1.DatabaseName;
                dbInfo.ConnString = dbConnector1.ConnectionString;
                dbInfo.DALClassName = dbConnector1.DALClassName;

                if (DBID != null && !DBID.Equals(Guid.Empty))
                    dbInfo.DBId = DBID;

                // Instantiate BL's database object
                DB db = new DB();
                db.SaveDB(dbInfo);
                IsSaveSuccess = true;
            }
            catch (Exception ex)
            {
                IsSaveSuccess = false;

                //if (ExceptionHandleProvider.HandleException(ex, "Global Policy") == true)
                //{
                //    throw ex;
                //}
                throw new ApplicationException("Error occured", ex);
            }
            finally
            {
                this.DialogResult = DialogResult.OK;
            }            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        
    }
}