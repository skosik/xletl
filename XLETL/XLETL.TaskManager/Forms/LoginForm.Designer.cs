﻿namespace XLETL.TaskManager.Forms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.tbRepPassword = new System.Windows.Forms.TextBox();
          this.lblRepPass = new System.Windows.Forms.Label();
          this.lblInfo = new System.Windows.Forms.Label();
          this.pictureBox1 = new System.Windows.Forms.PictureBox();
          this.tbPassword = new System.Windows.Forms.TextBox();
          this.lblPassword = new System.Windows.Forms.Label();
          this.tbUsername = new System.Windows.Forms.TextBox();
          this.lblUsername = new System.Windows.Forms.Label();
          this.btnLogin = new System.Windows.Forms.Button();
          this.btnCancel = new System.Windows.Forms.Button();
          this.groupBox1.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
          this.SuspendLayout();
          // 
          // groupBox1
          // 
          this.groupBox1.Controls.Add(this.tbRepPassword);
          this.groupBox1.Controls.Add(this.lblRepPass);
          this.groupBox1.Controls.Add(this.lblInfo);
          this.groupBox1.Controls.Add(this.pictureBox1);
          this.groupBox1.Controls.Add(this.tbPassword);
          this.groupBox1.Controls.Add(this.lblPassword);
          this.groupBox1.Controls.Add(this.tbUsername);
          this.groupBox1.Controls.Add(this.lblUsername);
          this.groupBox1.Location = new System.Drawing.Point(12, 8);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(313, 180);
          this.groupBox1.TabIndex = 0;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Authentication";
          // 
          // tbRepPassword
          // 
          this.tbRepPassword.Location = new System.Drawing.Point(152, 149);
          this.tbRepPassword.Name = "tbRepPassword";
          this.tbRepPassword.PasswordChar = '*';
          this.tbRepPassword.Size = new System.Drawing.Size(146, 20);
          this.tbRepPassword.TabIndex = 7;
          // 
          // lblRepPass
          // 
          this.lblRepPass.AutoSize = true;
          this.lblRepPass.Location = new System.Drawing.Point(43, 153);
          this.lblRepPass.Name = "lblRepPass";
          this.lblRepPass.Size = new System.Drawing.Size(94, 13);
          this.lblRepPass.TabIndex = 6;
          this.lblRepPass.Text = "Repeat Password:";
          // 
          // lblInfo
          // 
          this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.lblInfo.Location = new System.Drawing.Point(84, 15);
          this.lblInfo.Name = "lblInfo";
          this.lblInfo.Size = new System.Drawing.Size(221, 58);
          this.lblInfo.TabIndex = 5;
          this.lblInfo.Text = "Administrator login is required. Please enter Username and Password in order to a" +
              "ccess the application.";
          this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // pictureBox1
          // 
          this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
          this.pictureBox1.Location = new System.Drawing.Point(14, 37);
          this.pictureBox1.Name = "pictureBox1";
          this.pictureBox1.Size = new System.Drawing.Size(53, 57);
          this.pictureBox1.TabIndex = 4;
          this.pictureBox1.TabStop = false;
          // 
          // tbPassword
          // 
          this.tbPassword.Location = new System.Drawing.Point(152, 119);
          this.tbPassword.Name = "tbPassword";
          this.tbPassword.PasswordChar = '*';
          this.tbPassword.Size = new System.Drawing.Size(146, 20);
          this.tbPassword.TabIndex = 3;
          // 
          // lblPassword
          // 
          this.lblPassword.AutoSize = true;
          this.lblPassword.Location = new System.Drawing.Point(81, 123);
          this.lblPassword.Name = "lblPassword";
          this.lblPassword.Size = new System.Drawing.Size(56, 13);
          this.lblPassword.TabIndex = 2;
          this.lblPassword.Text = "Password:";
          // 
          // tbUsername
          // 
          this.tbUsername.Location = new System.Drawing.Point(152, 89);
          this.tbUsername.Name = "tbUsername";
          this.tbUsername.Size = new System.Drawing.Size(146, 20);
          this.tbUsername.TabIndex = 1;
          // 
          // lblUsername
          // 
          this.lblUsername.AutoSize = true;
          this.lblUsername.Location = new System.Drawing.Point(79, 93);
          this.lblUsername.Name = "lblUsername";
          this.lblUsername.Size = new System.Drawing.Size(58, 13);
          this.lblUsername.TabIndex = 0;
          this.lblUsername.Text = "Username:";
          // 
          // btnLogin
          // 
          this.btnLogin.Location = new System.Drawing.Point(93, 200);
          this.btnLogin.Name = "btnLogin";
          this.btnLogin.Size = new System.Drawing.Size(75, 23);
          this.btnLogin.TabIndex = 1;
          this.btnLogin.Text = "Login";
          this.btnLogin.UseVisualStyleBackColor = true;
          this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
          // 
          // btnCancel
          // 
          this.btnCancel.Location = new System.Drawing.Point(174, 200);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(75, 23);
          this.btnCancel.TabIndex = 2;
          this.btnCancel.Text = "Cancel";
          this.btnCancel.UseVisualStyleBackColor = true;
          this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
          // 
          // LoginForm
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(337, 235);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnLogin);
          this.Controls.Add(this.groupBox1);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.Name = "LoginForm";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "XLETL Task Manager Login";
          this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LoginForm_FormClosing);
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.TextBox tbRepPassword;
        private System.Windows.Forms.Label lblRepPass;
    }
}