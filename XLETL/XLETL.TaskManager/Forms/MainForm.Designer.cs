namespace XLETL.TaskManager.Forms
{
    public partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
          this.menuStrip1 = new System.Windows.Forms.MenuStrip();
          this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.createDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.editDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.deleteDBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.userToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.createUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.editUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.deleteUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.taskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.createTaskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.editTaskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.deleteTaskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.transformationPluginsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
          this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.splitContainer1 = new System.Windows.Forms.SplitContainer();
          this.splitContainer2 = new System.Windows.Forms.SplitContainer();
          this.tvDBs = new System.Windows.Forms.TreeView();
          this.tsDbs = new System.Windows.Forms.ToolStrip();
          this.toolStripAddDB = new System.Windows.Forms.ToolStripButton();
          this.toolStripEditDB = new System.Windows.Forms.ToolStripButton();
          this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
          this.toolStripAddUser = new System.Windows.Forms.ToolStripButton();
          this.toolStripEditUser = new System.Windows.Forms.ToolStripButton();
          this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
          this.toolStripAddTask = new System.Windows.Forms.ToolStripButton();
          this.toolStripEditTask = new System.Windows.Forms.ToolStripButton();
          this.pnlCompanyDetails = new System.Windows.Forms.Panel();
          this.label10 = new System.Windows.Forms.Label();
          this.tbDBDesc = new System.Windows.Forms.TextBox();
          this.label7 = new System.Windows.Forms.Label();
          this.label11 = new System.Windows.Forms.Label();
          this.tbDatabaseProvider = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.tbDatabase = new System.Windows.Forms.TextBox();
          this.label1 = new System.Windows.Forms.Label();
          this.tbDBFriendlyName = new System.Windows.Forms.TextBox();
          this.splitContainer3 = new System.Windows.Forms.SplitContainer();
          this.dgvItems = new System.Windows.Forms.DataGridView();
          this.toolStrip1 = new System.Windows.Forms.ToolStrip();
          this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
          this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
          this.tsbGenerateTemplate = new System.Windows.Forms.ToolStripButton();
          this.tsbViewTemplate = new System.Windows.Forms.ToolStripButton();
          this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
          this.tsbViewETLLog = new System.Windows.Forms.ToolStripButton();
          this.tsbETLProcess = new System.Windows.Forms.ToolStripButton();
          this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
          this.tsbTaskSchedule = new System.Windows.Forms.ToolStripButton();
          this.splitContainer4 = new System.Windows.Forms.SplitContainer();
          this.panel1 = new System.Windows.Forms.Panel();
          this.tbLastName = new System.Windows.Forms.TextBox();
          this.label8 = new System.Windows.Forms.Label();
          this.label9 = new System.Windows.Forms.Label();
          this.label13 = new System.Windows.Forms.Label();
          this.tbEmail = new System.Windows.Forms.TextBox();
          this.label14 = new System.Windows.Forms.Label();
          this.tbPhone = new System.Windows.Forms.TextBox();
          this.label15 = new System.Windows.Forms.Label();
          this.tbFirstName = new System.Windows.Forms.TextBox();
          this.pnlTaskDetails = new System.Windows.Forms.Panel();
          this.label16 = new System.Windows.Forms.Label();
          this.tbTargetColNo = new System.Windows.Forms.TextBox();
          this.label6 = new System.Windows.Forms.Label();
          this.label12 = new System.Windows.Forms.Label();
          this.tbNoOfTemplColumns = new System.Windows.Forms.TextBox();
          this.label3 = new System.Windows.Forms.Label();
          this.label5 = new System.Windows.Forms.Label();
          this.tbTaskDescription = new System.Windows.Forms.TextBox();
          this.tbTargetTableName = new System.Windows.Forms.TextBox();
          this.tbProcessMode = new System.Windows.Forms.TextBox();
          this.label4 = new System.Windows.Forms.Label();
          this.statusStrip1 = new System.Windows.Forms.StatusStrip();
          this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
          this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
          this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
          this.menuStrip1.SuspendLayout();
          this.splitContainer1.Panel1.SuspendLayout();
          this.splitContainer1.Panel2.SuspendLayout();
          this.splitContainer1.SuspendLayout();
          this.splitContainer2.Panel1.SuspendLayout();
          this.splitContainer2.Panel2.SuspendLayout();
          this.splitContainer2.SuspendLayout();
          this.tsDbs.SuspendLayout();
          this.pnlCompanyDetails.SuspendLayout();
          this.splitContainer3.Panel1.SuspendLayout();
          this.splitContainer3.Panel2.SuspendLayout();
          this.splitContainer3.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).BeginInit();
          this.toolStrip1.SuspendLayout();
          this.splitContainer4.Panel1.SuspendLayout();
          this.splitContainer4.Panel2.SuspendLayout();
          this.splitContainer4.SuspendLayout();
          this.panel1.SuspendLayout();
          this.pnlTaskDetails.SuspendLayout();
          this.statusStrip1.SuspendLayout();
          this.SuspendLayout();
          // 
          // menuStrip1
          // 
          this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.userToolStripMenuItem,
            this.taskToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.helpToolStripMenuItem});
          this.menuStrip1.Location = new System.Drawing.Point(0, 0);
          this.menuStrip1.Name = "menuStrip1";
          this.menuStrip1.Size = new System.Drawing.Size(875, 24);
          this.menuStrip1.TabIndex = 1;
          this.menuStrip1.Text = "menuStrip1";
          // 
          // editToolStripMenuItem
          // 
          this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createDBToolStripMenuItem,
            this.editDBToolStripMenuItem,
            this.deleteDBToolStripMenuItem});
          this.editToolStripMenuItem.Name = "editToolStripMenuItem";
          this.editToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
          this.editToolStripMenuItem.Text = "Database";
          // 
          // createDBToolStripMenuItem
          // 
          this.createDBToolStripMenuItem.Name = "createDBToolStripMenuItem";
          this.createDBToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
          this.createDBToolStripMenuItem.Text = "Create";
          this.createDBToolStripMenuItem.Click += new System.EventHandler(this.createDBToolStripMenuItem_Click);
          // 
          // editDBToolStripMenuItem
          // 
          this.editDBToolStripMenuItem.Name = "editDBToolStripMenuItem";
          this.editDBToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
          this.editDBToolStripMenuItem.Text = "Edit";
          this.editDBToolStripMenuItem.Click += new System.EventHandler(this.editDBToolStripMenuItem_Click);
          // 
          // deleteDBToolStripMenuItem
          // 
          this.deleteDBToolStripMenuItem.Name = "deleteDBToolStripMenuItem";
          this.deleteDBToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
          this.deleteDBToolStripMenuItem.Text = "Delete";
          this.deleteDBToolStripMenuItem.Click += new System.EventHandler(this.deleteDBToolStripMenuItem_Click);
          // 
          // userToolStripMenuItem
          // 
          this.userToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createUserToolStripMenuItem,
            this.editUserToolStripMenuItem,
            this.deleteUserToolStripMenuItem});
          this.userToolStripMenuItem.Name = "userToolStripMenuItem";
          this.userToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
          this.userToolStripMenuItem.Text = "User";
          // 
          // createUserToolStripMenuItem
          // 
          this.createUserToolStripMenuItem.Name = "createUserToolStripMenuItem";
          this.createUserToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
          this.createUserToolStripMenuItem.Text = "Create";
          this.createUserToolStripMenuItem.Click += new System.EventHandler(this.createUserToolStripMenuItem_Click);
          // 
          // editUserToolStripMenuItem
          // 
          this.editUserToolStripMenuItem.Name = "editUserToolStripMenuItem";
          this.editUserToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
          this.editUserToolStripMenuItem.Text = "Edit";
          this.editUserToolStripMenuItem.Click += new System.EventHandler(this.editUserToolStripMenuItem_Click);
          // 
          // deleteUserToolStripMenuItem
          // 
          this.deleteUserToolStripMenuItem.Name = "deleteUserToolStripMenuItem";
          this.deleteUserToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
          this.deleteUserToolStripMenuItem.Text = "Delete";
          this.deleteUserToolStripMenuItem.Click += new System.EventHandler(this.deleteUserToolStripMenuItem_Click);
          // 
          // taskToolStripMenuItem
          // 
          this.taskToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createTaskToolStripMenuItem,
            this.editTaskToolStripMenuItem,
            this.deleteTaskToolStripMenuItem});
          this.taskToolStripMenuItem.Name = "taskToolStripMenuItem";
          this.taskToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
          this.taskToolStripMenuItem.Text = "Task";
          // 
          // createTaskToolStripMenuItem
          // 
          this.createTaskToolStripMenuItem.Name = "createTaskToolStripMenuItem";
          this.createTaskToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
          this.createTaskToolStripMenuItem.Text = "Create";
          this.createTaskToolStripMenuItem.Click += new System.EventHandler(this.createTaskToolStripMenuItem_Click);
          // 
          // editTaskToolStripMenuItem
          // 
          this.editTaskToolStripMenuItem.Name = "editTaskToolStripMenuItem";
          this.editTaskToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
          this.editTaskToolStripMenuItem.Text = "Edit";
          this.editTaskToolStripMenuItem.Click += new System.EventHandler(this.editTaskToolStripMenuItem_Click);
          // 
          // deleteTaskToolStripMenuItem
          // 
          this.deleteTaskToolStripMenuItem.Name = "deleteTaskToolStripMenuItem";
          this.deleteTaskToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
          this.deleteTaskToolStripMenuItem.Text = "Delete";
          this.deleteTaskToolStripMenuItem.Click += new System.EventHandler(this.deleteTaskToolStripMenuItem_Click);
          // 
          // settingsToolStripMenuItem
          // 
          this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transformationPluginsToolStripMenuItem});
          this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
          this.settingsToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
          this.settingsToolStripMenuItem.Text = "Settings";
          // 
          // transformationPluginsToolStripMenuItem
          // 
          this.transformationPluginsToolStripMenuItem.Name = "transformationPluginsToolStripMenuItem";
          this.transformationPluginsToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
          this.transformationPluginsToolStripMenuItem.Text = "Transformation Plugins";
          this.transformationPluginsToolStripMenuItem.Click += new System.EventHandler(this.transformationPluginsToolStripMenuItem_Click);
          // 
          // helpToolStripMenuItem
          // 
          this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.toolStripSeparator3,
            this.aboutToolStripMenuItem});
          this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
          this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
          this.helpToolStripMenuItem.Text = "Help";
          // 
          // contentsToolStripMenuItem
          // 
          this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
          this.contentsToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
          this.contentsToolStripMenuItem.Text = "Contents";
          this.contentsToolStripMenuItem.Click += new System.EventHandler(this.contentsToolStripMenuItem_Click);
          // 
          // toolStripSeparator3
          // 
          this.toolStripSeparator3.Name = "toolStripSeparator3";
          this.toolStripSeparator3.Size = new System.Drawing.Size(126, 6);
          // 
          // aboutToolStripMenuItem
          // 
          this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
          this.aboutToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
          this.aboutToolStripMenuItem.Text = "About";
          this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
          // 
          // splitContainer1
          // 
          this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
          this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.splitContainer1.ForeColor = System.Drawing.SystemColors.ControlText;
          this.splitContainer1.Location = new System.Drawing.Point(0, 24);
          this.splitContainer1.Name = "splitContainer1";
          // 
          // splitContainer1.Panel1
          // 
          this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
          this.splitContainer1.Panel1MinSize = 200;
          // 
          // splitContainer1.Panel2
          // 
          this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
          this.splitContainer1.Size = new System.Drawing.Size(875, 621);
          this.splitContainer1.SplitterDistance = 240;
          this.splitContainer1.TabIndex = 2;
          // 
          // splitContainer2
          // 
          this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
          this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
          this.splitContainer2.IsSplitterFixed = true;
          this.splitContainer2.Location = new System.Drawing.Point(0, 0);
          this.splitContainer2.Name = "splitContainer2";
          this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
          // 
          // splitContainer2.Panel1
          // 
          this.splitContainer2.Panel1.Controls.Add(this.tvDBs);
          this.splitContainer2.Panel1.Controls.Add(this.tsDbs);
          this.splitContainer2.Panel1MinSize = 350;
          // 
          // splitContainer2.Panel2
          // 
          this.splitContainer2.Panel2.Controls.Add(this.pnlCompanyDetails);
          this.splitContainer2.Size = new System.Drawing.Size(240, 621);
          this.splitContainer2.SplitterDistance = 410;
          this.splitContainer2.TabIndex = 1;
          // 
          // tvDBs
          // 
          this.tvDBs.BackColor = System.Drawing.SystemColors.Control;
          this.tvDBs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.tvDBs.Dock = System.Windows.Forms.DockStyle.Fill;
          this.tvDBs.Location = new System.Drawing.Point(0, 31);
          this.tvDBs.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
          this.tvDBs.Name = "tvDBs";
          this.tvDBs.Size = new System.Drawing.Size(240, 379);
          this.tvDBs.TabIndex = 1;
          this.tvDBs.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvDBs_AfterSelect);
          // 
          // tsDbs
          // 
          this.tsDbs.ImageScalingSize = new System.Drawing.Size(24, 24);
          this.tsDbs.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripAddDB,
            this.toolStripEditDB,
            this.toolStripSeparator1,
            this.toolStripAddUser,
            this.toolStripEditUser,
            this.toolStripSeparator2,
            this.toolStripAddTask,
            this.toolStripEditTask});
          this.tsDbs.Location = new System.Drawing.Point(0, 0);
          this.tsDbs.Name = "tsDbs";
          this.tsDbs.Size = new System.Drawing.Size(240, 31);
          this.tsDbs.TabIndex = 2;
          this.tsDbs.Text = "toolStrip1";
          // 
          // toolStripAddDB
          // 
          this.toolStripAddDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripAddDB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAddDB.Image")));
          this.toolStripAddDB.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripAddDB.Name = "toolStripAddDB";
          this.toolStripAddDB.Size = new System.Drawing.Size(28, 28);
          this.toolStripAddDB.Text = "toolStripButton1";
          this.toolStripAddDB.ToolTipText = "Create Database";
          this.toolStripAddDB.Click += new System.EventHandler(this.toolStripAddDB_Click);
          // 
          // toolStripEditDB
          // 
          this.toolStripEditDB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripEditDB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEditDB.Image")));
          this.toolStripEditDB.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripEditDB.Name = "toolStripEditDB";
          this.toolStripEditDB.Size = new System.Drawing.Size(28, 28);
          this.toolStripEditDB.Text = "toolStripEditCompany";
          this.toolStripEditDB.ToolTipText = "Edit Database";
          this.toolStripEditDB.Click += new System.EventHandler(this.toolStripEditDB_Click);
          // 
          // toolStripSeparator1
          // 
          this.toolStripSeparator1.Name = "toolStripSeparator1";
          this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
          // 
          // toolStripAddUser
          // 
          this.toolStripAddUser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripAddUser.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAddUser.Image")));
          this.toolStripAddUser.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripAddUser.Name = "toolStripAddUser";
          this.toolStripAddUser.Size = new System.Drawing.Size(28, 28);
          this.toolStripAddUser.Text = "toolStripButton2";
          this.toolStripAddUser.ToolTipText = "Create User";
          this.toolStripAddUser.Click += new System.EventHandler(this.toolStripAddUser_Click);
          // 
          // toolStripEditUser
          // 
          this.toolStripEditUser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripEditUser.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEditUser.Image")));
          this.toolStripEditUser.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripEditUser.Name = "toolStripEditUser";
          this.toolStripEditUser.Size = new System.Drawing.Size(28, 28);
          this.toolStripEditUser.Text = "toolStripEditUser";
          this.toolStripEditUser.ToolTipText = "Edit User";
          this.toolStripEditUser.Click += new System.EventHandler(this.toolStripEditUser_Click);
          // 
          // toolStripSeparator2
          // 
          this.toolStripSeparator2.Name = "toolStripSeparator2";
          this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
          // 
          // toolStripAddTask
          // 
          this.toolStripAddTask.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripAddTask.Image = ((System.Drawing.Image)(resources.GetObject("toolStripAddTask.Image")));
          this.toolStripAddTask.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripAddTask.Name = "toolStripAddTask";
          this.toolStripAddTask.Size = new System.Drawing.Size(28, 28);
          this.toolStripAddTask.Text = "toolStripButton3";
          this.toolStripAddTask.ToolTipText = "Create Task";
          this.toolStripAddTask.Click += new System.EventHandler(this.toolStripAddTask_Click);
          // 
          // toolStripEditTask
          // 
          this.toolStripEditTask.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.toolStripEditTask.Image = ((System.Drawing.Image)(resources.GetObject("toolStripEditTask.Image")));
          this.toolStripEditTask.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.toolStripEditTask.Name = "toolStripEditTask";
          this.toolStripEditTask.Size = new System.Drawing.Size(28, 28);
          this.toolStripEditTask.Text = "toolStripButton3";
          this.toolStripEditTask.ToolTipText = "Edit Task";
          this.toolStripEditTask.Click += new System.EventHandler(this.toolStripEditTask_Click);
          // 
          // pnlCompanyDetails
          // 
          this.pnlCompanyDetails.AutoScroll = true;
          this.pnlCompanyDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pnlCompanyDetails.Controls.Add(this.label10);
          this.pnlCompanyDetails.Controls.Add(this.tbDBDesc);
          this.pnlCompanyDetails.Controls.Add(this.label7);
          this.pnlCompanyDetails.Controls.Add(this.label11);
          this.pnlCompanyDetails.Controls.Add(this.tbDatabaseProvider);
          this.pnlCompanyDetails.Controls.Add(this.label2);
          this.pnlCompanyDetails.Controls.Add(this.tbDatabase);
          this.pnlCompanyDetails.Controls.Add(this.label1);
          this.pnlCompanyDetails.Controls.Add(this.tbDBFriendlyName);
          this.pnlCompanyDetails.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pnlCompanyDetails.Location = new System.Drawing.Point(0, 0);
          this.pnlCompanyDetails.Name = "pnlCompanyDetails";
          this.pnlCompanyDetails.Size = new System.Drawing.Size(240, 207);
          this.pnlCompanyDetails.TabIndex = 1;
          // 
          // label10
          // 
          this.label10.AutoSize = true;
          this.label10.Location = new System.Drawing.Point(9, 64);
          this.label10.Name = "label10";
          this.label10.Size = new System.Drawing.Size(60, 13);
          this.label10.TabIndex = 8;
          this.label10.Text = "Description";
          // 
          // tbDBDesc
          // 
          this.tbDBDesc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbDBDesc.Location = new System.Drawing.Point(10, 80);
          this.tbDBDesc.Multiline = true;
          this.tbDBDesc.Name = "tbDBDesc";
          this.tbDBDesc.ReadOnly = true;
          this.tbDBDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.tbDBDesc.Size = new System.Drawing.Size(213, 30);
          this.tbDBDesc.TabIndex = 7;
          // 
          // label7
          // 
          this.label7.BackColor = System.Drawing.Color.Silver;
          this.label7.Dock = System.Windows.Forms.DockStyle.Top;
          this.label7.Location = new System.Drawing.Point(0, 0);
          this.label7.Name = "label7";
          this.label7.Size = new System.Drawing.Size(238, 18);
          this.label7.TabIndex = 6;
          this.label7.Text = "Database Details";
          // 
          // label11
          // 
          this.label11.AutoSize = true;
          this.label11.Location = new System.Drawing.Point(9, 118);
          this.label11.Name = "label11";
          this.label11.Size = new System.Drawing.Size(95, 13);
          this.label11.TabIndex = 5;
          this.label11.Text = "Database Provider";
          // 
          // tbDatabaseProvider
          // 
          this.tbDatabaseProvider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbDatabaseProvider.Location = new System.Drawing.Point(10, 134);
          this.tbDatabaseProvider.Name = "tbDatabaseProvider";
          this.tbDatabaseProvider.ReadOnly = true;
          this.tbDatabaseProvider.Size = new System.Drawing.Size(213, 20);
          this.tbDatabaseProvider.TabIndex = 4;
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(9, 160);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(84, 13);
          this.label2.TabIndex = 3;
          this.label2.Text = "Database Name";
          // 
          // tbDatabase
          // 
          this.tbDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbDatabase.Location = new System.Drawing.Point(10, 176);
          this.tbDatabase.Name = "tbDatabase";
          this.tbDatabase.ReadOnly = true;
          this.tbDatabase.Size = new System.Drawing.Size(213, 20);
          this.tbDatabase.TabIndex = 2;
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(9, 23);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(74, 13);
          this.label1.TabIndex = 1;
          this.label1.Text = "Friendly Name";
          // 
          // tbDBFriendlyName
          // 
          this.tbDBFriendlyName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbDBFriendlyName.Location = new System.Drawing.Point(10, 39);
          this.tbDBFriendlyName.Name = "tbDBFriendlyName";
          this.tbDBFriendlyName.ReadOnly = true;
          this.tbDBFriendlyName.Size = new System.Drawing.Size(213, 20);
          this.tbDBFriendlyName.TabIndex = 0;
          // 
          // splitContainer3
          // 
          this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
          this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
          this.splitContainer3.IsSplitterFixed = true;
          this.splitContainer3.Location = new System.Drawing.Point(0, 0);
          this.splitContainer3.Name = "splitContainer3";
          this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
          // 
          // splitContainer3.Panel1
          // 
          this.splitContainer3.Panel1.Controls.Add(this.dgvItems);
          this.splitContainer3.Panel1.Controls.Add(this.toolStrip1);
          // 
          // splitContainer3.Panel2
          // 
          this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
          this.splitContainer3.Size = new System.Drawing.Size(631, 621);
          this.splitContainer3.SplitterDistance = 410;
          this.splitContainer3.TabIndex = 0;
          // 
          // dgvItems
          // 
          this.dgvItems.AllowUserToAddRows = false;
          this.dgvItems.AllowUserToDeleteRows = false;
          this.dgvItems.BackgroundColor = System.Drawing.SystemColors.Window;
          this.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dgvItems.Dock = System.Windows.Forms.DockStyle.Fill;
          this.dgvItems.Location = new System.Drawing.Point(0, 31);
          this.dgvItems.Name = "dgvItems";
          this.dgvItems.ReadOnly = true;
          this.dgvItems.RowHeadersVisible = false;
          this.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dgvItems.Size = new System.Drawing.Size(631, 379);
          this.dgvItems.TabIndex = 0;
          this.dgvItems.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItems_CellClick);
          this.dgvItems.SelectionChanged += new System.EventHandler(this.dgvItems_SelectionChanged);
          // 
          // toolStrip1
          // 
          this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
          this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripLabel2,
            this.tsbGenerateTemplate,
            this.tsbViewTemplate,
            this.toolStripSeparator4,
            this.tsbViewETLLog,
            this.tsbETLProcess,
            this.toolStripSeparator5,
            this.tsbTaskSchedule});
          this.toolStrip1.Location = new System.Drawing.Point(0, 0);
          this.toolStrip1.Name = "toolStrip1";
          this.toolStrip1.Size = new System.Drawing.Size(631, 31);
          this.toolStrip1.TabIndex = 8;
          this.toolStrip1.Text = "toolStrip1";
          // 
          // toolStripLabel1
          // 
          this.toolStripLabel1.Name = "toolStripLabel1";
          this.toolStripLabel1.Size = new System.Drawing.Size(46, 28);
          this.toolStripLabel1.Text = "  Tasks  ";
          // 
          // toolStripLabel2
          // 
          this.toolStripLabel2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.toolStripLabel2.Name = "toolStripLabel2";
          this.toolStripLabel2.Size = new System.Drawing.Size(10, 28);
          this.toolStripLabel2.Text = " ";
          // 
          // tsbGenerateTemplate
          // 
          this.tsbGenerateTemplate.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.tsbGenerateTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.tsbGenerateTemplate.Image = ((System.Drawing.Image)(resources.GetObject("tsbGenerateTemplate.Image")));
          this.tsbGenerateTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.tsbGenerateTemplate.Name = "tsbGenerateTemplate";
          this.tsbGenerateTemplate.Size = new System.Drawing.Size(28, 28);
          this.tsbGenerateTemplate.Text = "Generate Template";
          this.tsbGenerateTemplate.Click += new System.EventHandler(this.tsbGenerateTemplate_Click);
          // 
          // tsbViewTemplate
          // 
          this.tsbViewTemplate.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.tsbViewTemplate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.tsbViewTemplate.Image = ((System.Drawing.Image)(resources.GetObject("tsbViewTemplate.Image")));
          this.tsbViewTemplate.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.tsbViewTemplate.Name = "tsbViewTemplate";
          this.tsbViewTemplate.Size = new System.Drawing.Size(28, 28);
          this.tsbViewTemplate.Text = "View Template";
          this.tsbViewTemplate.ToolTipText = "View Template";
          this.tsbViewTemplate.Click += new System.EventHandler(this.tsbViewTemplate_Click);
          // 
          // toolStripSeparator4
          // 
          this.toolStripSeparator4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.toolStripSeparator4.Name = "toolStripSeparator4";
          this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
          // 
          // tsbViewETLLog
          // 
          this.tsbViewETLLog.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.tsbViewETLLog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.tsbViewETLLog.Image = ((System.Drawing.Image)(resources.GetObject("tsbViewETLLog.Image")));
          this.tsbViewETLLog.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.tsbViewETLLog.Name = "tsbViewETLLog";
          this.tsbViewETLLog.Size = new System.Drawing.Size(28, 28);
          this.tsbViewETLLog.Text = "View ETL Logs";
          this.tsbViewETLLog.Click += new System.EventHandler(this.tsbViewETLLog_Click);
          // 
          // tsbETLProcess
          // 
          this.tsbETLProcess.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.tsbETLProcess.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.tsbETLProcess.Image = ((System.Drawing.Image)(resources.GetObject("tsbETLProcess.Image")));
          this.tsbETLProcess.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.tsbETLProcess.Name = "tsbETLProcess";
          this.tsbETLProcess.Size = new System.Drawing.Size(28, 28);
          this.tsbETLProcess.Text = "ETL Process Now";
          this.tsbETLProcess.Click += new System.EventHandler(this.tsbETLProcess_Click);
          // 
          // toolStripSeparator5
          // 
          this.toolStripSeparator5.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.toolStripSeparator5.Name = "toolStripSeparator5";
          this.toolStripSeparator5.Size = new System.Drawing.Size(6, 31);
          // 
          // tsbTaskSchedule
          // 
          this.tsbTaskSchedule.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.tsbTaskSchedule.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
          this.tsbTaskSchedule.Image = ((System.Drawing.Image)(resources.GetObject("tsbTaskSchedule.Image")));
          this.tsbTaskSchedule.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.tsbTaskSchedule.Name = "tsbTaskSchedule";
          this.tsbTaskSchedule.Size = new System.Drawing.Size(28, 28);
          this.tsbTaskSchedule.Text = "Task Schedule";
          this.tsbTaskSchedule.Click += new System.EventHandler(this.tsbTaskSchedule_Click);
          // 
          // splitContainer4
          // 
          this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
          this.splitContainer4.Location = new System.Drawing.Point(0, 0);
          this.splitContainer4.Name = "splitContainer4";
          // 
          // splitContainer4.Panel1
          // 
          this.splitContainer4.Panel1.Controls.Add(this.panel1);
          // 
          // splitContainer4.Panel2
          // 
          this.splitContainer4.Panel2.Controls.Add(this.pnlTaskDetails);
          this.splitContainer4.Size = new System.Drawing.Size(631, 207);
          this.splitContainer4.SplitterDistance = 270;
          this.splitContainer4.TabIndex = 0;
          // 
          // panel1
          // 
          this.panel1.AutoScroll = true;
          this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.panel1.Controls.Add(this.tbLastName);
          this.panel1.Controls.Add(this.label8);
          this.panel1.Controls.Add(this.label9);
          this.panel1.Controls.Add(this.label13);
          this.panel1.Controls.Add(this.tbEmail);
          this.panel1.Controls.Add(this.label14);
          this.panel1.Controls.Add(this.tbPhone);
          this.panel1.Controls.Add(this.label15);
          this.panel1.Controls.Add(this.tbFirstName);
          this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.panel1.Location = new System.Drawing.Point(0, 0);
          this.panel1.Name = "panel1";
          this.panel1.Size = new System.Drawing.Size(270, 207);
          this.panel1.TabIndex = 2;
          // 
          // tbLastName
          // 
          this.tbLastName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbLastName.Location = new System.Drawing.Point(11, 82);
          this.tbLastName.Name = "tbLastName";
          this.tbLastName.ReadOnly = true;
          this.tbLastName.Size = new System.Drawing.Size(243, 20);
          this.tbLastName.TabIndex = 9;
          // 
          // label8
          // 
          this.label8.AutoSize = true;
          this.label8.Location = new System.Drawing.Point(10, 64);
          this.label8.Name = "label8";
          this.label8.Size = new System.Drawing.Size(58, 13);
          this.label8.TabIndex = 8;
          this.label8.Text = "Last Name";
          // 
          // label9
          // 
          this.label9.BackColor = System.Drawing.Color.Silver;
          this.label9.Dock = System.Windows.Forms.DockStyle.Top;
          this.label9.Location = new System.Drawing.Point(0, 0);
          this.label9.Name = "label9";
          this.label9.Size = new System.Drawing.Size(268, 18);
          this.label9.TabIndex = 6;
          this.label9.Text = "User Details";
          // 
          // label13
          // 
          this.label13.AutoSize = true;
          this.label13.Location = new System.Drawing.Point(11, 113);
          this.label13.Name = "label13";
          this.label13.Size = new System.Drawing.Size(32, 13);
          this.label13.TabIndex = 5;
          this.label13.Text = "Email";
          // 
          // tbEmail
          // 
          this.tbEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbEmail.Location = new System.Drawing.Point(11, 130);
          this.tbEmail.Name = "tbEmail";
          this.tbEmail.ReadOnly = true;
          this.tbEmail.Size = new System.Drawing.Size(243, 20);
          this.tbEmail.TabIndex = 4;
          // 
          // label14
          // 
          this.label14.AutoSize = true;
          this.label14.Location = new System.Drawing.Point(12, 160);
          this.label14.Name = "label14";
          this.label14.Size = new System.Drawing.Size(38, 13);
          this.label14.TabIndex = 3;
          this.label14.Text = "Phone";
          // 
          // tbPhone
          // 
          this.tbPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbPhone.Location = new System.Drawing.Point(11, 176);
          this.tbPhone.Name = "tbPhone";
          this.tbPhone.ReadOnly = true;
          this.tbPhone.Size = new System.Drawing.Size(243, 20);
          this.tbPhone.TabIndex = 2;
          // 
          // label15
          // 
          this.label15.AutoSize = true;
          this.label15.Location = new System.Drawing.Point(10, 23);
          this.label15.Name = "label15";
          this.label15.Size = new System.Drawing.Size(57, 13);
          this.label15.TabIndex = 1;
          this.label15.Text = "First Name";
          // 
          // tbFirstName
          // 
          this.tbFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbFirstName.Location = new System.Drawing.Point(11, 39);
          this.tbFirstName.Name = "tbFirstName";
          this.tbFirstName.ReadOnly = true;
          this.tbFirstName.Size = new System.Drawing.Size(243, 20);
          this.tbFirstName.TabIndex = 0;
          // 
          // pnlTaskDetails
          // 
          this.pnlTaskDetails.AutoScroll = true;
          this.pnlTaskDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
          this.pnlTaskDetails.Controls.Add(this.label16);
          this.pnlTaskDetails.Controls.Add(this.tbTargetColNo);
          this.pnlTaskDetails.Controls.Add(this.label6);
          this.pnlTaskDetails.Controls.Add(this.label12);
          this.pnlTaskDetails.Controls.Add(this.tbNoOfTemplColumns);
          this.pnlTaskDetails.Controls.Add(this.label3);
          this.pnlTaskDetails.Controls.Add(this.label5);
          this.pnlTaskDetails.Controls.Add(this.tbTaskDescription);
          this.pnlTaskDetails.Controls.Add(this.tbTargetTableName);
          this.pnlTaskDetails.Controls.Add(this.tbProcessMode);
          this.pnlTaskDetails.Controls.Add(this.label4);
          this.pnlTaskDetails.Dock = System.Windows.Forms.DockStyle.Fill;
          this.pnlTaskDetails.Location = new System.Drawing.Point(0, 0);
          this.pnlTaskDetails.Name = "pnlTaskDetails";
          this.pnlTaskDetails.Size = new System.Drawing.Size(357, 207);
          this.pnlTaskDetails.TabIndex = 0;
          // 
          // label16
          // 
          this.label16.AutoSize = true;
          this.label16.Location = new System.Drawing.Point(12, 150);
          this.label16.Name = "label16";
          this.label16.Size = new System.Drawing.Size(113, 13);
          this.label16.TabIndex = 11;
          this.label16.Text = "No of Target Columns:";
          // 
          // tbTargetColNo
          // 
          this.tbTargetColNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbTargetColNo.Location = new System.Drawing.Point(199, 146);
          this.tbTargetColNo.Name = "tbTargetColNo";
          this.tbTargetColNo.ReadOnly = true;
          this.tbTargetColNo.Size = new System.Drawing.Size(146, 20);
          this.tbTargetColNo.TabIndex = 10;
          // 
          // label6
          // 
          this.label6.AutoSize = true;
          this.label6.Location = new System.Drawing.Point(12, 179);
          this.label6.Name = "label6";
          this.label6.Size = new System.Drawing.Size(137, 13);
          this.label6.TabIndex = 9;
          this.label6.Text = "No of Columns in Template:";
          // 
          // label12
          // 
          this.label12.BackColor = System.Drawing.Color.Silver;
          this.label12.Dock = System.Windows.Forms.DockStyle.Top;
          this.label12.Location = new System.Drawing.Point(0, 0);
          this.label12.Name = "label12";
          this.label12.Size = new System.Drawing.Size(355, 18);
          this.label12.TabIndex = 7;
          this.label12.Text = "Task Details";
          // 
          // tbNoOfTemplColumns
          // 
          this.tbNoOfTemplColumns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbNoOfTemplColumns.Location = new System.Drawing.Point(199, 175);
          this.tbNoOfTemplColumns.Name = "tbNoOfTemplColumns";
          this.tbNoOfTemplColumns.ReadOnly = true;
          this.tbNoOfTemplColumns.Size = new System.Drawing.Size(146, 20);
          this.tbNoOfTemplColumns.TabIndex = 8;
          // 
          // label3
          // 
          this.label3.AutoSize = true;
          this.label3.Location = new System.Drawing.Point(9, 23);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(87, 13);
          this.label3.TabIndex = 3;
          this.label3.Text = "Task Description";
          // 
          // label5
          // 
          this.label5.AutoSize = true;
          this.label5.Location = new System.Drawing.Point(12, 121);
          this.label5.Name = "label5";
          this.label5.Size = new System.Drawing.Size(71, 13);
          this.label5.TabIndex = 7;
          this.label5.Text = "Target Table:";
          // 
          // tbTaskDescription
          // 
          this.tbTaskDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbTaskDescription.Location = new System.Drawing.Point(9, 39);
          this.tbTaskDescription.Multiline = true;
          this.tbTaskDescription.Name = "tbTaskDescription";
          this.tbTaskDescription.ReadOnly = true;
          this.tbTaskDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.tbTaskDescription.Size = new System.Drawing.Size(336, 40);
          this.tbTaskDescription.TabIndex = 2;
          // 
          // tbTargetTableName
          // 
          this.tbTargetTableName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbTargetTableName.Location = new System.Drawing.Point(145, 117);
          this.tbTargetTableName.Name = "tbTargetTableName";
          this.tbTargetTableName.ReadOnly = true;
          this.tbTargetTableName.Size = new System.Drawing.Size(200, 20);
          this.tbTargetTableName.TabIndex = 6;
          // 
          // tbProcessMode
          // 
          this.tbProcessMode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
          this.tbProcessMode.Location = new System.Drawing.Point(145, 88);
          this.tbProcessMode.Name = "tbProcessMode";
          this.tbProcessMode.ReadOnly = true;
          this.tbProcessMode.Size = new System.Drawing.Size(200, 20);
          this.tbProcessMode.TabIndex = 4;
          // 
          // label4
          // 
          this.label4.AutoSize = true;
          this.label4.Location = new System.Drawing.Point(12, 92);
          this.label4.Name = "label4";
          this.label4.Size = new System.Drawing.Size(78, 13);
          this.label4.TabIndex = 5;
          this.label4.Text = "Process Mode:";
          // 
          // statusStrip1
          // 
          this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripProgressBar1});
          this.statusStrip1.Location = new System.Drawing.Point(0, 645);
          this.statusStrip1.Name = "statusStrip1";
          this.statusStrip1.Size = new System.Drawing.Size(875, 22);
          this.statusStrip1.TabIndex = 3;
          this.statusStrip1.Text = "statusStrip1";
          // 
          // toolStripStatusLabel1
          // 
          this.toolStripStatusLabel1.AutoSize = false;
          this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
          this.toolStripStatusLabel1.Size = new System.Drawing.Size(609, 17);
          this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
          // 
          // toolStripProgressBar1
          // 
          this.toolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
          this.toolStripProgressBar1.Name = "toolStripProgressBar1";
          this.toolStripProgressBar1.Size = new System.Drawing.Size(200, 16);
          this.toolStripProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
          this.toolStripProgressBar1.Visible = false;
          // 
          // backgroundWorker
          // 
          this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
          this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
          // 
          // MainForm
          // 
          this.ClientSize = new System.Drawing.Size(875, 667);
          this.Controls.Add(this.splitContainer1);
          this.Controls.Add(this.menuStrip1);
          this.Controls.Add(this.statusStrip1);
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.Name = "MainForm";
          this.Text = "XLETL Task Manager";
          this.Load += new System.EventHandler(this.MainForm_Load);
          this.menuStrip1.ResumeLayout(false);
          this.menuStrip1.PerformLayout();
          this.splitContainer1.Panel1.ResumeLayout(false);
          this.splitContainer1.Panel2.ResumeLayout(false);
          this.splitContainer1.ResumeLayout(false);
          this.splitContainer2.Panel1.ResumeLayout(false);
          this.splitContainer2.Panel1.PerformLayout();
          this.splitContainer2.Panel2.ResumeLayout(false);
          this.splitContainer2.ResumeLayout(false);
          this.tsDbs.ResumeLayout(false);
          this.tsDbs.PerformLayout();
          this.pnlCompanyDetails.ResumeLayout(false);
          this.pnlCompanyDetails.PerformLayout();
          this.splitContainer3.Panel1.ResumeLayout(false);
          this.splitContainer3.Panel1.PerformLayout();
          this.splitContainer3.Panel2.ResumeLayout(false);
          this.splitContainer3.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.dgvItems)).EndInit();
          this.toolStrip1.ResumeLayout(false);
          this.toolStrip1.PerformLayout();
          this.splitContainer4.Panel1.ResumeLayout(false);
          this.splitContainer4.Panel2.ResumeLayout(false);
          this.splitContainer4.ResumeLayout(false);
          this.panel1.ResumeLayout(false);
          this.panel1.PerformLayout();
          this.pnlTaskDetails.ResumeLayout(false);
          this.pnlTaskDetails.PerformLayout();
          this.statusStrip1.ResumeLayout(false);
          this.statusStrip1.PerformLayout();
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView tvDBs;
        private System.Windows.Forms.DataGridView dgvItems;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbNoOfTemplColumns;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbTargetTableName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbProcessMode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbTaskDescription;
        private System.Windows.Forms.Panel pnlTaskDetails;
        private System.Windows.Forms.ToolStripMenuItem createDBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editDBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteDBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createTaskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editTaskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteTaskToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.ToolStripMenuItem transformationPluginsToolStripMenuItem;
        private System.Windows.Forms.Panel pnlCompanyDetails;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbDBDesc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbDatabaseProvider;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDatabase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDBFriendlyName;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStrip tsDbs;
        private System.Windows.Forms.ToolStripButton toolStripAddDB;
        private System.Windows.Forms.ToolStripButton toolStripEditDB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripAddUser;
        private System.Windows.Forms.ToolStripButton toolStripEditUser;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripAddTask;
        private System.Windows.Forms.ToolStripButton toolStripEditTask;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton tsbGenerateTemplate;
        private System.Windows.Forms.ToolStripButton tsbViewTemplate;
        private System.Windows.Forms.ToolStripButton tsbETLProcess;
        private System.Windows.Forms.ToolStripButton tsbTaskSchedule;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbPhone;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbTargetColNo;
        private System.Windows.Forms.ToolStripButton tsbViewETLLog;
    }
}
