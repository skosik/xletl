using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using CodeProject.Dialog;
using XLETL.Model;
using XLETL.BLL;
using XLETL.Common;
using XLETL.ExceptionHandling;
using XLETL.TaskManager.Properties;
using System.Diagnostics;
//using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace XLETL.TaskManager.Forms
{
  public partial class MainForm : Form
  {
    enum TreeNodeType { Root, Database, Task, User };
    List<DBInfo> currentDBList = null; // hold the database tree
    int selectedDbIndex = -1; // holds the selected database index
    List<TaskInfo> selectedTasks = null;
    //List<UserInfo> selectedUsers = null;
    //List<ScheduleInfo> scheduleList = null;        
    TreeNodeType selectedTreeNode = TreeNodeType.Database;
    ImageList tvImageList = new ImageList();
    bool isDgvReady = false; // hold flag when datagridview is built

    public DBInfo SelectedDBInfo { get; set; }

    public MainForm()
    {
      InitializeComponent();

      Image img;
      this.tvImageList.ImageSize = new Size(16, 16);
      this.tvImageList.TransparentColor = System.Drawing.Color.Transparent;
      img = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("XLETL.TaskManager.Images.companies.png"));
      this.tvImageList.Images.Add(img);
      img = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("XLETL.TaskManager.Images.company.png"));
      this.tvImageList.Images.Add(img);
      img = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("XLETL.TaskManager.Images.users.png"));
      this.tvImageList.Images.Add(img);
      img = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("XLETL.TaskManager.Images.user.png"));
      this.tvImageList.Images.Add(img);
      img = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("XLETL.TaskManager.Images.tasks.png"));
      this.tvImageList.Images.Add(img);
      img = new Bitmap(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("XLETL.TaskManager.Images.task.png"));
      this.tvImageList.Images.Add(img);

      this.tvDBs.ImageList = this.tvImageList;

      // Make sure common storage directory exist 
      if (!Directory.Exists(GlobalCache.COMMON_STORAGE_FOLDER))
      {
        Directory.CreateDirectory(GlobalCache.COMMON_STORAGE_FOLDER);
      }

      PopulateDBList();
    }

    private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      MsgBox.Show("XLETL Version 1.0.0.0 \r\n\r\n Sergey Kosik\r\n\r\n Final Project, DIT, 2008-2009", "About XLETL", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }



    /// <summary>
    /// Populate Database TreeView control with Database objects based on the collection
    /// stored in data storage.
    /// </summary>
    /// <param name="companyCol"></param>
    private void PopulateDBList()
    {
      try
      {
        DB c = new DB();
        currentDBList = c.GetDeepDBList();
        //currentCompanyList = c.GetCompanies();
        BuildDBTreeView(currentDBList);
      }
      catch (Exception ex)
      {
        //if (ExceptionHandleProvider.HandleException(ex, "Global Policy") == true)
        //{
        //    throw ex;
        //}
        //ExceptionProcessor.ProcessVLException(ex);
        throw new ApplicationException("Error occured", ex);
      }
    }

    private void BuildDBTreeView(List<DBInfo> companyList)
    {
      // Don�t repaint the TreeView until all the nodes have been created.
      tvDBs.BeginUpdate();

      // Clear the TreeView each time the method is called.
      tvDBs.Nodes.Clear();

      TreeNode dbs = new TreeNode("Databases");
      dbs.ImageIndex = 0;
      dbs.SelectedImageIndex = 0;
      tvDBs.Nodes.Add(dbs);

      TreeNode dbNode = null;
      TreeNode childNode = null;
      //int colIndex = 0;
      // Add a root TreeNode for each Database object in the List<>.
      foreach (DBInfo ci in companyList)
      {
        dbNode = new TreeNode();
        dbNode.Text = ci.DBFriendlyName;
        dbNode.Tag = companyList.IndexOf(ci);
        dbNode.Name = string.Concat("DB_", ci.DBId);
        dbNode.ImageIndex = 1;
        dbNode.SelectedImageIndex = 1;


        // Now append User nodes
        // Create user's parent
        TreeNode users = new TreeNode("Users");
        users.ImageIndex = 2;
        users.SelectedImageIndex = 2;
        dbNode.Nodes.Add(users);

        if (ci.UserInfoList != null)
        {
          // Now add user nodes
          foreach (UserInfo userObj in ci.UserInfoList)
          {
            childNode = new TreeNode();
            childNode.Text = userObj.UserName;
            childNode.Tag = ci.UserInfoList.IndexOf(userObj);
            childNode.Name = string.Concat("User_", userObj.Id);
            childNode.ImageIndex = 3;
            childNode.SelectedImageIndex = 3;
            dbNode.Nodes[0].Nodes.Add(childNode);
          }
        }

        // Now append Task nodes
        TreeNode tasks = new TreeNode("Tasks");
        tasks.ImageIndex = 4;
        tasks.SelectedImageIndex = 4;
        dbNode.Nodes.Add(tasks);

        if (ci.TaskInfoList != null)
        {
          foreach (TaskInfo taskObj in ci.TaskInfoList)
          {
            childNode = new TreeNode();
            childNode.Text = taskObj.TaskDetails.Name;
            childNode.Tag = ci.TaskInfoList.IndexOf(taskObj);
            childNode.ToolTipText = taskObj.TaskDetails.Description;
            childNode.Name = string.Concat("Task_", taskObj.ID);
            childNode.ImageIndex = 5;
            childNode.SelectedImageIndex = 5;
            dbNode.Nodes[1].Nodes.Add(childNode);
          }
        }

        tvDBs.Nodes[0].Nodes.Add(dbNode);

      }

      tvDBs.Nodes[0].ExpandAll();

      // Now paint the TreeView.
      tvDBs.EndUpdate();
    }


    private void tvDBs_AfterSelect(object sender, TreeViewEventArgs e)
    {
      switch (e.Node.Level)
      {
        case 0:
          selectedTreeNode = TreeNodeType.Root;
          List<TaskInfo> allTasksList = null;
          if (currentDBList != null)
          {
            allTasksList = new List<TaskInfo>();
            object lockObj = new object();

            for (int d = 0; d < currentDBList.Count; d++)
            {
              if (currentDBList[d].TaskInfoList != null)
              {
                for (int i = 0; i < currentDBList[d].TaskInfoList.Count; i++)
                {
                  lock (lockObj)
                  {
                    allTasksList.Add(currentDBList[d].TaskInfoList[i]);
                  }
                }
              }
            }
            selectedTasks = allTasksList;
            DisplayTaskList(selectedTasks, true);
          }
          break;
        case 1:
          selectedTreeNode = TreeNodeType.Database;
          selectedDbIndex = Convert.ToInt32(e.Node.Tag);
          SelectedDBInfo = currentDBList[selectedDbIndex];
          DisplayDBDetails(SelectedDBInfo);
          selectedTasks = (List<TaskInfo>)currentDBList[selectedDbIndex].TaskInfoList;
          DisplayTaskList(selectedTasks, false);
          break;
        case 2:
          selectedDbIndex = Convert.ToInt32(e.Node.Parent.Tag);
          SelectedDBInfo = currentDBList[selectedDbIndex];
          DisplayDBDetails(SelectedDBInfo);
          selectedTasks = (List<TaskInfo>)currentDBList[selectedDbIndex].TaskInfoList;
          DisplayTaskList(selectedTasks, false);
          switch (e.Node.Text)
          {
            case "Users":
              //selectedUsers = (List<UserInfo>)currentDBList[selectedDbIndex].UserInfoList;
              //DisplayUserList(selectedUsers);
              break;
            case "Tasks":
              //object taskId = e.Node.Tag;
              //selectedTasks = (List<TaskInfo>)currentDBList[selectedDbIndex].TaskInfoList;
              //DisplayTaskList(selectedTasks);
              break;
          }
          break;

        case 3:
          selectedDbIndex = Convert.ToInt32(e.Node.Parent.Parent.Tag);
          SelectedDBInfo = currentDBList[selectedDbIndex];
          DisplayDBDetails(SelectedDBInfo);
          selectedTasks = (List<TaskInfo>)currentDBList[selectedDbIndex].TaskInfoList;
          DisplayTaskList(selectedTasks, false);
          switch (e.Node.Parent.Text)
          {
            case "Users":
              selectedTreeNode = TreeNodeType.User;
              //selectedUsers = (List<UserInfo>)currentDBList[selectedDbIndex].UserInfoList[Convert.ToInt32(e.Node.Tag)];
              UserInfo selectedUi = currentDBList[selectedDbIndex].UserInfoList[Convert.ToInt32(e.Node.Tag)];
              DisplayUserDetails(selectedUi);
              /*
              UserInfo selectedUi = currentCompanyList[Convert.ToInt32(e.Node.Parent.Parent.Tag)].UserInfoList[Convert.ToInt32(e.Node.Tag)];
              if (selectedUi != null)
              {
                  tbTaskDescription.Text = selectedUi.FirstName;
                  tbLastName.Text = selectedUi.LastName;
                  tbPhone.Text = selectedUi.Phone;
                  tbEmail.Text = selectedUi.Email;
              }*/
              break;
            case "Tasks":
              selectedTreeNode = TreeNodeType.Task;
              TaskInfo selectedTask = currentDBList[selectedDbIndex].TaskInfoList[Convert.ToInt32(e.Node.Tag)];
              DisplayTaskDetails(selectedTask);
              //selectedTasks = (List<TaskInfo>)currentDBList[selectedDbIndex].TaskInfoList;
              //DisplayTaskList(selectedTasks);                           
              break;
          }
          break;
      }


      //// Show info and highlight node.
      //lblNodeInfo.Text = nodeInfo;
      //e.Node.BackColor = Color.AliceBlue;
    }

    private void DisplayDBDetails(DBInfo selectedDBInfo)
    {
      tbDatabase.Text = selectedDBInfo.DatabaseName;
      tbDBFriendlyName.Text = selectedDBInfo.DBFriendlyName;
      tbDatabaseProvider.Text = selectedDBInfo.DALClassName;
      tbDBDesc.Text = selectedDBInfo.DBDesc;
    }

    private void DisplayTaskList(List<TaskInfo> taskList, bool isAll)
    {
      isDgvReady = false;

      // Set title for the datagridview
      //lblListTitle.Text = "Tasks";

      dgvItems.SuspendLayout();

      dgvItems.Columns.Clear();
      dgvItems.Rows.Clear();

      DataGridViewTextBoxColumn taskNameColumn = new DataGridViewTextBoxColumn();
      taskNameColumn.HeaderText = "Task Name";
      taskNameColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

      DataGridViewTextBoxColumn targetTableColumn = new DataGridViewTextBoxColumn();
      targetTableColumn.HeaderText = "Target Table";
      targetTableColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

      DataGridViewTextBoxColumn processModeColumn = new DataGridViewTextBoxColumn();
      processModeColumn.HeaderText = "Process Mode";
      processModeColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

      DataGridViewTextBoxColumn taskStatusColumn = new DataGridViewTextBoxColumn();
      taskStatusColumn.HeaderText = "Task Status";
      taskStatusColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

      DataGridViewTextBoxColumn taskProcessTimeColumn = new DataGridViewTextBoxColumn();
      taskProcessTimeColumn.HeaderText = "Process Time";
      taskProcessTimeColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

      DataGridViewTextBoxColumn taskDBColumn = new DataGridViewTextBoxColumn();
      taskDBColumn.HeaderText = "Database";
      taskDBColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

      /*
      DataGridViewButtonColumn taskViewTemplateColumn = new DataGridViewButtonColumn();
      taskViewTemplateColumn.HeaderText = "";
      taskViewTemplateColumn.Text = "View Template";
      taskViewTemplateColumn.Name = "TemplateView";
      taskViewTemplateColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      taskViewTemplateColumn.UseColumnTextForButtonValue = true;

      DataGridViewButtonColumn taskGenerateTemplateColumn = new DataGridViewButtonColumn();
      taskGenerateTemplateColumn.HeaderText = "";
      taskGenerateTemplateColumn.Text = "Generate Template";
      taskGenerateTemplateColumn.Name = "TemplateGenerate";
      taskGenerateTemplateColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      taskGenerateTemplateColumn.UseColumnTextForButtonValue = true;

      DataGridViewButtonColumn taskProcessColumn = new DataGridViewButtonColumn();
      taskProcessColumn.HeaderText = "";
      taskProcessColumn.Text = "Process Now";
      taskProcessColumn.Name = "ProcessNow";
      taskProcessColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      taskProcessColumn.UseColumnTextForButtonValue = true;

      DataGridViewButtonColumn taskScheduleColumn = new DataGridViewButtonColumn();
      taskScheduleColumn.HeaderText = "";
      taskScheduleColumn.Text = "Schedule";
      taskScheduleColumn.Name = "Schedule";
      taskScheduleColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      taskScheduleColumn.UseColumnTextForButtonValue = true;
      */
      dgvItems.Columns.AddRange(new DataGridViewColumn[] { taskNameColumn, targetTableColumn, processModeColumn, taskStatusColumn, taskProcessTimeColumn, taskDBColumn });

      DataGridViewRow dgvRow = null;
      DataGridViewTextBoxCell taskNameCell = null;
      DataGridViewTextBoxCell targetTableCell = null;
      DataGridViewTextBoxCell processModeCell = null;
      DataGridViewTextBoxCell taskStatusCell = null;
      DataGridViewTextBoxCell taskProcessTimeCell = null;
      DataGridViewTextBoxCell taskDBCell = null;

      foreach (TaskInfo task in taskList)
      {
        taskNameCell = new DataGridViewTextBoxCell();
        taskNameCell.Value = task.TaskDetails.Name;
        targetTableCell = new DataGridViewTextBoxCell();
        targetTableCell.Value = task.TargetTableName;
        processModeCell = new DataGridViewTextBoxCell();
        processModeCell.Value = Common.Utilities.GetDescriptionAttrFromEnum(typeof(Globals.LoadMode), task.TaskDetails.ProcessMode);
        taskProcessTimeCell = new DataGridViewTextBoxCell();
        taskProcessTimeCell.Value = (task.TaskSchedule == null) ? "Not Set" : task.TaskSchedule.ToString();
        taskStatusCell = new DataGridViewTextBoxCell();
        taskStatusCell.Value = Enum.GetName(typeof(Globals.StatusType), task.TaskStatusType);
        taskDBCell = new DataGridViewTextBoxCell();

        if (isAll)
        {
          DBInfo found = currentDBList.Find(delegate(DBInfo search) { return search.DBId.Equals(task.DatabaseID); });
          if (found != null)
            taskDBCell.Value = found.DBFriendlyName;
        }
        else
        {
          taskDBCell.Value = SelectedDBInfo.DBFriendlyName;
        }

        dgvRow = new DataGridViewRow();
        dgvRow.Cells.AddRange(new DataGridViewCell[] { taskNameCell, targetTableCell, processModeCell, taskStatusCell, taskProcessTimeCell, taskDBCell });

        // Add ready row into datagridview
        dgvItems.Rows.Add(dgvRow);
      }

      dgvItems.ClearSelection();
      dgvItems.ResumeLayout();
      isDgvReady = true;
    }

    /*
      private void DisplayUserList(List<UserInfo> userList)
      {
          isDgvReady = false;

          // Set title for the datagridview
          //lblListTitle.Text = "Users";

          dgvItems.SuspendLayout();

          dgvItems.Columns.Clear();
          dgvItems.Rows.Clear();

          DataGridViewTextBoxColumn userFirstNameColumn = new DataGridViewTextBoxColumn();
          userFirstNameColumn.HeaderText = "First Name";
          userFirstNameColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          DataGridViewTextBoxColumn userLastNameColumn = new DataGridViewTextBoxColumn();
          userLastNameColumn.HeaderText = "Last Name";
          userLastNameColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          DataGridViewTextBoxColumn userPhoneColumn = new DataGridViewTextBoxColumn();
          userPhoneColumn.HeaderText = "Phone";
          userPhoneColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          DataGridViewTextBoxColumn userEmailColumn = new DataGridViewTextBoxColumn();
          userEmailColumn.HeaderText = "Email";
          userEmailColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

          dgvItems.Columns.AddRange(new DataGridViewColumn[] { userFirstNameColumn, userLastNameColumn, userPhoneColumn, userEmailColumn });

          DataGridViewRow dgvRow = null;
          DataGridViewTextBoxCell userFirstNameCell = null;
          DataGridViewTextBoxCell userLastNameCell = null;
          DataGridViewTextBoxCell userPhoneCell = null;
          DataGridViewTextBoxCell userEmailCell = null;

          foreach (UserInfo user in userList)
          {
              userFirstNameCell = new DataGridViewTextBoxCell();
              userFirstNameCell.Value = user.FirstName;
              userLastNameCell = new DataGridViewTextBoxCell();
              userLastNameCell.Value = user.LastName;
              userPhoneCell = new DataGridViewTextBoxCell();
              userPhoneCell.Value = user.Phone;
              userEmailCell = new DataGridViewTextBoxCell();
              userEmailCell.Value = user.Email;

              dgvRow = new DataGridViewRow();
              dgvRow.Cells.AddRange(new DataGridViewCell[] { userFirstNameCell, userLastNameCell, userPhoneCell, userEmailCell });

              // Add ready row into datagridview
              dgvItems.Rows.Add(dgvRow);
          }

          dgvItems.ClearSelection();

          dgvItems.ResumeLayout();

          isDgvReady = true;
      }*/

    private void DisplayUserDetails(UserInfo selectedUserInfo)
    {
      if (selectedUserInfo != null)
      {
        tbFirstName.Text = selectedUserInfo.FirstName;
        tbLastName.Text = selectedUserInfo.LastName;
        tbPhone.Text = selectedUserInfo.Phone;
        tbEmail.Text = selectedUserInfo.Email;
      }
      else
      {
        tbFirstName.Text = "";
        tbLastName.Text = "";
        tbPhone.Text = "";
        tbEmail.Text = "";
      }
    }

    private void dgvItems_CellClick(object sender, DataGridViewCellEventArgs e)
    {
      /*
      // Ignore clicks that are not on button cells. 
      if (e.RowIndex < 0 || e.ColumnIndex < 4)
        return;

      //MessageBox.Show(string.Format("View Template Button Clicked (Row: {0}).", e.RowIndex));

      if (e.ColumnIndex == dgvItems.Columns["TemplateView"].Index)
      {
        if (File.Exists(selectedTasks[e.RowIndex].TemplatePath))
        {
          // show template
          this.ViewTemplate(selectedTasks[e.RowIndex].TemplatePath);
        }
      }
      else if (e.ColumnIndex == dgvItems.Columns["TemplateGenerate"].Index)
      {
        // Start Generation Process
        // Set template generator UI
        this.toolStripProgressBar1.Visible = true;
        this.toolStripStatusLabel1.Text = "Template Generating...";
        // Begin template generating asynchronously
        this.backgroundWorker.RunWorkerAsync(e.RowIndex);

        //XLETL.TemplateGenerator.TemplateGenerator tmplGen = new XLETL.TemplateGenerator.TemplateGenerator();
        //tmplGen.GenerateTemplate(selectedTasks[e.RowIndex], SelectedCompanyInfo);
      }
      else if (e.ColumnIndex == dgvItems.Columns["ProcessNow"].Index)
      {
        ProcessTaskDialog ptd = new ProcessTaskDialog(selectedTasks[e.RowIndex], SelectedDBInfo);
        DlgBox.ShowDialog(ptd);
        ptd.Dispose();
        ptd = null;
      }
      else if (e.ColumnIndex == dgvItems.Columns["Schedule"].Index)
      {
        TaskScheduleDialog tsd = new TaskScheduleDialog(selectedTasks[e.RowIndex]);
        if (DlgBox.ShowDialog(tsd) != DialogResult.Cancel)
        {
          selectedTasks[e.RowIndex].TaskSchedule = tsd.TaskSchedule;

          // Now refresh task list
          DisplayTaskList(selectedTasks);
        }
        tsd.Dispose();
        tsd = null;
      }
      */



    }

    private void dgvItems_SelectionChanged(object sender, EventArgs e)
    {
      if (isDgvReady)
      {
        int rowIndex = dgvItems.SelectedCells[0].RowIndex;
        if (rowIndex > -1)
        {
          DisplayTaskDetails(this.selectedTasks[rowIndex]);
        }
        else
        {
          DisplayTaskDetails(null);
        }
      }
    }

    private void DisplayTaskDetails(TaskInfo selectedTask)
    {
      if (selectedTask != null)
      {
        this.tbTaskDescription.Text = selectedTask.TaskDetails.Description;
        this.tbProcessMode.Text = Common.Utilities.GetDescriptionAttrFromEnum(typeof(Globals.LoadMode), selectedTask.TaskDetails.ProcessMode);
        this.tbTargetTableName.Text = selectedTask.TargetTableName;
        this.tbNoOfTemplColumns.Text = selectedTask.SourceColumns.Count.ToString();
        this.tbTargetColNo.Text = selectedTask.TargetColumns.Count.ToString();
      }
      else
      {
        this.tbTaskDescription.Text = "";
        this.tbProcessMode.Text = "";
        this.tbTargetTableName.Text = "";
        this.tbNoOfTemplColumns.Text = "";
        this.tbTargetColNo.Text = "";
      }
    }

    #region Menu Click Events
    /// <summary>
    /// Create Database menu click handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void createDBToolStripMenuItem_Click(object sender, EventArgs e)
    {
      // Instantiate DBDetailsDialog object
      DBDetailsDialog createDBDialog = new DBDetailsDialog();

      // Once User click OK button retrieve newly entered info
      if (DlgBox.ShowDialog(createDBDialog) == DialogResult.OK)
      {
        try
        {
          // Refresh database list tree only if company was saved sucessfuly
          if (createDBDialog.IsSaveSuccess)
          {
            PopulateDBList();
          }
        }
        catch (Exception ex)
        {
          //if (ExceptionHandleProvider.HandleException(ex, "Global Policy") == true)
          //{
          //    throw ex;
          //}
          throw new ApplicationException("Error occured", ex);
        }
      }

      // Now dispose the dialog object
      createDBDialog.Dispose();
      createDBDialog = null;
    }

    /// <summary>
    /// Create User menu click handler
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void createUserToolStripMenuItem_Click(object sender, EventArgs e)
    {
      // make sure any company is selected
      if (tvDBs.SelectedNode.Level > 0)
      {
        // Instantiate UserDetailsDialog object for create user mode
        UserDetailsDialog createUserDialog = new UserDetailsDialog(SelectedDBInfo.DBId, SelectedDBInfo.DBFriendlyName);

        // Display the dialog and once User click OK button retrieve the entered info
        if (DlgBox.ShowDialog(createUserDialog) == DialogResult.OK)
        {
          // Refresh company list tree only if user was saved sucessfuly
          if (createUserDialog.IsSaveSuccess)
          {
            PopulateDBList();
          }
        }

        // Now dispose the dialog object
        createUserDialog.Dispose();
        createUserDialog = null;
      }
      else
      {
        MsgBox.Show(Resources.MSG_SELECT_DB, Resources.HD_SELECT_DB, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void editDBToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (tvDBs.SelectedNode.Level > 0)
      {
        // make sure dbId exist
        if (!SelectedDBInfo.DBId.Equals(Guid.Empty))
        {
          DBDetailsDialog editDBDlg = new DBDetailsDialog(SelectedDBInfo);

          // Display the dialog and once User click OK button retrieve the newly entered info
          if (DlgBox.ShowDialog(editDBDlg) == DialogResult.OK)
          {
            try
            {
              // Refresh database list tree only if database was saved sucessfuly
              if (editDBDlg.IsSaveSuccess)
              {
                PopulateDBList();
              }
            }
            catch (Exception ex)
            {
              //if (ExceptionHandleProvider.HandleException(ex, "Global Policy") == true)
              //{
              //    throw ex;
              //}
              throw new ApplicationException("Error occured", ex);
            }
          }
        }
      }
      else
      {
        MsgBox.Show(Resources.MSG_SELECT_DB, Resources.HD_SELECT_DB, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void editUserToolStripMenuItem_Click(object sender, EventArgs e)
    {
      int userInfoIndex = -1;
      if (tvDBs.SelectedNode.Level == 3 && selectedTreeNode == TreeNodeType.User && tvDBs.SelectedNode.Tag != null)
      {
        userInfoIndex = Convert.ToInt32(tvDBs.SelectedNode.Tag);
      }
      //else if (lblListTitle.Text == "Users" && dgvItems.SelectedCells.Count > 0)
      //{

      //    userInfoIndex = dgvItems.SelectedCells[0].RowIndex;
      //}

      UserInfo selectedUi = SelectedDBInfo.UserInfoList[userInfoIndex];
      if (selectedUi != null)
      {
        // Instantiate UserDetailsDialog object with Edit user flag
        UserDetailsDialog editUserDialog = new UserDetailsDialog(selectedUi, SelectedDBInfo.DBFriendlyName);

        // Display the dialog and once User click OK button retrieve the newly entered info
        if (DlgBox.ShowDialog(editUserDialog) == DialogResult.OK)
        {
          // Refresh company list tree only if task was saved sucessfuly
          if (editUserDialog.IsSaveSuccess)
          {
            PopulateDBList();
          }
        }

        // Now dispose the dialog object
        editUserDialog.Dispose();
        editUserDialog = null;
      }
    }

    private void createTaskToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (tvDBs.SelectedNode.Level > 0)
      {
        //TreeNode savedNode = tvDBs.SelectedNode;
        //TaskDetailsDialog createTaskDialog = new TaskDetailsDialog(SelectedCompanyInfo.ConnString);
        BuildTaskDialog createTaskDialog = new BuildTaskDialog(SelectedDBInfo);

        if (DlgBox.ShowDialog(createTaskDialog) != DialogResult.Cancel)
        {
          // Refresh company list tree only if task was saved sucessfuly
          if (createTaskDialog.IsSaveSuccess)
          {
            PopulateDBList();
            //tvDBs.SelectedNode = savedNode;
            this.selectedTasks = (List<TaskInfo>)currentDBList[selectedDbIndex].TaskInfoList;
            DisplayTaskList(this.selectedTasks, false);
          }
        }

        // Now dispose the dialog object
        createTaskDialog.Dispose();
        createTaskDialog = null;
      }
      else
      {
        MsgBox.Show(Resources.MSG_SELECT_DB, Resources.HD_SELECT_DB, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void editTaskToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }

    private void deleteDBToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }

    private void deleteTaskToolStripMenuItem_Click(object sender, EventArgs e)
    {
      int taskInfoIndex = -1;
      if (tvDBs.SelectedNode.Level == 3 && selectedTreeNode == TreeNodeType.Task && tvDBs.SelectedNode.Tag != null)
      {
        taskInfoIndex = Convert.ToInt32(tvDBs.SelectedNode.Tag);
      }

      TaskInfo selectedTi = SelectedDBInfo.TaskInfoList[taskInfoIndex];
      if (selectedTi != null)
      {
        if (DialogResult.Cancel != MsgBox.Show("Delete the selected task record?", "Confirm Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question))
        {
          try
          {
            Task task = new Task();
            task.DeleteTask(selectedTi.ID);
            PopulateDBList();
          }
          catch (Exception ex)
          {
            throw ex;
          }
        }
      }

    }

    private void deleteUserToolStripMenuItem_Click(object sender, EventArgs e)
    {
      int userInfoIndex = -1;
      if (tvDBs.SelectedNode.Level == 3 && selectedTreeNode == TreeNodeType.User && tvDBs.SelectedNode.Tag != null)
      {
        userInfoIndex = Convert.ToInt32(tvDBs.SelectedNode.Tag);
      }

      UserInfo selectedUi = SelectedDBInfo.UserInfoList[userInfoIndex];
      if (selectedUi != null)
      {
        if (DialogResult.Cancel != MsgBox.Show("Delete the selected user record?", "Confirm Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question))
        {
          try
          {
            User user = new User();
            user.DeleteUser(selectedUi.Id);
            PopulateDBList();
          }
          catch (Exception ex)
          {
            throw ex;
          }
        }
      }
    }

    private void toolStripAddDB_Click(object sender, EventArgs e)
    {
      this.createDBToolStripMenuItem_Click(sender, e);
    }

    private void toolStripAddUser_Click(object sender, EventArgs e)
    {
      this.createUserToolStripMenuItem_Click(sender, e);
    }

    private void toolStripAddTask_Click(object sender, EventArgs e)
    {
      this.createTaskToolStripMenuItem_Click(sender, e);
    }

    private void toolStripEditDB_Click(object sender, EventArgs e)
    {
      this.editDBToolStripMenuItem_Click(sender, e);
    }

    private void toolStripEditUser_Click(object sender, EventArgs e)
    {
      this.editUserToolStripMenuItem_Click(sender, e);
    }

    private void toolStripEditTask_Click(object sender, EventArgs e)
    {
      this.editTaskToolStripMenuItem_Click(sender, e);
    }

    private void contentsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      // Display Help
      Help.ShowHelp(this, "XLETL_help.chm");
    }

    private void tsbTaskSchedule_Click(object sender, EventArgs e)
    {
      int rowIndex = (dgvItems.SelectedCells.Count > 0) ? dgvItems.SelectedCells[0].RowIndex : -1;
      if (rowIndex > -1)
      {
        TaskScheduleDialog tsd = new TaskScheduleDialog(this.selectedTasks[rowIndex]);
        if (DlgBox.ShowDialog(tsd) != DialogResult.Cancel)
        {
          this.selectedTasks[rowIndex].TaskSchedule = tsd.TaskSchedule;

          // Now refresh task list
          DisplayTaskList(selectedTasks, false);
        }
        tsd.Dispose();
        tsd = null;
      }
    }

    private void tsbETLProcess_Click(object sender, EventArgs e)
    {
      int rowIndex = (dgvItems.SelectedCells.Count > 0) ? dgvItems.SelectedCells[0].RowIndex : -1;
      if (rowIndex > -1)
      {
        ProcessTaskDialog ptd = new ProcessTaskDialog(this.selectedTasks[rowIndex], SelectedDBInfo);
        DlgBox.ShowDialog(ptd);
        ptd.Dispose();
        ptd = null;
      }
    }

    private void tsbViewTemplate_Click(object sender, EventArgs e)
    {
      int rowIndex = (dgvItems.SelectedCells.Count > 0) ? dgvItems.SelectedCells[0].RowIndex : -1;
      if (rowIndex > -1)
      {
        if (File.Exists(this.selectedTasks[rowIndex].TemplatePath))
        {
          // show template
          this.ViewDocument(this.selectedTasks[rowIndex].TemplatePath);
        }
      }
    }

    private void tsbGenerateTemplate_Click(object sender, EventArgs e)
    {
      // Start Generation Process
      int rowIndex = (dgvItems.SelectedCells.Count > 0) ? dgvItems.SelectedCells[0].RowIndex : -1;
      if (rowIndex > -1)
      {
        // Set template generator UI
        this.toolStripProgressBar1.Visible = true;
        this.toolStripStatusLabel1.Text = "Template Generating...";
        // Begin template generating asynchronously
        this.backgroundWorker.RunWorkerAsync(rowIndex);
      }
    }

    private void tsbViewETLLog_Click(object sender, EventArgs e)
    {
      // Start Generation Process
      int rowIndex = (dgvItems.SelectedCells.Count > 0) ? dgvItems.SelectedCells[0].RowIndex : -1;
      if (rowIndex > -1)
      {
        ViewETLLogs vLogs = new ViewETLLogs(this.selectedTasks[rowIndex]);
        DlgBox.ShowDialog(vLogs);
        vLogs.Dispose();
        vLogs = null;
      }
    }

    #endregion

    #region template generation

    /// <summary>
    /// Method to view specified document in the native viewer such as (Notepad, Office Excel)
    /// </summary>
    /// <param name="file">File path to view</param>
    private void ViewDocument(string file)
    {
      // Create a ProcessStartInfo object and configure it with the
      // information required to run the new process.
      ProcessStartInfo startInfo = new ProcessStartInfo();
      startInfo.FileName = file;
      startInfo.WindowStyle = ProcessWindowStyle.Normal;
      startInfo.ErrorDialog = true;
      // Declare a new Process object.
      Process process;
      try
      {
        // Start the new process.
        process = Process.Start(startInfo);

        Cursor.Current = Cursors.Default;
        //// Wait for the new process to terminate before exiting.
        //Console.WriteLine("Waiting 30 seconds for process to finish.");
        //if (process.WaitForExit(30000))
        //{
        //    Console.WriteLine("Process terminated.");
        //}
        //else
        //{
        //    Console.WriteLine("Timed out waiting for process to end.");
        //}
      }
      catch (Exception)
      {
        MsgBox.Show("Could not start process.");
      }
    }

    private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      XLETL.TemplateGenerator.TemplateGenerator tmplGen = new XLETL.TemplateGenerator.TemplateGenerator();

      tmplGen.GenerateTemplate(selectedTasks[(int)e.Argument], SelectedDBInfo);

    }

    private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      Cursor.Current = Cursors.Default;

      // Reset progress UI
      this.toolStripStatusLabel1.Text = "Ready";
      this.toolStripProgressBar1.Visible = false;

      // Was there an error?
      if (e.Error != null)
      {
        MsgBox.Show(e.Error.Message, "Error Generation");
        return;
      }

    }

    #endregion

    private void transformationPluginsToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }

    // Just for testng ETL Engine
    private void button1_Click(object sender, EventArgs e)
    {
      int rowIndex = (dgvItems.SelectedCells.Count > 0) ? dgvItems.SelectedCells[0].RowIndex : -1;
      if (rowIndex > -1)
      {
        Guid selectedTaskId = this.selectedTasks[rowIndex].ID;

        ETLEngine.ETLEngine etlEng = new XLETL.ETLEngine.ETLEngine();
        string fileName = @"c:\UploadedXLData\data_task.xlsm";
        //etlEng.StartETLProcess(selectedTaskId, fileName);
      }
    }

    private void MainForm_Load(object sender, EventArgs e)
    {

    }
  }
}

