﻿namespace XLETL.TaskManager.Forms
{
    partial class ProcessTaskDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcessTaskDialog));
          this.pictureBox1 = new System.Windows.Forms.PictureBox();
          this.progressBar1 = new System.Windows.Forms.ProgressBar();
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.lblTaskName = new System.Windows.Forms.Label();
          this.rtbLog = new System.Windows.Forms.RichTextBox();
          this.btnBrowse = new System.Windows.Forms.Button();
          this.label1 = new System.Windows.Forms.Label();
          this.tbDataFile = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.btnClose = new System.Windows.Forms.Button();
          this.btnProcess = new System.Windows.Forms.Button();
          this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
          this.openExcelFile = new System.Windows.Forms.OpenFileDialog();
          this.chbBackup = new System.Windows.Forms.CheckBox();
          this.btnRollback = new System.Windows.Forms.Button();
          this.chbTestRun = new System.Windows.Forms.CheckBox();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
          this.groupBox1.SuspendLayout();
          this.SuspendLayout();
          // 
          // pictureBox1
          // 
          this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
          this.pictureBox1.Location = new System.Drawing.Point(12, 9);
          this.pictureBox1.Name = "pictureBox1";
          this.pictureBox1.Size = new System.Drawing.Size(58, 53);
          this.pictureBox1.TabIndex = 0;
          this.pictureBox1.TabStop = false;
          // 
          // progressBar1
          // 
          this.progressBar1.Location = new System.Drawing.Point(170, 130);
          this.progressBar1.Name = "progressBar1";
          this.progressBar1.Size = new System.Drawing.Size(232, 10);
          this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
          this.progressBar1.TabIndex = 1;
          this.progressBar1.Visible = false;
          // 
          // groupBox1
          // 
          this.groupBox1.Controls.Add(this.lblTaskName);
          this.groupBox1.Location = new System.Drawing.Point(71, 7);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(333, 48);
          this.groupBox1.TabIndex = 2;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Task";
          // 
          // lblTaskName
          // 
          this.lblTaskName.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.lblTaskName.Location = new System.Drawing.Point(15, 14);
          this.lblTaskName.Name = "lblTaskName";
          this.lblTaskName.Size = new System.Drawing.Size(305, 23);
          this.lblTaskName.TabIndex = 12;
          this.lblTaskName.Text = "Task Name here";
          this.lblTaskName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // rtbLog
          // 
          this.rtbLog.Location = new System.Drawing.Point(6, 145);
          this.rtbLog.Name = "rtbLog";
          this.rtbLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
          this.rtbLog.Size = new System.Drawing.Size(401, 192);
          this.rtbLog.TabIndex = 3;
          this.rtbLog.Text = "";
          // 
          // btnBrowse
          // 
          this.btnBrowse.Location = new System.Drawing.Point(341, 70);
          this.btnBrowse.Name = "btnBrowse";
          this.btnBrowse.Size = new System.Drawing.Size(63, 21);
          this.btnBrowse.TabIndex = 4;
          this.btnBrowse.Text = "Browse...";
          this.btnBrowse.UseVisualStyleBackColor = true;
          this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(9, 74);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(96, 13);
          this.label1.TabIndex = 5;
          this.label1.Text = "Data Spreadsheet:";
          // 
          // tbDataFile
          // 
          this.tbDataFile.Location = new System.Drawing.Point(111, 70);
          this.tbDataFile.Name = "tbDataFile";
          this.tbDataFile.Size = new System.Drawing.Size(226, 20);
          this.tbDataFile.TabIndex = 6;
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(9, 126);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(69, 13);
          this.label2.TabIndex = 7;
          this.label2.Text = "Process Log:";
          // 
          // btnClose
          // 
          this.btnClose.Location = new System.Drawing.Point(341, 343);
          this.btnClose.Name = "btnClose";
          this.btnClose.Size = new System.Drawing.Size(65, 21);
          this.btnClose.TabIndex = 8;
          this.btnClose.Text = "Close";
          this.btnClose.UseVisualStyleBackColor = true;
          this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
          // 
          // btnProcess
          // 
          this.btnProcess.Location = new System.Drawing.Point(95, 343);
          this.btnProcess.Name = "btnProcess";
          this.btnProcess.Size = new System.Drawing.Size(98, 21);
          this.btnProcess.TabIndex = 9;
          this.btnProcess.Text = "Process Now!";
          this.btnProcess.UseVisualStyleBackColor = true;
          this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
          // 
          // backgroundWorker1
          // 
          this.backgroundWorker1.WorkerReportsProgress = true;
          this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
          this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
          this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
          // 
          // openExcelFile
          // 
          this.openExcelFile.FileName = "openFileDialog1";
          // 
          // chbBackup
          // 
          this.chbBackup.AutoSize = true;
          this.chbBackup.Location = new System.Drawing.Point(33, 101);
          this.chbBackup.Name = "chbBackup";
          this.chbBackup.Size = new System.Drawing.Size(153, 17);
          this.chbBackup.TabIndex = 10;
          this.chbBackup.Text = "Backup Target Table Data";
          this.chbBackup.UseVisualStyleBackColor = true;
          // 
          // btnRollback
          // 
          this.btnRollback.Enabled = false;
          this.btnRollback.Location = new System.Drawing.Point(198, 343);
          this.btnRollback.Name = "btnRollback";
          this.btnRollback.Size = new System.Drawing.Size(98, 21);
          this.btnRollback.TabIndex = 11;
          this.btnRollback.Text = "Roll Back";
          this.btnRollback.UseVisualStyleBackColor = true;
          this.btnRollback.Click += new System.EventHandler(this.btnRollback_Click);
          // 
          // chbTestRun
          // 
          this.chbTestRun.AutoSize = true;
          this.chbTestRun.Location = new System.Drawing.Point(16, 345);
          this.chbTestRun.Name = "chbTestRun";
          this.chbTestRun.Size = new System.Drawing.Size(70, 17);
          this.chbTestRun.TabIndex = 12;
          this.chbTestRun.Text = "Test Run";
          this.chbTestRun.UseVisualStyleBackColor = true;
          // 
          // ProcessTaskDialog
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(415, 372);
          this.Controls.Add(this.chbTestRun);
          this.Controls.Add(this.btnRollback);
          this.Controls.Add(this.chbBackup);
          this.Controls.Add(this.btnProcess);
          this.Controls.Add(this.btnClose);
          this.Controls.Add(this.label2);
          this.Controls.Add(this.tbDataFile);
          this.Controls.Add(this.label1);
          this.Controls.Add(this.btnBrowse);
          this.Controls.Add(this.rtbLog);
          this.Controls.Add(this.groupBox1);
          this.Controls.Add(this.progressBar1);
          this.Controls.Add(this.pictureBox1);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
          this.HelpButton = true;
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.Name = "ProcessTaskDialog";
          this.ShowInTaskbar = false;
          this.Text = "Process Task";
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
          this.groupBox1.ResumeLayout(false);
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblTaskName;
        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDataFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnProcess;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.OpenFileDialog openExcelFile;
        private System.Windows.Forms.CheckBox chbBackup;
        private System.Windows.Forms.Button btnRollback;
        private System.Windows.Forms.CheckBox chbTestRun;
    }
}