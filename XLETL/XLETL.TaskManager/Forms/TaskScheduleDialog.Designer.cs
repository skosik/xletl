﻿namespace XLETL.TaskManager.Forms
{
    partial class TaskScheduleDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TaskScheduleDialog));
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.lblTaskName = new System.Windows.Forms.Label();
          this.groupBox2 = new System.Windows.Forms.GroupBox();
          this.dtpTaskTime = new System.Windows.Forms.DateTimePicker();
          this.chbThur = new System.Windows.Forms.CheckBox();
          this.chbSun = new System.Windows.Forms.CheckBox();
          this.rdbDaily = new System.Windows.Forms.RadioButton();
          this.chbSat = new System.Windows.Forms.CheckBox();
          this.rdbWeekly = new System.Windows.Forms.RadioButton();
          this.chbFri = new System.Windows.Forms.CheckBox();
          this.chbWen = new System.Windows.Forms.CheckBox();
          this.chbMon = new System.Windows.Forms.CheckBox();
          this.chbTue = new System.Windows.Forms.CheckBox();
          this.label9 = new System.Windows.Forms.Label();
          this.pictureBox1 = new System.Windows.Forms.PictureBox();
          this.btnSave = new System.Windows.Forms.Button();
          this.btnCancel = new System.Windows.Forms.Button();
          this.groupBox1.SuspendLayout();
          this.groupBox2.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
          this.SuspendLayout();
          // 
          // groupBox1
          // 
          this.groupBox1.Controls.Add(this.lblTaskName);
          this.groupBox1.Location = new System.Drawing.Point(69, 7);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(206, 48);
          this.groupBox1.TabIndex = 3;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Task";
          // 
          // lblTaskName
          // 
          this.lblTaskName.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.lblTaskName.Location = new System.Drawing.Point(15, 14);
          this.lblTaskName.Name = "lblTaskName";
          this.lblTaskName.Size = new System.Drawing.Size(188, 23);
          this.lblTaskName.TabIndex = 12;
          this.lblTaskName.Text = "Task Name here";
          this.lblTaskName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // groupBox2
          // 
          this.groupBox2.Controls.Add(this.dtpTaskTime);
          this.groupBox2.Controls.Add(this.chbThur);
          this.groupBox2.Controls.Add(this.chbSun);
          this.groupBox2.Controls.Add(this.rdbDaily);
          this.groupBox2.Controls.Add(this.chbSat);
          this.groupBox2.Controls.Add(this.rdbWeekly);
          this.groupBox2.Controls.Add(this.chbFri);
          this.groupBox2.Controls.Add(this.chbWen);
          this.groupBox2.Controls.Add(this.chbMon);
          this.groupBox2.Controls.Add(this.chbTue);
          this.groupBox2.Controls.Add(this.label9);
          this.groupBox2.Location = new System.Drawing.Point(8, 61);
          this.groupBox2.Name = "groupBox2";
          this.groupBox2.Size = new System.Drawing.Size(267, 199);
          this.groupBox2.TabIndex = 4;
          this.groupBox2.TabStop = false;
          this.groupBox2.Text = "Process Schedule";
          // 
          // dtpTaskTime
          // 
          this.dtpTaskTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
          this.dtpTaskTime.Location = new System.Drawing.Point(53, 161);
          this.dtpTaskTime.Name = "dtpTaskTime";
          this.dtpTaskTime.ShowUpDown = true;
          this.dtpTaskTime.Size = new System.Drawing.Size(101, 20);
          this.dtpTaskTime.TabIndex = 24;
          this.dtpTaskTime.Value = new System.DateTime(2009, 1, 7, 0, 0, 0, 0);
          // 
          // chbThur
          // 
          this.chbThur.AutoSize = true;
          this.chbThur.Enabled = false;
          this.chbThur.Location = new System.Drawing.Point(117, 107);
          this.chbThur.Name = "chbThur";
          this.chbThur.Size = new System.Drawing.Size(70, 17);
          this.chbThur.TabIndex = 23;
          this.chbThur.Text = "Thursday";
          this.chbThur.UseVisualStyleBackColor = true;
          // 
          // chbSun
          // 
          this.chbSun.AutoSize = true;
          this.chbSun.Enabled = false;
          this.chbSun.Location = new System.Drawing.Point(190, 130);
          this.chbSun.Name = "chbSun";
          this.chbSun.Size = new System.Drawing.Size(62, 17);
          this.chbSun.TabIndex = 22;
          this.chbSun.Text = "Sunday";
          this.chbSun.UseVisualStyleBackColor = true;
          // 
          // rdbDaily
          // 
          this.rdbDaily.AutoSize = true;
          this.rdbDaily.Checked = true;
          this.rdbDaily.Location = new System.Drawing.Point(18, 25);
          this.rdbDaily.Name = "rdbDaily";
          this.rdbDaily.Size = new System.Drawing.Size(48, 17);
          this.rdbDaily.TabIndex = 14;
          this.rdbDaily.TabStop = true;
          this.rdbDaily.Text = "Daily";
          this.rdbDaily.UseVisualStyleBackColor = true;
          this.rdbDaily.CheckedChanged += new System.EventHandler(this.rdbDaily_CheckedChanged);
          // 
          // chbSat
          // 
          this.chbSat.AutoSize = true;
          this.chbSat.Enabled = false;
          this.chbSat.Location = new System.Drawing.Point(117, 130);
          this.chbSat.Name = "chbSat";
          this.chbSat.Size = new System.Drawing.Size(68, 17);
          this.chbSat.TabIndex = 21;
          this.chbSat.Text = "Saturday";
          this.chbSat.UseVisualStyleBackColor = true;
          // 
          // rdbWeekly
          // 
          this.rdbWeekly.AutoSize = true;
          this.rdbWeekly.Location = new System.Drawing.Point(18, 53);
          this.rdbWeekly.Name = "rdbWeekly";
          this.rdbWeekly.Size = new System.Drawing.Size(61, 17);
          this.rdbWeekly.TabIndex = 15;
          this.rdbWeekly.Text = "Weekly";
          this.rdbWeekly.UseVisualStyleBackColor = true;
          // 
          // chbFri
          // 
          this.chbFri.AutoSize = true;
          this.chbFri.Enabled = false;
          this.chbFri.Location = new System.Drawing.Point(30, 130);
          this.chbFri.Name = "chbFri";
          this.chbFri.Size = new System.Drawing.Size(54, 17);
          this.chbFri.TabIndex = 20;
          this.chbFri.Text = "Friday";
          this.chbFri.UseVisualStyleBackColor = true;
          // 
          // chbWen
          // 
          this.chbWen.AutoSize = true;
          this.chbWen.Enabled = false;
          this.chbWen.Location = new System.Drawing.Point(30, 107);
          this.chbWen.Name = "chbWen";
          this.chbWen.Size = new System.Drawing.Size(83, 17);
          this.chbWen.TabIndex = 19;
          this.chbWen.Text = "Wednesday";
          this.chbWen.UseVisualStyleBackColor = true;
          // 
          // chbMon
          // 
          this.chbMon.AutoSize = true;
          this.chbMon.Enabled = false;
          this.chbMon.Location = new System.Drawing.Point(30, 84);
          this.chbMon.Name = "chbMon";
          this.chbMon.Size = new System.Drawing.Size(64, 17);
          this.chbMon.TabIndex = 16;
          this.chbMon.Text = "Monday";
          this.chbMon.UseVisualStyleBackColor = true;
          // 
          // chbTue
          // 
          this.chbTue.AutoSize = true;
          this.chbTue.Enabled = false;
          this.chbTue.Location = new System.Drawing.Point(117, 84);
          this.chbTue.Name = "chbTue";
          this.chbTue.Size = new System.Drawing.Size(67, 17);
          this.chbTue.TabIndex = 18;
          this.chbTue.Text = "Tuesday";
          this.chbTue.UseVisualStyleBackColor = true;
          // 
          // label9
          // 
          this.label9.AutoSize = true;
          this.label9.Location = new System.Drawing.Point(14, 165);
          this.label9.Name = "label9";
          this.label9.Size = new System.Drawing.Size(33, 13);
          this.label9.TabIndex = 17;
          this.label9.Text = "Time:";
          // 
          // pictureBox1
          // 
          this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
          this.pictureBox1.Location = new System.Drawing.Point(9, 9);
          this.pictureBox1.Name = "pictureBox1";
          this.pictureBox1.Size = new System.Drawing.Size(54, 50);
          this.pictureBox1.TabIndex = 5;
          this.pictureBox1.TabStop = false;
          // 
          // btnSave
          // 
          this.btnSave.Location = new System.Drawing.Point(64, 266);
          this.btnSave.Name = "btnSave";
          this.btnSave.Size = new System.Drawing.Size(75, 23);
          this.btnSave.TabIndex = 6;
          this.btnSave.Text = "Save";
          this.btnSave.UseVisualStyleBackColor = true;
          this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
          // 
          // btnCancel
          // 
          this.btnCancel.Location = new System.Drawing.Point(145, 266);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(75, 23);
          this.btnCancel.TabIndex = 7;
          this.btnCancel.Text = "Cancel";
          this.btnCancel.UseVisualStyleBackColor = true;
          this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
          // 
          // TaskScheduleDialog
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(285, 297);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnSave);
          this.Controls.Add(this.pictureBox1);
          this.Controls.Add(this.groupBox2);
          this.Controls.Add(this.groupBox1);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.Name = "TaskScheduleDialog";
          this.ShowInTaskbar = false;
          this.Text = "Task Schedule";
          this.groupBox1.ResumeLayout(false);
          this.groupBox2.ResumeLayout(false);
          this.groupBox2.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblTaskName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtpTaskTime;
        private System.Windows.Forms.CheckBox chbThur;
        private System.Windows.Forms.CheckBox chbSun;
        private System.Windows.Forms.RadioButton rdbDaily;
        private System.Windows.Forms.CheckBox chbSat;
        private System.Windows.Forms.RadioButton rdbWeekly;
        private System.Windows.Forms.CheckBox chbFri;
        private System.Windows.Forms.CheckBox chbWen;
        private System.Windows.Forms.CheckBox chbMon;
        private System.Windows.Forms.CheckBox chbTue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}