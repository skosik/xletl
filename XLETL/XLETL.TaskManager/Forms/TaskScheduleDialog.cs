﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XLETL.Model;
using XLETL.BLL;

namespace XLETL.TaskManager.Forms
{
    public partial class TaskScheduleDialog : Form
    {
        
        public ScheduleInfo TaskSchedule { get; set; }

      private TaskInfo currentTask = null;
      private Schedule schedule = null; // business object

        public TaskScheduleDialog(TaskInfo taskInfo)
        {
            InitializeComponent();

            if (taskInfo == null)
            {
              throw new ArgumentNullException("taskInfo");
            }

          currentTask = taskInfo;
          lblTaskName.Text = taskInfo.TaskDetails.Name;
          TaskSchedule = taskInfo.TaskSchedule;

          if (TaskSchedule != null)
          {
            LoadScheduleIntoForm(TaskSchedule);
          }          
        }

        private void LoadScheduleIntoForm(ScheduleInfo taskSchedule)
        {
          // get schedule       
          if (taskSchedule != null)
          {
            rdbDaily.Checked = taskSchedule.Dayly;
            
            if (taskSchedule.Weekly)
            {
              DayOfWeek dOw;

              // iterate through each week day and check the appropriate form control
              for(int i=0; i<taskSchedule.WeekDays.Length; i++)
              {
                dOw = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), taskSchedule.WeekDays[i]);
                switch (dOw)
                {
                  case DayOfWeek.Monday:
                    this.chbMon.Checked = true;
                    break;
                  case DayOfWeek.Tuesday:
                    this.chbTue.Checked = true;
                    break;
                  case DayOfWeek.Wednesday:
                    this.chbWen.Checked = true;
                    break;
                  case DayOfWeek.Thursday:
                    this.chbThur.Checked = true;
                    break;
                  case DayOfWeek.Friday:
                    this.chbFri.Checked = true;
                    break;
                  case DayOfWeek.Saturday:
                    this.chbSat.Checked = true;
                    break;
                  case DayOfWeek.Sunday:
                    this.chbSun.Checked = true;
                    break;
                }
              }
            }
            this.dtpTaskTime.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, taskSchedule.Hour, taskSchedule.Min, 0);

          }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
          if (rdbWeekly.Checked)
          {
            bool isValid = this.ValidateIfWeekDayChecked();
            if (!isValid)
            {
              MessageBox.Show("At least one Week Day must be checked");
              return;
            }
          }
          // instantiate object
          schedule = new Schedule();

          bool isNew = false;

          if (TaskSchedule == null)
          {
            TaskSchedule = new ScheduleInfo();
            TaskSchedule.TaskID = currentTask.ID;
            TaskSchedule.ID = Guid.NewGuid();
            isNew = true;
          }


          TaskSchedule.Dayly = this.rdbDaily.Checked;
          TaskSchedule.Weekly = this.rdbWeekly.Checked;
          if (rdbWeekly.Checked)
          {            
            List<string> listDayOfWeek = new List<string>();
            foreach (Control cntrl in groupBox2.Controls)
            {
              if (cntrl is CheckBox && (cntrl as CheckBox).Checked)
              {
                listDayOfWeek.Add(cntrl.Text);                
              }
            }
            TaskSchedule.WeekDays = listDayOfWeek.ToArray();
          }
          
          TaskSchedule.Hour = dtpTaskTime.Value.Hour;
          TaskSchedule.Min = dtpTaskTime.Value.Minute;

          bool isSuccess = schedule.SaveTaskProcessSchedule(TaskSchedule, isNew);
          if (isSuccess)
          {
            this.DialogResult = DialogResult.OK;
          }
          else
          {
            throw new ApplicationException("Error saving the Task schedule");
          }
        }

      private void rdbDaily_CheckedChanged(object sender, EventArgs e)
      {
        rdbWeekly.Checked = !rdbDaily.Checked;
        IsEnableWeekDayControls(!rdbDaily.Checked);
      }

      private void IsEnableWeekDayControls(bool isEnable)
      {
        this.chbMon.Enabled = isEnable;
        this.chbTue.Enabled = isEnable;
        this.chbWen.Enabled = isEnable;
        this.chbThur.Enabled = isEnable;
        this.chbFri.Enabled = isEnable;
        this.chbSat.Enabled = isEnable;
        this.chbSun.Enabled = isEnable;
      }

      private void btnCancel_Click(object sender, EventArgs e)
      {
        this.DialogResult = DialogResult.Cancel;
      } 

      private bool ValidateIfWeekDayChecked()
      {
        bool isValid = false;
        foreach (Control cntrl in groupBox2.Controls)
        {
          if (cntrl is CheckBox && (cntrl as CheckBox).Checked)
          {
            isValid = true;
            break;
          }
        }
        return isValid;
      }         
  }
}

