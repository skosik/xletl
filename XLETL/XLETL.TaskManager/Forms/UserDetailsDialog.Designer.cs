namespace XLETL.TaskManager.Forms
{
    partial class UserDetailsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDetailsDialog));
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.tbPhone = new System.Windows.Forms.TextBox();
          this.label6 = new System.Windows.Forms.Label();
          this.tbLastName = new System.Windows.Forms.TextBox();
          this.label5 = new System.Windows.Forms.Label();
          this.tbFirstName = new System.Windows.Forms.TextBox();
          this.label4 = new System.Windows.Forms.Label();
          this.label1 = new System.Windows.Forms.Label();
          this.tbUsername = new System.Windows.Forms.TextBox();
          this.tbPassword = new System.Windows.Forms.TextBox();
          this.label2 = new System.Windows.Forms.Label();
          this.tbEmail = new System.Windows.Forms.TextBox();
          this.label3 = new System.Windows.Forms.Label();
          this.groupBox2 = new System.Windows.Forms.GroupBox();
          this.btnOK = new System.Windows.Forms.Button();
          this.btnCancel = new System.Windows.Forms.Button();
          this.lblDBName = new System.Windows.Forms.Label();
          this.groupBox3 = new System.Windows.Forms.GroupBox();
          this.pictureBox1 = new System.Windows.Forms.PictureBox();
          this.groupBox1.SuspendLayout();
          this.groupBox2.SuspendLayout();
          this.groupBox3.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
          this.SuspendLayout();
          // 
          // groupBox1
          // 
          this.groupBox1.Controls.Add(this.tbPhone);
          this.groupBox1.Controls.Add(this.label6);
          this.groupBox1.Controls.Add(this.tbLastName);
          this.groupBox1.Controls.Add(this.label5);
          this.groupBox1.Controls.Add(this.tbFirstName);
          this.groupBox1.Controls.Add(this.label4);
          this.groupBox1.Location = new System.Drawing.Point(8, 173);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(280, 104);
          this.groupBox1.TabIndex = 4;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Personal Details";
          // 
          // tbPhone
          // 
          this.tbPhone.Location = new System.Drawing.Point(88, 72);
          this.tbPhone.Name = "tbPhone";
          this.tbPhone.Size = new System.Drawing.Size(173, 21);
          this.tbPhone.TabIndex = 7;
          // 
          // label6
          // 
          this.label6.AutoSize = true;
          this.label6.Location = new System.Drawing.Point(15, 76);
          this.label6.Name = "label6";
          this.label6.Size = new System.Drawing.Size(41, 13);
          this.label6.TabIndex = 10;
          this.label6.Text = "Phone:";
          // 
          // tbLastName
          // 
          this.tbLastName.Location = new System.Drawing.Point(88, 45);
          this.tbLastName.Name = "tbLastName";
          this.tbLastName.Size = new System.Drawing.Size(173, 21);
          this.tbLastName.TabIndex = 6;
          // 
          // label5
          // 
          this.label5.AutoSize = true;
          this.label5.Location = new System.Drawing.Point(15, 49);
          this.label5.Name = "label5";
          this.label5.Size = new System.Drawing.Size(61, 13);
          this.label5.TabIndex = 8;
          this.label5.Text = "Last Name:";
          // 
          // tbFirstName
          // 
          this.tbFirstName.Location = new System.Drawing.Point(88, 19);
          this.tbFirstName.Name = "tbFirstName";
          this.tbFirstName.Size = new System.Drawing.Size(173, 21);
          this.tbFirstName.TabIndex = 5;
          // 
          // label4
          // 
          this.label4.AutoSize = true;
          this.label4.Location = new System.Drawing.Point(15, 23);
          this.label4.Name = "label4";
          this.label4.Size = new System.Drawing.Size(62, 13);
          this.label4.TabIndex = 6;
          this.label4.Text = "First Name:";
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(15, 22);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(59, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "Username:";
          // 
          // tbUsername
          // 
          this.tbUsername.Location = new System.Drawing.Point(88, 18);
          this.tbUsername.Name = "tbUsername";
          this.tbUsername.Size = new System.Drawing.Size(173, 21);
          this.tbUsername.TabIndex = 1;
          // 
          // tbPassword
          // 
          this.tbPassword.Location = new System.Drawing.Point(88, 44);
          this.tbPassword.Name = "tbPassword";
          this.tbPassword.Size = new System.Drawing.Size(173, 21);
          this.tbPassword.TabIndex = 2;
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(15, 48);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(57, 13);
          this.label2.TabIndex = 2;
          this.label2.Text = "Password:";
          // 
          // tbEmail
          // 
          this.tbEmail.Location = new System.Drawing.Point(88, 70);
          this.tbEmail.Name = "tbEmail";
          this.tbEmail.Size = new System.Drawing.Size(173, 21);
          this.tbEmail.TabIndex = 3;
          // 
          // label3
          // 
          this.label3.AutoSize = true;
          this.label3.Location = new System.Drawing.Point(15, 74);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(35, 13);
          this.label3.TabIndex = 4;
          this.label3.Text = "Email:";
          // 
          // groupBox2
          // 
          this.groupBox2.Controls.Add(this.tbUsername);
          this.groupBox2.Controls.Add(this.tbPassword);
          this.groupBox2.Controls.Add(this.label3);
          this.groupBox2.Controls.Add(this.label1);
          this.groupBox2.Controls.Add(this.tbEmail);
          this.groupBox2.Controls.Add(this.label2);
          this.groupBox2.Location = new System.Drawing.Point(8, 69);
          this.groupBox2.Name = "groupBox2";
          this.groupBox2.Size = new System.Drawing.Size(280, 101);
          this.groupBox2.TabIndex = 0;
          this.groupBox2.TabStop = false;
          this.groupBox2.Text = "User Details";
          // 
          // btnOK
          // 
          this.btnOK.Location = new System.Drawing.Point(71, 285);
          this.btnOK.Name = "btnOK";
          this.btnOK.Size = new System.Drawing.Size(75, 23);
          this.btnOK.TabIndex = 8;
          this.btnOK.Text = "OK";
          this.btnOK.UseVisualStyleBackColor = true;
          this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
          // 
          // btnCancel
          // 
          this.btnCancel.Location = new System.Drawing.Point(152, 285);
          this.btnCancel.Name = "btnCancel";
          this.btnCancel.Size = new System.Drawing.Size(75, 23);
          this.btnCancel.TabIndex = 9;
          this.btnCancel.Text = "Cancel";
          this.btnCancel.UseVisualStyleBackColor = true;
          this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
          // 
          // lblDBName
          // 
          this.lblDBName.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.lblDBName.Location = new System.Drawing.Point(10, 17);
          this.lblDBName.Name = "lblDBName";
          this.lblDBName.Size = new System.Drawing.Size(192, 14);
          this.lblDBName.TabIndex = 11;
          this.lblDBName.Text = "Database Name here";
          this.lblDBName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          // 
          // groupBox3
          // 
          this.groupBox3.Controls.Add(this.lblDBName);
          this.groupBox3.Location = new System.Drawing.Point(76, 10);
          this.groupBox3.Name = "groupBox3";
          this.groupBox3.Size = new System.Drawing.Size(211, 43);
          this.groupBox3.TabIndex = 10;
          this.groupBox3.TabStop = false;
          this.groupBox3.Text = "Database";
          // 
          // pictureBox1
          // 
          this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
          this.pictureBox1.Location = new System.Drawing.Point(14, 11);
          this.pictureBox1.Name = "pictureBox1";
          this.pictureBox1.Size = new System.Drawing.Size(52, 50);
          this.pictureBox1.TabIndex = 11;
          this.pictureBox1.TabStop = false;
          // 
          // UserDetailsDialog
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(299, 317);
          this.Controls.Add(this.pictureBox1);
          this.Controls.Add(this.groupBox3);
          this.Controls.Add(this.btnCancel);
          this.Controls.Add(this.btnOK);
          this.Controls.Add(this.groupBox2);
          this.Controls.Add(this.groupBox1);
          this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
          this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
          this.MaximizeBox = false;
          this.MinimizeBox = false;
          this.Name = "UserDetailsDialog";
          this.Text = "Create User";
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          this.groupBox2.ResumeLayout(false);
          this.groupBox2.PerformLayout();
          this.groupBox3.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbPhone;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblDBName;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}