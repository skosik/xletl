using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using XLETL.BLL;
using XLETL.Model;
using XLETL.ExceptionHandling;

namespace XLETL.TaskManager.Forms
{
    public partial class UserDetailsDialog : Form
    {
        #region Properties

        /// <summary>
        /// Hold the status of save task's operation to report it to the mainform
        /// </summary>
        public bool IsSaveSuccess { get; set; }        

        public UserInfo userInfo { get; private set; }

        #endregion

        public UserDetailsDialog(Guid companyId, string companyName)
        {
            InitializeComponent();

            this.lblDBName.Text = companyName;
            this.Text = "Create User";
            userInfo = new UserInfo();
            userInfo.DatabaseID = companyId;
        }
            

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="isCreate"></param>
        /// <param name="companyName"></param>
        public UserDetailsDialog(UserInfo usertoEdit, string companyName)
        {
            InitializeComponent();

            // Set appropriate dialog title and company name
            this.Text = "Edit User";
            this.lblDBName.Text = companyName;
            userInfo = usertoEdit;
            tbFirstName.Text = usertoEdit.FirstName;
            tbLastName.Text = usertoEdit.LastName;
            tbEmail.Text = usertoEdit.Email;
            tbPhone.Text = usertoEdit.Phone;
            tbUsername.Text = usertoEdit.UserName;
            tbPassword.Text = usertoEdit.Password;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                // Save User Details to persistent storage                
                userInfo.UserName = this.tbUsername.Text;
                userInfo.Password = this.tbPassword.Text;
                userInfo.FirstName = this.tbFirstName.Text;
                userInfo.LastName = this.tbLastName.Text;
                userInfo.Email = this.tbEmail.Text;
                userInfo.Phone = this.tbPhone.Text;                

                // Instantiate BL's company object
                User user = new User();
                user.SaveUser(userInfo);

                IsSaveSuccess = true;
            }
            catch (Exception ex)
            {
                IsSaveSuccess = false;

                //if (ExceptionHandleProvider.HandleException(ex, "Global Policy") == true)
                //{
                //    throw ex;
                //}
                throw new ApplicationException("Error occured", ex);
            }
            finally
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}