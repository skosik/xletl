﻿namespace XLETL.TaskManager.Forms
{
  partial class ViewETLLogs
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewETLLogs));
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.lblTaskName = new System.Windows.Forms.Label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.label1 = new System.Windows.Forms.Label();
      this.cbLogItems = new System.Windows.Forms.ComboBox();
      this.tbETLLog = new System.Windows.Forms.TextBox();
      this.btnClose = new System.Windows.Forms.Button();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox1.Controls.Add(this.lblTaskName);
      this.groupBox1.Location = new System.Drawing.Point(99, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(355, 48);
      this.groupBox1.TabIndex = 3;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Task";
      // 
      // lblTaskName
      // 
      this.lblTaskName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.lblTaskName.Font = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.lblTaskName.Location = new System.Drawing.Point(15, 14);
      this.lblTaskName.Name = "lblTaskName";
      this.lblTaskName.Size = new System.Drawing.Size(305, 23);
      this.lblTaskName.TabIndex = 12;
      this.lblTaskName.Text = "Task Name here";
      this.lblTaskName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
      this.pictureBox1.Location = new System.Drawing.Point(22, 13);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(64, 50);
      this.pictureBox1.TabIndex = 4;
      this.pictureBox1.TabStop = false;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(31, 85);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(51, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "ETL Log:";
      // 
      // cbLogItems
      // 
      this.cbLogItems.FormattingEnabled = true;
      this.cbLogItems.Location = new System.Drawing.Point(100, 81);
      this.cbLogItems.Name = "cbLogItems";
      this.cbLogItems.Size = new System.Drawing.Size(274, 21);
      this.cbLogItems.TabIndex = 6;
      this.cbLogItems.SelectedIndexChanged += new System.EventHandler(this.cbLogItems_SelectedIndexChanged);
      // 
      // tbETLLog
      // 
      this.tbETLLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.tbETLLog.Location = new System.Drawing.Point(12, 114);
      this.tbETLLog.Multiline = true;
      this.tbETLLog.Name = "tbETLLog";
      this.tbETLLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.tbETLLog.Size = new System.Drawing.Size(442, 196);
      this.tbETLLog.TabIndex = 7;
      // 
      // btnClose
      // 
      this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
      this.btnClose.Location = new System.Drawing.Point(196, 316);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(75, 23);
      this.btnClose.TabIndex = 8;
      this.btnClose.Text = "Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // ViewETLLogs
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(466, 344);
      this.Controls.Add(this.btnClose);
      this.Controls.Add(this.tbETLLog);
      this.Controls.Add(this.cbLogItems);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.groupBox1);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ViewETLLogs";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "ETL Log Viewer";
      this.Load += new System.EventHandler(this.ViewETLLogs_Load);
      this.groupBox1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label lblTaskName;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox cbLogItems;
    private System.Windows.Forms.TextBox tbETLLog;
    private System.Windows.Forms.Button btnClose;

  }
}