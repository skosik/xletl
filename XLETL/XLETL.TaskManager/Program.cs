using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Text;
using XLETL.TaskManager.Forms;
using XLETL.ExceptionHandling;

namespace XLETL.TaskManager
{
    public static class Program
    {
        //public static string dbConnString = null; 

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ExceptionHandler.AddHandler(true, true, false);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Unhandled exceptions will be delivered to our ThreadException handler
            //Application.ThreadException += new ThreadExceptionEventHandler(AppThreadException);

           LoginForm loginForm = new LoginForm();
           loginForm.ShowDialog();
           bool closeApp = loginForm.IsClose;
           loginForm.Close();
           loginForm.Dispose();

          if (!closeApp)
          {
            MainForm mainForm = new MainForm();
            Application.Run(mainForm);            
          }
          else
          {
            Application.Exit();
          }          
        }

        /// <summary>
        /// Displays dialog with information about exceptions that occur in the application. 
        /// </summary>
        /*private static void AppThreadException(object source, ThreadExceptionEventArgs e)
        {
            ProcessUnhandledException(e.Exception);
        }*/

        /// <summary>
        /// Process any unhandled exceptions that occur in the application.
        /// This code is called by all UI entry points in the application (e.g. button click events)
        /// when an unhandled exception occurs.
        /// You could also achieve this by handling the Application.ThreadException event, however
        /// the VS2005 debugger will break before this event is called.
        /// </summary>
        /// <param name="ex">The unhandled exception</param>
        /*private static void ProcessUnhandledException(Exception ex)
        {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.AppendFormat("The following error occured during execution of the XLETL Task Manager.\n\n{0}\n\n", ex.Message);
            errorMessage.Append("Exceptions can be caused by invalid configuration information.\n");
            errorMessage.Append(Environment.NewLine);
            errorMessage.Append("Do you want to exit the application?");

            DialogResult result = MessageBox.Show(errorMessage.ToString(), "Application Error", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

            // Exits the program when the user clicks Abort.
            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }            
        }*/
    }
}