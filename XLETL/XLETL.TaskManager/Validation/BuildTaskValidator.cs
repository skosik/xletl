﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using XLETL.Model;

namespace XLETL.TaskManager.Business
{
    public class BuildTaskValidator
    {
      const string errorMsgAtLeastOneKey = "- There must be defined at least one Template column as a key field for this process mode";
      const string errorMsgDefaultValueForTargetColumn = "- Target Column: '{0}' should have a default value or moved into the Template document";
      const string errorMsgComboboxColumnWithoutLookup = "- Source Column: '{0}' defined as Combobox and should have a lookup transformation";
      const string errorMsgUniqueTemplateHeadings = "- Found duplicate heading name: '{0}'; Template Column headings must be unique";
      
      StringBuilder sbErrorLog = new StringBuilder();

      public string ErrorLog
      {
        get
        {
          return sbErrorLog.ToString();
        }
      }

      public void ClearErrorLog()
      {
        sbErrorLog.Remove(0, sbErrorLog.Length);
      }

      public bool IsTaskValid
      {
        get
        {
          return (sbErrorLog.Length > 0) ? false : true;
        }
      }
      
      /// <summary>
        /// Method to validate that the Target Column List contains only nullable columns
        /// or mandatory columns that have transformations pre-set
        /// </summary>
        /// <returns></returns>
        public bool ValidateMandatoryTargetColumns(List<TargetColumnInfo> columnList)
        {
            //StringBuilder sb = new StringBuilder();
          bool isValid = true;

            foreach (TargetColumnInfo tci in columnList)
            {
              if (!tci.IsIdentity && !tci.IsDefaultNull && !tci.Name.ToLower().Equals("rowguid") && (tci.Transformations.Count == 0))
                {
                  sbErrorLog.AppendFormat(errorMsgDefaultValueForTargetColumn, tci.Name);
                  sbErrorLog.AppendLine();
                  isValid = false;
                }
            }
            return isValid;
        }

        /// <summary>
        /// Method to validate that at least one column is defined as key field
        /// </summary>
        /// <param name="columnList"></param>
        /// <returns></returns>
        public bool ValidateKeyFieldsExist(List<SourceColumnInfo> columnList)
        {
            bool isValid = false;

            foreach (SourceColumnInfo sci in columnList)
            {
              if (sci.IsKeyField)
                {
                    isValid = true;
                    break; // once the key is found we can leave the loop 
                }
            }

            if (!isValid)
            {
              sbErrorLog.Append(errorMsgAtLeastOneKey);
              sbErrorLog.AppendLine();
            }
            return isValid;
        }

        public bool ValidateUniqueTemplateHeadings(List<SourceColumnInfo> columnList)
        {
            bool isValid = true;
            //int count = 0;
            SourceColumnInfo search = null;
            for (int i = 0; i < columnList.Count; i++)
            {
                if ((i + 1) <= columnList.Count)
                {
                    search = columnList[i];

                    for (int j = i + 1; j < columnList.Count; j++)
                    {
                        if (search.Name.Equals(columnList[j].Name))
                        {
                            // we found it then exit the loop
                          sbErrorLog.AppendFormat(errorMsgUniqueTemplateHeadings, columnList[j].Name);
                          sbErrorLog.AppendLine();
                          isValid = false;
                          break;
                        }
                    }
                }
            }            
            
            return isValid;
        }

      /// <summary>
      /// Validate that all combobox template columns are assigned a lookup transformation 
      /// </summary>
      /// <param name="columnList"></param>
      /// <returns></returns>
        public bool ValidateLookupColumns(List<SourceColumnInfo> columnList)
        {
          bool isValid = true;

          foreach (SourceColumnInfo sci in columnList)
          {
            if (sci.Style == Globals.CellStyle.Combobox)
            {
              PluginInfo pi = sci.Transformations.Find(delegate(PluginInfo search)
                {
                    return search.Name.Equals("Lookup");                        
                });
              //foreach (PluginInfo pi in sci.Transformations)
              //{
              //  if (pi.Name == "Lookup")
              //  {
              //    isValid = true; 
              //    break;
              //  }                 
              //}
              if (pi == null)
              {
                sbErrorLog.AppendFormat(errorMsgComboboxColumnWithoutLookup, sci.Name);
                sbErrorLog.AppendLine();
                isValid = false;
              }              
            }
          }
          return isValid;
        }
    }
}
