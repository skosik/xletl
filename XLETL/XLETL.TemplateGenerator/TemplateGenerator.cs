using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Data.SqlClient;
using System.Data;
using XLETL.Model;
using XLETL.BLL;
using XLETL.ExceptionHandling;
using XLETL.Common;
using System.ComponentModel;
using Microsoft.Office.Interop.Excel;

namespace XLETL.TemplateGenerator
{
    /// <summary>
    /// Template Generator class contains methods for generating excel template
    /// </summary>
    public class TemplateGenerator
    {
        //string templatePath = AppDomain.CurrentDomain.BaseDirectory + "//storage//tmpl_11.xlsm";
       // string templatePath = "c:\\tmpl_11.xlsm";

        private int rowRangeLimit = 63000; // hold the limit of rows in the sheet;

        public bool GenerateTemplate(TaskInfo taskInfo, DBInfo dbInfo)
        {
            TargetDatabase targetDb = null;

            try
            {
                targetDb = new TargetDatabase(dbInfo.DALClassName);
            }
            catch (Exception ex)
            {                
              throw new Exception("Error creating TargetDatabase class", ex);
            }

            // 1. Build the list of tables which includes lookup tables
            DataSet tableList = new DataSet();                              

            foreach (SourceColumnInfo scInfo in taskInfo.SourceColumns)
            {
                if (scInfo.Style == Globals.CellStyle.Combobox)
                {
                    // Get Lookup transformation
                    PluginInfo foundPlugin = scInfo.Transformations.Find(delegate(PluginInfo search)
                    {
                        if (search.Name.Equals("Lookup"))
                            return true;
                        else
                            return false;
                    });

                    if (foundPlugin != null)
                    {
                        XLETL.Model.Parameter[] transParams = foundPlugin.TransformationParamList;

                        // get data of the table
                        System.Data.DataTable lookupDataTable = targetDb.GetDataOfTable(dbInfo.ConnString, Convert.ToString(transParams[0].ParameterValue), new string[] { Convert.ToString(transParams[1].ParameterValue), Convert.ToString(transParams[2].ParameterValue) });
                        tableList.Tables.Add(lookupDataTable);
                    }
                }
            }

            // 2. Generate workbook
            this.CreateWorkbook(taskInfo.TemplatePath, tableList, taskInfo);            

            return true;
        }        

        #region Get Macro        

        private string CreateDropDownsMacro(string rangeFrom, string rangeTo, string sheetFrom, string sheetTo, int numCount)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("Sub CreateDropDownList{0}()\n", numCount);
            sb.AppendFormat("  Sheets(\"{0}\").Select\n", sheetFrom);
            sb.AppendFormat("  Range(\"{0}\").Select\n", rangeFrom);
            sb.AppendFormat("  ActiveWorkbook.Names.Add Name:=\"{0}List\", RefersToR1C1:= _\n", sheetFrom);
            sb.AppendFormat("  \"={0}!R{1}C2:R{2}C2\"\n", sheetFrom, rangeFrom.Substring(1, 1), rangeFrom.Substring(4));
            sb.AppendFormat(" Sheets(\"{0}\").Select\n", sheetTo);
            sb.AppendFormat(" Range(\"{0}\").Select\n", rangeTo);
            sb.Append(" With Selection.Validation\n");
            sb.Append(" .Delete\n");
            sb.Append(" .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, Operator:= _\n");
            sb.AppendFormat(" xlBetween, Formula1:=\"={0}List\"\n", sheetFrom);
            sb.Append("  .IgnoreBlank = True\n");
            sb.Append("  .InCellDropdown = True\n");
            sb.Append("  .InputTitle = \"\"\n");
            sb.Append("  .ErrorTitle = \"\"\n");
            sb.Append("  .InputMessage = \"\"\n");
            sb.Append("  .ErrorMessage = \"\"\n");
            sb.Append("  .ShowInput = True\n");
            sb.Append("  .ShowError = True\n");
            sb.Append("  End With\n");
            sb.Append("End Sub\n");

            return sb.ToString();
        }

        private string CreateCheckBoxesMacro(string rangeTarget, string sheetTarget, string columnName)
        {
          StringBuilder sb = new StringBuilder();

          sb.AppendFormat("Sub CreateCheckBoxes{0}()\n", columnName);
          sb.Append("  On Error Resume Next\n");
          sb.Append("  Dim c As Range, myRange As Range\n");
          sb.AppendFormat("  Sheets(\"{0}\").Select\n", sheetTarget);
          sb.AppendFormat("  Set myRange = Range(\"{0}\")\n", rangeTarget);
          sb.Append("  For Each c In myRange.Cells\n");
          sb.Append("    ActiveSheet.CheckBoxes.Add(c.Left, c.Top, c.Width, c.Height).Select\n");
          sb.Append("      With Selection\n");
          sb.Append("       .LinkedCell = c.Address\n");
          sb.Append("       .Characters.Text = \"\"\n");
          sb.Append("       .Name = c.Address\n");
          sb.Append("      End With\n");
          sb.Append("      c.Select\n");
          sb.Append("      With Selection\n");
          sb.Append("         .FormatConditions.Delete\n");
          sb.Append("         .FormatConditions.Add Type:=xlExpression, _\n");
          sb.Append("            Formula1:=\"=\" & c.Address & \"=TRUE\"\n");
          sb.Append("         .FormatConditions(1).Font.ColorIndex = 6\n");   // 'change for other color when ticked
          sb.Append("         .FormatConditions(1).Interior.ColorIndex = 6\n"); // 'change for other color when ticked
          sb.Append("         .Font.ColorIndex = 2\n");   // 'cell background color = White
          sb.Append("      End With\n");
          sb.Append("    Next\n");
          sb.Append("    myRange.Select\n");
          sb.Append("End Sub\n");
          
          return sb.ToString();
        }
        #endregion

        #region Create Workbook
        private void CreateWorkbook(string FileName, DataSet ds, TaskInfo currentTask)
        {

            Application xl = null;
            _Workbook wb = null;
            _Worksheet sheet = null;
            Microsoft.Vbe.Interop.VBComponent module = null;            
            Sheets sheets = null;
            Range range = null;
            bool SaveChanges = false;
            bool IsMacroEnabled = false;

            try
            {
                // delete old template if exist
                if (File.Exists(FileName)) { File.Delete(FileName); }

                GC.Collect();

                //create new EXCEL application
                xl = new Application();
                xl.Visible = false;
                xl.Caption = Guid.NewGuid().ToString().ToUpper();
                XlFileFormat xlFileFormat = XlFileFormat.xlOpenXMLWorkbookMacroEnabled;

                //Add a new workbook to the file
                wb = (_Workbook)(xl.Workbooks.Add(Missing.Value));

                //get the workbook sheets collection
                sheets = wb.Sheets;
                
                int sheetCount = 1;

                // Now get first sheet
                sheet = (_Worksheet)sheets.get_Item(sheetCount);
                sheet.Name = currentTask.TaskDetails.SourceSheetName;
                sheet.Activate();

                this.fillWorksheet_Headings(sheet, range, currentTask.SourceColumns);
                
                for(int c=0; c<currentTask.SourceColumns.Count; c++)
                {
                    if (currentTask.SourceColumns[c].Style == Globals.CellStyle.Combobox)
                    {
                        // Use 2 default sheets as we used the first already
                        if (sheetCount < sheets.Count)
                        {
                            sheet = (_Worksheet)sheets.get_Item(sheetCount + 1);
                        }
                        else
                        {
                            //Create a new sheet for the requested sheet
                            sheet = (_Worksheet)wb.Sheets.Add(Type.Missing, (_Worksheet)sheets.get_Item(sheets.Count), Type.Missing, Type.Missing);
                        }

                        // Get DataView for the next lookup table                        
                        DataView dv = ds.Tables[sheetCount - 1].DefaultView;

                        //Change its name to that requested
                        sheet.Name = ds.Tables[sheetCount - 1].TableName;

                        sheet.Activate();

                        // Fill EXCEL worksheet with DataView values
                        string rangeFrom = fillWorksheet_WithDataView(sheet, range, dv);

                        // Create the appropriate cell range
                        string rangeTo = string.Format("{0}2:{0}{1}", char.ConvertFromUtf32(c + 65), rowRangeLimit);
                        
                        // Create Comboboxes macro
                        string macro = CreateDropDownsMacro(rangeFrom, rangeTo, sheet.Name, currentTask.TaskDetails.SourceSheetName, sheetCount - 1);

                        // Add VBA macro to the workbook
                        module = wb.VBProject.VBComponents.Add(Microsoft.Vbe.Interop.vbext_ComponentType.vbext_ct_StdModule);
                        module.CodeModule.AddFromString(macro);

                        // Execute macro
                        wb.Application.Run(string.Format("CreateDropDownList{0}", sheetCount - 1), Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                     Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                     Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                     Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                        IsMacroEnabled = true;

                        sheetCount++;

                    }
                    else if (currentTask.SourceColumns[c].Style == Globals.CellStyle.Checkbox)
                    {
                      string rangeTarget = string.Format("{0}2:{0}{1}", char.ConvertFromUtf32(c + 65), 100); // only 100 rows for demonstration purpose as the generation of the full document takes significant time
                      string macro = CreateCheckBoxesMacro(rangeTarget, currentTask.TaskDetails.SourceSheetName, currentTask.SourceColumns[c].Name);

                      module = wb.VBProject.VBComponents.Add(Microsoft.Vbe.Interop.vbext_ComponentType.vbext_ct_StdModule);
                      module.CodeModule.AddFromString(macro);

                      wb.Application.Run(string.Format("CreateCheckBoxes{0}", currentTask.SourceColumns[c].Name), Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                     Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                     Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                                     Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                        IsMacroEnabled = true;
                    }
                
                }

                xl.Visible = false;
                xl.UserControl = false;
                SaveChanges = true;
                
                wb.SaveAs(FileName, xlFileFormat,
                    null, null, false, false, XlSaveAsAccessMode.xlShared,
                    false, false, null, null, null);               
                

            }
            catch (Exception theException)
            {
                String msg;
                msg = "Error: ";
                msg = String.Concat(msg, theException.Message);
                msg = String.Concat(msg, " Line: ");
                msg = String.Concat(msg, theException.Source);
                Console.WriteLine(msg);
            }
            finally
            {
                // Get Handle of the created excel document which will need to find process id
                string sVer = xl.Version;
                IntPtr iHandle = IntPtr.Zero;
                if (Convert.ToDecimal(sVer) >= 10)
                {
                    iHandle = new IntPtr(xl.Parent.Hwnd);
                }

                try
                {
                    xl.Visible = false;
                    xl.UserControl = false;
                    wb.Close(SaveChanges, null, null);
                    xl.Workbooks.Close();
                }
                catch { }

                xl.Quit();

                if (ds != null) { ds.Dispose(); }
                if (module != null) { Marshal.ReleaseComObject(module); }
                if (range != null) { Marshal.ReleaseComObject(range); }
                if (sheets != null) { Marshal.ReleaseComObject(sheets); }
                if (sheet != null) { Marshal.ReleaseComObject(sheet); }
                if (wb != null) { Marshal.ReleaseComObject(wb); }
                if (xl != null) { Marshal.ReleaseComObject(xl); }

                ds = null;
                module = null;
                range = null;
                sheets = null;
                sheet = null;
                wb = null;
                xl = null;

                GC.Collect();

                //kill the EXCEL process as a safety measure
                EnsureProcessKilled(iHandle, sVer);

            }

        }
        #endregion

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool EndTask(IntPtr hWnd, bool fShutDown, bool fForce);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError=true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("kernel32.dll")]
        static extern void SetLastError(uint dwErrCode);        

        private void EnsureProcessKilled(IntPtr MainWindowHandle, string Caption)
        {
            SetLastError(0);
            //for Excel versions <10, this won't be set yet
            if (IntPtr.Equals(MainWindowHandle, IntPtr.Zero))
            {

                MainWindowHandle = FindWindow(null, Caption);

                if (IntPtr.Equals(MainWindowHandle, IntPtr.Zero))
                {
                    return; //Exit at this point, presume the window has been closed.
                }
            }

            uint iRes, iProcID;
            iRes = GetWindowThreadProcessId(MainWindowHandle, out iProcID);

            if (iProcID == 0)
            {
                // Then can�t get Process ID
                if (!EndTask(MainWindowHandle, true, true))
                {
                    // success                    
                    throw new ApplicationException("Failed to close.");
                }
            }
            else
            {
                System.Diagnostics.Process proc = System.Diagnostics.Process.GetProcessById(Convert.ToInt32(iProcID));
                proc.CloseMainWindow();
                proc.Refresh();
                if (!proc.HasExited)
                {
                    proc.Kill();                    
                }
            }

        }
   

        private string fillWorksheet_WithDataView(_Worksheet worksheet, Range range, DataView dv)
        {
            //Add DataView Columns To Worksheet
            int row = 1;
            int col = 1;
            // Loop thought the columns
            for (int i = 0; i < dv.Table.Columns.Count; i++)
            {
                range = (Range)worksheet.Cells[row, col++];
                fillExcelCell(worksheet, range, dv.Table.Columns[i].ToString());
            }

            //Add DataView Rows To Worksheet
            row = 2;
            col = 1;

            for (int i = 0; i < dv.Table.Rows.Count; i++)
            {
                for (int j = 0; j < dv.Table.Columns.Count; j++)
                {
                    range = (Range)worksheet.Cells[row, col++];
                    fillExcelCell(worksheet, range, dv[i][j].ToString());
                }
                col = 1;
                row++;
            }


            /*if (range != null) { Marshal.ReleaseComObject(range); }
            range = null;*/

            return string.Format("B2:B{0}", row);

        }

        private void fillExcelCell(_Worksheet worksheet, Range range, Object Value)
        {
            range.Select();
            range.Value2 = Value.ToString();

            // because the Value2 method doesn't recognize date cells. It returns a float value instead of a date value.
            //(object[,])range.get_Value(XlRangeValueDataType.xlRangeValueDefault);

            //range.Columns.EntireColumn.AutoFit();
            range.Columns.EntireColumn.ColumnWidth = 26;            
            
            range.Borders.Weight = XlBorderWeight.xlThin;
            range.Borders.LineStyle = XlLineStyle.xlContinuous;
            range.Borders.ColorIndex = XlColorIndex.xlColorIndexAutomatic;
        }

        private void fillWorksheet_Headings(_Worksheet worksheet, Range range, List<SourceColumnInfo> columnList)
        {
            int row = 1;
            int col = 1;
            // Loop thought the columns
            for (int i = 0; i < columnList.Count; i++)
            {
                range = (Range)worksheet.Cells[row, col++];
                fillExcelCell(worksheet, range, columnList[i].Name);

                // now apply number format to whole column range if it is presented
                if (!string.IsNullOrEmpty(columnList[i].NumberFormat))
                {
                    range = (Range)worksheet.get_Range(string.Format("{0}2:{0}{1}", char.ConvertFromUtf32(i + 65), rowRangeLimit), Type.Missing);
                    range.NumberFormat = columnList[i].NumberFormat;
                }
            }
        }
    }

    
}
