﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using XLETL.Model;

namespace XLETL.TransformationPluginManager
{
  /// <summary>
  /// Interface for Transformation Plugin
  /// </summary>
    public interface IPluginControl
    {
        bool DoTransformation(DataTable data, string targetColumnName);
        Parameter[] GetTransformationParamList();
    }
}
