﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XLETL.TransformationPluginManager
{
    [global::System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class PluginAttribute : Attribute
    {
        readonly string name;
        readonly string description;
        readonly string target;

        // This is a positional argument
        public PluginAttribute(string name, string description, string target)
        {
            this.name = name;
            this.description = description;
            this.target = target;
        }

        public string Name { get { return name; } }
        public string Description { get { return description; } }
        public string Target { get { return target; } }
    }
}
