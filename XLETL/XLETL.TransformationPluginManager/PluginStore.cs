﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using XLETL.Model;

namespace XLETL.TransformationPluginManager
{
    [XmlRoot("PluginStore")]
    public class PluginStore
    {
        public List<PluginInfo> Plugins { get; set; }

        public PluginStore() { Plugins = new List<PluginInfo>(); } 
    }   
    
}
