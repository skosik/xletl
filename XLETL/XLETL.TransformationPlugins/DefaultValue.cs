﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Common;
using XLETL.TransformationPluginManager;
using XLETL.Model;

namespace XLETL.TransformationPlugins
{
    [Plugin("Default Value", "Set default value of a particular type", "Both")]
    public class DefaultValue : UserControl, IPluginControl
    {
        private GroupBox groupBox1;
        private TextBox tbNumeric;
        private DateTimePicker dtpDateTime;
        private RadioButton rbDateTime;
        private TextBox tbText;
        private RadioButton rbText;
        private TextBox tbNull;
        private RadioButton rbNull;
        private RadioButton rbBoolean;
        private ComboBox cbBoolean;
        private RadioButton rbNumeric;

        public string TransformationDefaultValue { get; private set; }
    
        public DefaultValue(Parameter[] pluginParams, Parameter[] transformationParams)
        {
            InitializeComponent();

            // if the control in edit mode then assign the appropriate values
            if (transformationParams != null && transformationParams.Length == 2)
            {
                switch (transformationParams[0].ParameterValue)
                {
                    case "rbNumeric":
                        rbNumeric.Checked = true;
                        tbNumeric.Text = transformationParams[1].ParameterValue;
                        break;
                    case "rbText":
                        rbText.Checked = true;
                        tbText.Text = transformationParams[1].ParameterValue;
                        break;
                    case "rbNull":
                        rbNull.Checked = true;
                        tbNull.Text = transformationParams[1].ParameterValue;
                        break;
                    case "rbDateTime":
                        rbDateTime.Checked = true;
                        // TODO: try parse the date before
                        dtpDateTime.Value = Convert.ToDateTime(transformationParams[1].ParameterValue);
                        break;
                    case "rbBoolean":                    
                        rbBoolean.Checked = true;
                        cbBoolean.Text = transformationParams[1].ParameterValue;                       
                        break;
                }

                TransformationDefaultValue = transformationParams[1].ParameterValue;
            }
        }

        #region IPluginControl Members

        public bool DoTransformation(DataTable data, string targetColumnName)
        {
            bool isSuccess = true;

            try
            {
                Type columnType = data.Columns[targetColumnName].DataType;
                object correctTypeValue = Convert.ChangeType(TransformationDefaultValue, columnType);
                
                foreach (DataRow dr in data.Rows)
                {
                    dr[targetColumnName] = correctTypeValue;
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }

            return isSuccess;
        }

        public Parameter[] GetTransformationParamList()
        {
            Parameter[] parameters = new Parameter[]{new Parameter(), new Parameter()};

            foreach (Control c in this.groupBox1.Controls)
            {
                if (c.GetType().FullName.Equals("System.Windows.Forms.RadioButton"))
                {
                    RadioButton control = (RadioButton)c;
                    if (control.Checked)
                    {
                        parameters[0].ParameterName = "RadioControlName";
                        parameters[0].ParameterType = "System.String";
                        parameters[0].ParameterValue = control.Name;
                    }
                }
                else if ((c.GetType().FullName.Equals("System.Windows.Forms.TextBox") || c.GetType().FullName.Equals("System.Windows.Forms.DateTimePicker") || c.GetType().FullName.Equals("System.Windows.Forms.ComboBox")) && c.Enabled)
                {
                    switch (c.Name)
                    {
                        case "tbNumeric":
                            parameters[1].ParameterName = "Numeric";
                            parameters[1].ParameterType = "";
                            parameters[1].ParameterValue = tbNumeric.Text;
                            break;
                        case "tbText":
                            parameters[1].ParameterName = "Text";
                            parameters[1].ParameterType = "System.String";
                            parameters[1].ParameterValue = tbText.Text;
                            break;
                        case "tbNull":
                            parameters[1].ParameterName = "Text";
                            parameters[1].ParameterType = "System.String";
                            parameters[1].ParameterValue = tbNull.Text;
                            break;
                        case "dtpDateTime":
                            parameters[1].ParameterName = "DateTime";
                            parameters[1].ParameterType = "System.DateTime";
                            parameters[1].ParameterValue = dtpDateTime.Value.ToString();
                            break;
                        case "cbBoolean":
                            parameters[1].ParameterName = "Boolean";
                            parameters[1].ParameterType = "System.Boolean";
                            parameters[1].ParameterValue = cbBoolean.Text;
                            break;
                    }

                }
            }
            return parameters;
        }

        #endregion

        private void InitializeComponent()
        {
          this.groupBox1 = new System.Windows.Forms.GroupBox();
          this.cbBoolean = new System.Windows.Forms.ComboBox();
          this.rbBoolean = new System.Windows.Forms.RadioButton();
          this.tbNull = new System.Windows.Forms.TextBox();
          this.rbNull = new System.Windows.Forms.RadioButton();
          this.tbText = new System.Windows.Forms.TextBox();
          this.rbText = new System.Windows.Forms.RadioButton();
          this.tbNumeric = new System.Windows.Forms.TextBox();
          this.dtpDateTime = new System.Windows.Forms.DateTimePicker();
          this.rbDateTime = new System.Windows.Forms.RadioButton();
          this.rbNumeric = new System.Windows.Forms.RadioButton();
          this.groupBox1.SuspendLayout();
          this.SuspendLayout();
          // 
          // groupBox1
          // 
          this.groupBox1.Controls.Add(this.cbBoolean);
          this.groupBox1.Controls.Add(this.rbBoolean);
          this.groupBox1.Controls.Add(this.tbNull);
          this.groupBox1.Controls.Add(this.rbNull);
          this.groupBox1.Controls.Add(this.tbText);
          this.groupBox1.Controls.Add(this.rbText);
          this.groupBox1.Controls.Add(this.tbNumeric);
          this.groupBox1.Controls.Add(this.dtpDateTime);
          this.groupBox1.Controls.Add(this.rbDateTime);
          this.groupBox1.Controls.Add(this.rbNumeric);
          this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.groupBox1.Location = new System.Drawing.Point(0, 0);
          this.groupBox1.Name = "groupBox1";
          this.groupBox1.Size = new System.Drawing.Size(304, 343);
          this.groupBox1.TabIndex = 0;
          this.groupBox1.TabStop = false;
          this.groupBox1.Text = "Set Default Value";
          // 
          // cbBoolean
          // 
          this.cbBoolean.Enabled = false;
          this.cbBoolean.FormattingEnabled = true;
          this.cbBoolean.Items.AddRange(new object[] {
            "False",
            "True"});
          this.cbBoolean.Location = new System.Drawing.Point(79, 292);
          this.cbBoolean.Name = "cbBoolean";
          this.cbBoolean.Size = new System.Drawing.Size(199, 21);
          this.cbBoolean.TabIndex = 9;
          // 
          // rbBoolean
          // 
          this.rbBoolean.AutoSize = true;
          this.rbBoolean.Location = new System.Drawing.Point(20, 272);
          this.rbBoolean.Name = "rbBoolean";
          this.rbBoolean.Size = new System.Drawing.Size(64, 17);
          this.rbBoolean.TabIndex = 8;
          this.rbBoolean.Text = "Boolean";
          this.rbBoolean.UseVisualStyleBackColor = true;
          // 
          // tbNull
          // 
          this.tbNull.Enabled = false;
          this.tbNull.Location = new System.Drawing.Point(79, 239);
          this.tbNull.Name = "tbNull";
          this.tbNull.Size = new System.Drawing.Size(199, 20);
          this.tbNull.TabIndex = 7;
          // 
          // rbNull
          // 
          this.rbNull.AutoSize = true;
          this.rbNull.Location = new System.Drawing.Point(20, 218);
          this.rbNull.Name = "rbNull";
          this.rbNull.Size = new System.Drawing.Size(43, 17);
          this.rbNull.TabIndex = 6;
          this.rbNull.Text = "Null";
          this.rbNull.UseVisualStyleBackColor = true;
          this.rbNull.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
          // 
          // tbText
          // 
          this.tbText.Enabled = false;
          this.tbText.Location = new System.Drawing.Point(79, 154);
          this.tbText.Multiline = true;
          this.tbText.Name = "tbText";
          this.tbText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.tbText.Size = new System.Drawing.Size(199, 52);
          this.tbText.TabIndex = 5;
          // 
          // rbText
          // 
          this.rbText.AutoSize = true;
          this.rbText.Location = new System.Drawing.Point(20, 134);
          this.rbText.Name = "rbText";
          this.rbText.Size = new System.Drawing.Size(46, 17);
          this.rbText.TabIndex = 4;
          this.rbText.Text = "Text";
          this.rbText.UseVisualStyleBackColor = true;
          this.rbText.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
          // 
          // tbNumeric
          // 
          this.tbNumeric.Location = new System.Drawing.Point(79, 48);
          this.tbNumeric.Name = "tbNumeric";
          this.tbNumeric.Size = new System.Drawing.Size(199, 20);
          this.tbNumeric.TabIndex = 3;
          // 
          // dtpDateTime
          // 
          this.dtpDateTime.Enabled = false;
          this.dtpDateTime.Location = new System.Drawing.Point(79, 101);
          this.dtpDateTime.Name = "dtpDateTime";
          this.dtpDateTime.Size = new System.Drawing.Size(199, 20);
          this.dtpDateTime.TabIndex = 2;
          // 
          // rbDateTime
          // 
          this.rbDateTime.AutoSize = true;
          this.rbDateTime.Location = new System.Drawing.Point(20, 80);
          this.rbDateTime.Name = "rbDateTime";
          this.rbDateTime.Size = new System.Drawing.Size(71, 17);
          this.rbDateTime.TabIndex = 1;
          this.rbDateTime.Text = "DateTime";
          this.rbDateTime.UseVisualStyleBackColor = true;
          this.rbDateTime.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
          // 
          // rbNumeric
          // 
          this.rbNumeric.AutoSize = true;
          this.rbNumeric.Checked = true;
          this.rbNumeric.Location = new System.Drawing.Point(20, 27);
          this.rbNumeric.Name = "rbNumeric";
          this.rbNumeric.Size = new System.Drawing.Size(64, 17);
          this.rbNumeric.TabIndex = 0;
          this.rbNumeric.TabStop = true;
          this.rbNumeric.Text = "Numeric";
          this.rbNumeric.UseVisualStyleBackColor = true;
          this.rbNumeric.CheckedChanged += new System.EventHandler(this.rb_CheckedChanged);
          // 
          // DefaultValue
          // 
          this.Controls.Add(this.groupBox1);
          this.Name = "DefaultValue";
          this.Size = new System.Drawing.Size(304, 343);
          this.groupBox1.ResumeLayout(false);
          this.groupBox1.PerformLayout();
          this.ResumeLayout(false);

        }

        #region Designer

        private void rb_CheckedChanged(object sender, EventArgs e)
        {
            tbNumeric.Enabled = rbNumeric.Checked;
            tbText.Enabled = rbText.Checked;
            dtpDateTime.Enabled = rbDateTime.Checked;           
            tbNull.Enabled = rbNull.Checked;
            cbBoolean.Enabled = rbBoolean.Checked;            
        }       
        
        #endregion

        
    }
}
