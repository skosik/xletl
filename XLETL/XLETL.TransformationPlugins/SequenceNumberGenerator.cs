﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XLETL.TransformationPluginManager;
using XLETL.Model;
using System.Data;

namespace XLETL.TransformationPlugins
{
    [Plugin("Sequence Number Generator", "Generate Numbers in sequence with predefened step", "Target")]
    public class SequenceNumberGenerator : UserControl, IPluginControl
    {
        private Label label1;
        private NumericUpDown nudStepNum;
        private NumericUpDown nudStartNum;
        private GroupBox groupBox1;
        private Label label2;

        private decimal StartNumber
        {
            get
            {
                return nudStartNum.Value;
            }
            set
            {
                nudStartNum.Value = value;
            }
        }

        private decimal StepNumber
        {
            get
            {
                return nudStepNum.Value;
            }
            set
            {
                nudStepNum.Value = value;
            }
        }

        public SequenceNumberGenerator(Parameter[] pluginParams, Parameter[] transformationParams)
        {
            InitializeComponent();

            if (transformationParams != null && transformationParams.Length == 2)
            {
                try
                {
                    nudStartNum.Value = Convert.ToDecimal(transformationParams[0].ParameterValue);
                    nudStepNum.Value = Convert.ToDecimal(transformationParams[1].ParameterValue);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }        

        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nudStepNum = new System.Windows.Forms.NumericUpDown();
            this.nudStartNum = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudStepNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStartNum)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Step:";
            // 
            // nudStepNum
            // 
            this.nudStepNum.Location = new System.Drawing.Point(94, 66);
            this.nudStepNum.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudStepNum.Name = "nudStepNum";
            this.nudStepNum.Size = new System.Drawing.Size(82, 20);
            this.nudStepNum.TabIndex = 2;
            // 
            // nudStartNum
            // 
            this.nudStartNum.Location = new System.Drawing.Point(95, 30);
            this.nudStartNum.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nudStartNum.Name = "nudStartNum";
            this.nudStartNum.Size = new System.Drawing.Size(82, 20);
            this.nudStartNum.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Start Number:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nudStartNum);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.nudStepNum);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 107);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sequence Number Generator";
            // 
            // SequenceNumberGenerator
            // 
            this.Controls.Add(this.groupBox1);
            this.Name = "SequenceNumberGenerator";
            this.Size = new System.Drawing.Size(203, 107);
            ((System.ComponentModel.ISupportInitialize)(this.nudStepNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStartNum)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #region IPluginControl Members

        public bool DoTransformation(System.Data.DataTable data, string targetColumnName)
        {        
            bool isSuccess = true;

            int nextNumber = Convert.ToInt32(StartNumber);
            int step = Convert.ToInt32(StepNumber);

            try
            {
                //Type columnType = data.Columns[targetColumnName].DataType;      
                
                foreach (DataRow dr in data.Rows)
                {
                    dr[targetColumnName] = nextNumber;
                    nextNumber += step;
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }

            return isSuccess;
        
        }

        public Parameter[] GetTransformationParamList()
        { 
            Parameter[] parameters = null;
            try{
            Parameter paramStartNum = new Parameter();
            paramStartNum.ParameterName = "StartNumber";
            paramStartNum.ParameterType = "System.Decimal";
            paramStartNum.ParameterValue = Convert.ToString(StartNumber);
            Parameter paramStepNum = new Parameter();
            paramStartNum.ParameterName = "StepNumber";
            paramStartNum.ParameterType = "System.Decimal";
            paramStartNum.ParameterValue = Convert.ToString(StepNumber);

            parameters = new Parameter[] { paramStartNum, paramStepNum };
            }catch (Exception ex)
            {
                throw;
            }

            return parameters; 
        }

        #endregion
    }
}



    
