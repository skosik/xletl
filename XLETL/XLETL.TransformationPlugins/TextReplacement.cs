﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XLETL.TransformationPluginManager;
using XLETL.Model;
using System.Data;

namespace XLETL.TransformationPlugins
{
    [Plugin("Text Replacement", "Replace predefined text with another text", "Source")]
    public class TextReplacement : UserControl, IPluginControl
    {
        private GroupBox groupBox1;
        private Button btnAdd;
        private Button btnRemove;
        private DataGridViewTextBoxColumn TextToFind;
        private DataGridViewTextBoxColumn TextToReplace;
        private DataGridView dataGridView1;

        public TextReplacement(Parameter[] pluginParams, Parameter[] transformationParams)
        {
            InitializeComponent();
            //SetupPlugin();
        }

        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.TextToFind = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TextToReplace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRemove);
            this.groupBox1.Controls.Add(this.btnAdd);
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 213);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Define Text To Replace";
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.Location = new System.Drawing.Point(177, 25);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 20);
            this.btnRemove.TabIndex = 2;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Location = new System.Drawing.Point(258, 25);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 20);
            this.btnAdd.TabIndex = 1;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TextToFind,
            this.TextToReplace});
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dataGridView1.Location = new System.Drawing.Point(6, 51);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(332, 156);
            this.dataGridView1.TabIndex = 0;
            // 
            // TextToFind
            // 
            this.TextToFind.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TextToFind.HeaderText = "Text To Find";
            this.TextToFind.Name = "TextToFind";
            this.TextToFind.ReadOnly = true;
            // 
            // TextToReplace
            // 
            this.TextToReplace.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TextToReplace.HeaderText = "TextToReplace";
            this.TextToReplace.Name = "TextToReplace";
            this.TextToReplace.ReadOnly = true;
            // 
            // TextReplacement
            // 
            this.Controls.Add(this.groupBox1);
            this.Name = "TextReplacement";
            this.Size = new System.Drawing.Size(344, 213);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }       

        
        #region IPluginControl Members

        public bool DoTransformation(System.Data.DataTable data, string targetColumnName)
        {
          bool isSuccess = true;

          try
          {
            foreach (DataRow dr in data.Rows)
            {
              //dr[targetColumnName] = correctTypeValue;
            }
          }
          catch (Exception)
          {
            isSuccess = false;
          }

          return isSuccess;
        }

        public XLETL.Model.Parameter[] GetTransformationParamList()
        {
            throw new NotImplementedException();
        }


        #endregion
    }
}
