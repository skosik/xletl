﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XLETL.BLL;
using XLETL.Model;
using XLETL.Common;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
        string username = Login1.UserName;
        string password = Login1.Password;
        User user = new User();
        UserInfo userInfo = user.GetUser(username, password);

        if (userInfo != null)
        {
            Page.Server.Transfer("Tasks.aspx?db=" + userInfo.DatabaseID.ToString(), true);          
        }else
        {
            e.Authenticated = false;
            
        }
    }
}
