﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Downloads : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    string qFile = Request.QueryString["file"];

    if (!string.IsNullOrEmpty(qFile))
    {
      // Get the physical Path of the file(test.doc)
      string filepath = qFile;

      // Create New instance of FileInfo class to get the properties of the file being downloaded
      FileInfo file = new FileInfo(filepath);

      // Checking if file exists
      if (file.Exists)
      {
        // Clear the content of the response
        Response.ClearContent();

        // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
        Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);

        // Add the file size into the response header
        Response.AddHeader("Content-Length", file.Length.ToString());

        // Set the ContentType
        Response.ContentType = ReturnExtension(file.Extension.ToLower());

        // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
        Response.TransmitFile(file.FullName);

        // End the response
        Response.End();
      }
    }
  }

  private string ReturnExtension(string fileExtension)
  {
    switch (fileExtension)
    {
      case ".htm":
      case ".html":
      case ".log":
        return "text/HTML";
      case ".txt":
        return "text/plain";
      case ".doc":
        return "application/ms-word";
      case ".tiff":
      case ".tif":
        return "image/tiff";
      case ".asf":
        return "video/x-ms-asf";
      case ".avi":
        return "video/avi";
      case ".zip":
        return "application/zip";
      case ".xls":
      case ".xlsx":
      case ".csv":
        return "application/vnd.ms-excel";
      case ".xlsm":
        return ".xlsm";
      case ".gif":
        return "image/gif";
      case ".jpg":
      case "jpeg":
        return "image/jpeg";
      case ".bmp":
        return "image/bmp";
      case ".wav":
        return "audio/wav";
      case ".mp3":
        return "audio/mpeg3";
      case ".mpg":
      case "mpeg":
        return "video/mpeg";
      case ".rtf":
        return "application/rtf";
      case ".asp":
        return "text/asp";
      case ".pdf":
        return "application/pdf";
      case ".fdf":
        return "application/vnd.fdf";
      case ".ppt":
        return "application/mspowerpoint";
      case ".dwg":
        return "image/vnd.dwg";
      case ".msg":
        return "application/msoutlook";
      case ".xml":
      case ".sdxl":
        return "application/xml";
      case ".xdp":
        return "application/vnd.adobe.xdp+xml";
      default:
        return "application/octet-stream";
    }
  }
}
