﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Tasks.aspx.cs" MasterPageFile="~/MasterPage.master" Inherits="Tasks" Title="XLETL Tasks" %>
<%@ Import Namespace="XLETL.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    
    <h2><b>List of Tasks &nbsp; ( Database: </b><asp:Label ID="databaseName" runat="server"></asp:Label> )</h2>
    <br />
    <p>Below you find a list of all available XLETL tasks.
    <br />
    To view details about a task, download the Excel Template, or upload the data file for processing, click the corresponding <b>Select</b> link.</p>
    <div class="taskContent">
      <asp:GridView ID="GridView1" runat="server"  AutoGenerateColumns="False" Width="700" RowStyle-HorizontalAlign="Left"  
        DataSourceID="ObjectDataSource1"   HeaderStyle-ForeColor="Gray" DataKeyNames="ID" BorderColor="#666633" BorderStyle="Solid" BorderWidth="1px" CellPadding="4" ForeColor="#333333" GridLines="Both" AllowPaging="True" PageSize="5" 
      AutoGenerateSelectButton="True">
        <Columns>        
           <asp:TemplateField HeaderText="Task Name">
            <ItemTemplate>
              <asp:Label ID="Label1" runat="server" Text='<%# Eval("TaskDetails.Name") %>'></asp:Label>
            </ItemTemplate>
            <ItemStyle Width="250px" />
          </asp:TemplateField>          
          <asp:TemplateField HeaderText="Task Description">
            <ItemTemplate>
              <asp:Label ID="Label3" runat="server" Text='<%# Eval("TaskDetails.Description") %>'></asp:Label>
            </ItemTemplate>
            <ItemStyle Width="400px" />
          </asp:TemplateField>                   
        </Columns>
        <SelectedRowStyle  ForeColor="White" Width="75px" BackColor="#CCCC99" Font-Bold="true"></SelectedRowStyle>
    <RowStyle BorderColor="#CCCC99" BorderStyle="None" BorderWidth="1px"  ForeColor="#8C4510" BackColor="#FFF7E7"></RowStyle>
     
      </asp:GridView>
      <br /> 
           
      <asp:DetailsView ID="DetailsView1" runat="server" Height="10px" Width="700px"
      RowStyle-HorizontalAlign="Left" FieldHeaderStyle-Width="150px" FieldHeaderStyle-Font-Bold="true" 
             BackColor="#FFF7E7" BorderColor="#666633" BorderStyle="Solid" BorderWidth="1" CellSpacing="0" CellPadding="3" 
      AutoGenerateRows="False" DataSourceID="TaskDetails" DataKeyNames="ID">
      <FooterStyle ForeColor="#FF0000" BackColor="#CCCC99"></FooterStyle>
             <RowStyle ForeColor="#8C4510" BackColor="#FFF7E7" BorderStyle="Solid" BorderWidth="1px"></RowStyle>
             <HeaderTemplate>
             <h3>Task Details:</h3>
             </HeaderTemplate>             
        <Fields>        
        <asp:TemplateField HeaderText="Task Name">
            <ItemTemplate>
              <asp:Label ID="Label1" Width="400px" runat="server" Text='<%# Eval("TaskDetails.Name") %>'></asp:Label>
            </ItemTemplate>
            <ItemStyle Width="130px" Wrap="True" />
          </asp:TemplateField>
        <asp:TemplateField HeaderText="Task Description">
            <ItemTemplate>
              <asp:Label ID="Label2" runat="server" Text='<%# Eval("TaskDetails.Description") %>'></asp:Label>
            </ItemTemplate>
            <HeaderStyle Width="130px" />
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Process Mode">
            <ItemTemplate>
              <asp:Label ID="Label3" runat="server" Text='<%# Eval("TaskDetails.ProcessMode") %>'></asp:Label>
            </ItemTemplate>
          </asp:TemplateField>
          <asp:BoundField DataField="TargetTableName" HeaderText="Target Table Name" 
            SortExpression="TargetTableName" />
          <asp:BoundField DataField="TaskSchedule" HeaderText="ETL Schedule" 
            SortExpression="TaskSchedule" />         
            <asp:HyperLinkField HeaderText="Download Template" ControlStyle-ForeColor="#003366" ControlStyle-Font-Underline="true"  DataTextField="TemplatePath" DataTextFormatString="Click here" DataNavigateUrlFields="TemplatePath" DataNavigateUrlFormatString="http://localhost:3920/XLETL.Web/Downloads.aspx?file={0}" >             
             </asp:HyperLinkField>          
          <asp:TemplateField HeaderText="Upload Document">
            <ItemTemplate><asp:FileUpload ID="fuDocument" runat="server" />
            <asp:Button ID="Button1" runat="server" Height="22px" Text="Upload" onclick="Button1_Click" />
            </ItemTemplate>            
          </asp:TemplateField>
        </Fields>

<FieldHeaderStyle Width="130px"></FieldHeaderStyle>
        <FooterTemplate>
        <asp:Label ID="lblMessageStatus" runat="server" Visible="false" Text="Document uploaded successfuly."></asp:Label>      
        </FooterTemplate>
        <HeaderStyle BackColor="White" ForeColor="Gray" HorizontalAlign="Left"  
                Font-Bold="True"></HeaderStyle>
    </asp:DetailsView>
    </div>
    <br />   
    
     
    
      <asp:ObjectDataSource ID="TaskDetails" runat="server" 
      SelectMethod="GetTaskDetails" TypeName="XLETL.BLL.Task" 
      onselecting="TaskDetails_Selecting">
        <SelectParameters>
          <asp:ControlParameter ControlID="GridView1" DbType="Guid" Name="taskID" 
            PropertyName="SelectedValue" />
        </SelectParameters>       
    </asp:ObjectDataSource>   
      
      <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
        SelectMethod="GetTasks" TypeName="XLETL.BLL.Task" 
        onselecting="ObjectDataSource1_Selecting">
        <SelectParameters>
          <asp:Parameter DbType="Guid" Name="databaseId" />
        </SelectParameters>
      </asp:ObjectDataSource>
    
    
    </asp:Content>
