﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using XLETL.BLL;
using XLETL.Model;
using XLETL.Common;

public partial class Tasks : System.Web.UI.Page
{
  Guid DatabaseId;
    protected void Page_Load(object sender, EventArgs e)
    {
      string dbId = Request.QueryString["db"];

      if (!string.IsNullOrEmpty(dbId))
      {
        DatabaseId = new Guid(dbId);        
      }
    }

    protected override void OnPreRender(EventArgs e)
    {
      base.OnPreRender(e);

      if (DatabaseId != Guid.Empty)
      {
        DB db = new DB();
        DBInfo dbInfo = db.GetDBDetails(DatabaseId);
        if (dbInfo != null)
        {
          this.databaseName.Text = dbInfo.DBFriendlyName;
        }
      }
    }

    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters[0] = DatabaseId;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
      Label msgStatus = (Label)DetailsView1.FindControl("lblMessageStatus");
      FileUpload theFileUpload = (FileUpload)DetailsView1.FindControl("fuDocument");
      if (theFileUpload != null && theFileUpload.HasFile)
      {
        object[] items = (object[])TaskDetails.Select();

        if (items.Length > 0)
        {
          try
          {
            TaskInfo task = (TaskInfo)items[0];
            theFileUpload.SaveAs(task.FileUploadPath + "\\" + theFileUpload.FileName);
          }
          catch (Exception ex)
          {
            msgStatus.Text = "Error occured while uploading document";
          }
          finally
          {
            msgStatus.Visible = true;
          }
        }
      }       
      
    }
    protected void TaskDetails_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      
    }
}
